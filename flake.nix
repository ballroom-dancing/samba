{
  description = "samba – smart and mechanic ballroom assistant";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    rust-overlay = {
      url = "github:oxalica/rust-overlay";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, flake-utils, rust-overlay, ... }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        overlays = [ (import rust-overlay) ];
        pkgs = import nixpkgs {
          inherit system overlays;
        };

        rust-toolchain = pkgs.rust-bin.fromRustupToolchainFile ./rust-toolchain.toml;
        buildinputs-common = [
          rust-toolchain
          pkgs.clang
          # mold is disabled for now, see .cargo/config.toml for conditions for
          # reenabling it
          # pkgs.mold
          # the *-sys crates rely on pkg-config to determine the nix library
          # headers we are installing subsequently
          pkgs.pkg-config
        ];
        buildinputs-websrv = buildinputs-common ++ [
          # samba-backend-websrv requires openssl for axum's TLS support
          # and sqlite for embedded backend
          pkgs.openssl
          pkgs.sqlite
        ];
        buildinputs-desktop = buildinputs-common ++ [
          # tauri requires multiple sys crates
          pkgs.dbus
          pkgs.dbus-glib
          pkgs.libsoup
          pkgs.webkitgtk
        ];
      in
      {
        devShells = {
          default = pkgs.mkShell {
            buildInputs = with pkgs; buildinputs-websrv ++ buildinputs-desktop ++ [
              # project development setup
              cargo-deny
              cargo-edit
              cargo-machete
              # cargo-readme is broken since we use workspace inheritance
              # reenable once https://github.com/livioribeiro/cargo-readme/issues/81
              # is resolved
              # cargo-readme
              # our test suite does not actually run on a level where coverage would
              # be significant, so we don't need tarpaulin yet (corresponding CI job
              # is disabled as well)
              # cargo-tarpaulin
              # currently, we don't build the desktop app, so there is no need
              # for the tauri helper
              cargo-tauri
              # frontend
              cargo-leptos
              leptosfmt
              tailwindcss
              nodejs # for daisyUI
              hurl
              trunk
              pre-commit
              nixpkgs-fmt
              # diesel-cli for developing the database parts
              diesel-cli
              # audiowaveform for analysing tracks and importing their
              # waveform into samba
              audiowaveform
              # weasyprint for generating reports for samba
              python3Packages.weasyprint
              # css dev setup for the webapp
              csslint
              # SSG for the website
              zola
            ];

            RUST_SRC_PATH = "${rust-toolchain}/lib/rustlib/src/rust/library";
            LIBCLANG_PATH = "${pkgs.libclang.lib}/lib/";
          };
          lint = pkgs.mkShell {
            nativeBuildInputs = with pkgs; [
              csslint
              hurl
              leptosfmt
              nixpkgs-fmt
              pre-commit
              rustfmt
              git
            ];
          };
          website = pkgs.mkShell {
            nativeBuildInputs = with pkgs; [
              zola
            ];
          };
        };
      }
    );
}
