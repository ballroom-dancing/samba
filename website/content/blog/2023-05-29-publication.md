+++
title = "Open-sourcing samba"
description = "A music and playlist management tool for ballroom dancers."
template = "blog/page.html"
toc = true

[extra]
lead = """\
After more than five years and four rewrites I decided to open-source samba, \
a tool dedicated to help ballroom dancers manage their music (and hopefully \
more than that in the future). This post details what samba is about, where it \
started (the short version), what it should become, and how you can help (not \
only by suggesting a new name).\
"""
+++

# Managing music for ballroom dancers

After years of ballroom dancing (WDSF Standard and Latin), there is one thing I
consider most essential in every practice and at every competition: the music.
However, it is actually non-trivial to manage a considerable collection of
dance music for several reasons, among them:

- Dancers measure differently. Basically all music-related software assesses a
  track's tempo by displaying the beats per minute (BPM). Dancers, however,
  use BPM – bars per minute (or measures per minute), a metric you cannot
  convince any sane software to respect as it depends on more factors than
  just the beats.
- Some dancers, who do not work with streaming services, have huge music
  collections and very different ways to categorize tracks. Identifying a
  track as being a “Slow Waltz” or a “Cha Cha Cha” will involve different
  criteria in different music collections. Existing apps usually do not cope
  well with neither: huge collections nor flexible criteria to identify a
  dance.
- (Competitive) dancers do not usually need mixing or complicated audio
  effects. Playback is usually enough. But in your average competition
  practice you do not want your whole 3 minute track to play but rather
  between 90 and 120 seconds (competition length). Fade outs are a nice
  gimmick and appreciated as well.
- Dancing competitions and their practice follow certain patterns (e.g. a
  fixed order of dances, each dance danced in a given number of heats and so
  on). I am not aware of any smart playlist management system that allows me
  to encode a certain order of dances as a base playlist and modify that
  playlist into something like “base playlist in 2 heats”.

In 2016, the first two reasons were enough for me to write a small application
for dance music management. Since then, I largely gave up with the custom
software and used available applications (okay, mostly VLC and a smartwatch to
control the playback) to tailor my dance music experience to my practice needs.
But with more involvement into dancing as my primary sport the desire for a
dedicated software never ceased to exist and so I kept this as one of my
continuous side projects, working on and off on it.

# samba – a ballroom assistant

Since 2016, a lot has happened. The software I wrote back then does not exist
anymore, I have learned a few things about software engineering, and my
interest in dancing has evolved. Fast forward a few years, I am happy to
present you samba, the result of five rewrites of a small tool I once wrote to
just look at my dance music library.

Today, samba is a Rust full-stack project, featuring a backend using
[Axum][axum-gh] and a frontend using the [leptos][leptos-gh] frontend
framework. Its main use case is still to provide a good overview over your
music collections, opinionated for dancers. To that end, it uses
[aoide][aoide-gh] for music library management and [tantivy][tantivy-gh] to
provide query-based search facilities.

Solving use case 1 (searching for bars per minute) in samba is as simple as
querying for `mpm:50` or for a range like `mpm:[48 TO 50]`. Well, but this
alone is not too useful because it would return me Samba tracks as well as
Quicksteps. Instead, I can show off use case 2 (flexibility) as well:
`mpm:[48 TO 50] AND dance:Samba` will return all Samba tracks in the given mpm
range but some people use the genre tag to identify their dances so they will
use `mpm:[48 TO 50] AND genre:Samba`.

{{ image(
  path = "blog/2023-05-29-publication/library-query.png",
  alt = "library view with a dance query matching samba tracks of 48 to 50 measures per minute"
) }}

So far so good, we can view and play individual tracks from a library view. But
it does not actually solve a real use case in dancing practice. So for a
minimum viable product (or at least open-sourcing samba) I needed another
feature: playlists/practice sessions. They are based on the same query
principles, just that you use queries to identify playlist items and on
playback time, these queries are then resolved to get individual tracks.

{{ image(
  path = "blog/2023-05-29-publication/edit-practice-session.jpg",
  alt = "edit a practice session for WDSF latin practice"
) }}

Okay, so these are the two main features that are currently implemented. And
even those do have their rough edges. But they work well enought that I do use
them for my personal dancing practice at home.

Apart from the aforementioned two main features, samba comes with a basic
authentication system (not enough that you should trust it, so please do not
publicly host instances just yet) and user preferences.

In the long run, there are two more main features that I intend to add:

- routine management (also known as choreographies, to have a place to store
  dance routines, practice routines, etc.; ideally both in textual
  representation of the steps as well as media to visualize them)
- dancing event management (especially in terms of required music, for instance
  competition planning or planning the next practice evening)

More details on this can be found on the roadmap (see the project's README).
Nevertheless, let me outline what is out of scope of the project for now:

- being a sophisticated music player (samba is opinionated towards dancing)
- being a management interface to music files (samba indexes these files but
  does not work on filesystem level even though aoide supports synchronizing
  back; for now: use proper tag editors)

# How you can help

Let me start out with the non-technical aspects: currently, I am maintaining
samba alone. I am looking for interested dancers and developers alike. I am
basically looking for co-maintainers as well as less ambitious help, for
instance:

- [help finding a new name][new-name]: samba has always been intended as a
  temporary name, especially since another popular server software with that
  name exists, feel free to make suggestions for new names in the issue.
  This is pretty important to the project as a whole because it will be a
  blocker for a potential first release.
- contributing ideas: samba strives to become a go-to tool for ballroom
  dancers, so it would be interesting to hear more user stories what is
  missing in the current workflows of dancers, be it related to music or
  other aspects of dancing practice.
- provide early feedback: Try samba's current features and report how it
  integrates into your workflow or what is missing for you to try it in a
  real setting (e.g. your local dance club).
- help designing the frontend: I am not a web designer. Even if you do not
  know your way around Rust, leptos' JSX is like HTML and with some frontend
  design knowledge in the HTML and CSS world you might well contribute to the
  frontend's design and maybe even help [choosing samba's next CSS
  framework][css-framework].
- help developing the application: As a single developer with limited spare
  time it is not easy to achieve anything quickly. However, if you feel like
  joining the development of a Rust full-stack application with quite a number
  of open TODOs ranging from backend/database work like improving the
  authentication process or pre-processing tracks to frontend tasks like
  improving the error handling or even adding new user interfaces, just have
  a look at the issue tracker or drop me a line in samba's matrix room.

Some items above have corresponding issues, other items do not. If you just
want to discuss ballroom-related technical topics or this project, feel free
to [join samba's matrix room][matrix].

# My experience using Rust for this project

Last but not least, let me answer the question “why Rust for this project” in a
few bullet points:

- “if it compiles, it works”: Yes, this has actually been my experience, at
  least when working on the backend. But even in the WASM frontend world, I am
  impressed by the (low) number of development iterations needed until a
  feature that compiles actually works as intended. The only two runtime errors
  I actually remember during my development time in Rust on this project are a
  diesel-related migration issue and a number of “mutable borrow” issues in
  sycamore and leptos.
- “sharing code between backend and frontend”: The previous version of samba,
  written in Kotlin, already achieved this. But in my experience, Kotlin's
  multiplatform projects are not as seamless as Rust's story concerning shared
  code between backend (native) code and frontend (WASM) code. That goes as
  far as me considering to leverage leptos' SSR with hydration in the future
  which compiles the same project on WASM and native (with few feature gates),
  something I would have never really considered in Kotlin.
- “great developer tooling”: `cargo` is awesome (especially considering this
  project previously used `gradle`). Simple dependency management is great
  as well. And tools like `trunk` even make WASM frontend development pretty
  enjoyable. I look forward to make my way to `cargo-leptos` one day.
- “a feeling of locality”: Okay, this punchline is probably a bit broad. What
  I actually mean is that every code segment feels local enough that I know
  what is going on. I know what is going to fail (and how) because of the
  `Result` error handling, I know what is imported from where, and the module
  structure seems to encourage me to split code at a reasonably granular level.
  Of course, this whole point is very subjective.
- “good error messages … within reason”: I love to get descriptive error
  messages. And indeed, Rust's error messages are among the best I have seen so
  far. However, after mastering the basics of the borrow checker, they are
  mostly great in situations where I would need less hints but they lack
  quite severely when I would need more. Working with `diesel` and `axum` and
  their non-trivial trait system comes to my mind.

Everything that is more detailed (like the history of the other rewrites, how I
ended up with Rust, why I switched from sycamore to leptos, and what I am
missing from previous iterations, be it Kotlin or sycamore) will have to wait
for another blog post as this announcement post has become long enough.

[aoide-gh]: https://gitlab.com/uklotzde/aoide-rs
[axum-gh]: https://github.com/tokio-rs/axum
[leptos-gh]: https://github.com/leptos-rs/leptos
[tantivy-gh]: https://github.com/quickwit-oss/tantivy
[css-framework]: https://gitlab.com/ballroom-dancing/samba/-/issues/16
[matrix]: https://matrix.to/#/#smart-and-mechanic-ballroom-assistant:matrix.org?via=matrix.org
[new-name]: https://gitlab.com/ballroom-dancing/samba/-/issues/2
