# samba

*The smart and mechanic ballroom assistant.*

![agplv3](https://img.shields.io/gitlab/license/ballroom-dancing/samba?color=blue&style=flat-square)
![release](https://img.shields.io/gitlab/v/release/ballroom-dancing/samba?style=flat-square)
![chat on matrix](https://img.shields.io/matrix/smart-and-mechanic-ballroom-assistant:matrix.org?label=chat%20on%20matrix&style=flat-square)
![latest build result](https://img.shields.io/gitlab/pipeline-status/ballroom-dancing/samba?branch=main&style=flat-square)

> **Warning**: This is pre-release software. Many essential features like
> authentication, user management and easy upgrade paths are unfinished.
> Use at your own risk. Please do not host public instances just yet.

## What is samba (supposed to be)?

* Music library interface for dancesport-oriented music libraries
* Session-oriented music player (e.g. practice sessions, competition practice,
  workouts, …)
* Routine management (dancing routines, show choreographies, …)
* Dancing event management (especially in terms of required music)

In a nutshell, samba aims to be of service (and not in the way) of dancing
clubs and active dancers, for regular practice, competition practice, and all
the other dancing related matters.

![Library view](./resources/library-view.png){width=40%}
&nbsp;&nbsp;&nbsp;&nbsp;
![Edit practice session](./resources/edit-practice-session.png){width=40%}

> The name *samba* is temporary. If you have new ideas, please add a comment at
> https://gitlab.com/ballroom-dancing/samba/-/issues/2.

## What is samba *not*?

* Sophisticated music player (samba is opinionated towards dancing)
* Management interface to music files (samba indexes these files but does not
  work on filesystem level; read: use proper tag editors)

## How to run it?

If you do have a [Rust setup](https://rustup.rs) for Rust nightly with
[trunk](https://trunkrs.dev) set up (see [`CONTRIBUTING`](./CONTRIBUTING.md)
for more details), it is recommended to run two local shells or starting all
in one:

```
$ nix develop
$ cargo run -p samba-backend-websrv &
$ cd samba-frontend/webapp && trunk serve --features csr
```

This will run the local backend at `http://localhost:8081` and serve the
frontend at `http://localhost:8080`. The default login is `demo`/`demo`.

Alternatively, you can download the latest backend and frontend artifacts from
the continuous integration jobs, run the `samba-backend-websrv` binary and let
a local webserver host the distribution directory of the frontend.

> If after updating anything fails, please note that before v0.1.0 database
> migrations are changed regularly, so you may have to remigrate. Simply
> `rm samba-backend/websrv/{aoide,samba}.sqlite && rm -r /tmp/samba-tantivy`
> (if you have kept default settings) and run the backend again, it will start
> afresh empty with the latest migrations.

## Quickstart guide

0. Start the backend server.
1. Open the frontend in your browser (by default `http://localhost:8080`).
   Log into the management interface using username `demo` and password `demo`.
2. Go to settings (e.g. from the link at the bottom of the page), select
   manage libraries.
3. Add a new library and hit the reindex button afterwards (the one with the
   two arrows). Do not select too large libraries (a couple of hundred files
   will work fine, though).
4. Wait for the process to complete (give it some time, there is no status
   indicator for now).
5. Enjoy the WIP user experience.

**Alternative**: Instead of using the frontend to add a new library, you can
use samba's initialization [hurl](https://hurl.dev) file in the
[backend directory][hurlfile]. Adjust the music library path in that file to
your library's path and then run `hurl initialize.hurl`. Then you can skip
steps 2–4 above.

## Roadmap

This is a very general overview. Look into the repository's issues for
more details.

### v0.1.0

* Provide everything mentioned in the `What is samba?` section, where the focus
  is clearly on the music aspect, routines and events may be very barebones.
  Target platform for acceptance is Linux x86 for the backend and the web for
  the frontend (i.e. no Tauri frontend needed for this point).
* Eliminate all clearly inefficient workarounds (e.g. client-side pagination,
  bad filtering, …) to have sustainable performance, even though it might not
  be blazing fast.
* Get JS interaction right so that at least the Wavesurfer player works as
  intended and displays ranges, plays correctly and fetches pre-processed
  waveforms.
* Have basic authentication (even if only single-user) to allow hosting music
  from at home or at some hoster while accessing it on a mobile from a dancing
  club.

### future

* Explore server-side rendering and generate a proxy server that serves the
  frontend and integrates the backend routes.
* Tauri desktop and mobile app.
* Accessibility improvements.
* …

## License

This software makes use of aoide, an AGPLv3-licensed server software. Hence,
for license-compatibility, the AGPLv3 license has been chose for all parts of
the application making use of aoide which turned out to be the whole
application.

There are several static assets included in this repository which are
distributed under their respective licenses (e.g. fontawesome, the frontend
fonts, …). See the license notes in the respective directories.

[hurlfile]: https://gitlab.com/ballroom-dancing/samba/-/blob/main/samba-backend/websrv/initialize.hurl
