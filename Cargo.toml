[workspace]
members = [
  "samba-desktop",
  "samba-shared",
  "samba-backend/database",
  "samba-backend/reports",
  "samba-backend/websrv",
  "samba-frontend/shared",
  "samba-frontend/uicomponents",
  "samba-frontend/webapp",
]
default-members = [
  "samba-shared",
  "samba-backend/websrv",
  "samba-frontend/webapp",
]
resolver = "2"

[workspace.package]
version = "0.1.0"
edition = "2024"
rust-version = "1.85"
publish = false
authors = [ "Ben Frank <ben.frank@mail.de>" ]

[workspace.metadata.nix]
toolchain = "nightly"

[workspace.dependencies]
# samba's own components
samba-shared                = { path = "samba-shared" }
samba-backend-database      = { path = "samba-backend/database" }
samba-backend-reports       = { path = "samba-backend/reports" }
samba-backend-websrv        = { path = "samba-backend/websrv" }
samba-frontend-shared       = { path = "samba-frontend/shared" }
samba-frontend-uicomponents = { path = "samba-frontend/uicomponents" }

# dependencies required for all of samba
aoide                       = { git = "https://gitlab.com/uklotzde/aoide-rs.git", branch = "main" }
axum                        = "0.8.0-rc.1"
chrono                      = { version = "0.4.39", default-features = false }
serde                       = "1.0.216"
serde_json                  = "1.0.133"
thiserror                   = "2.0.8"
tower                       = "0.5.2"
tower-http                  = "0.6.2"

# dependencies in samba's backend
argon2                      = "0.5.3"
askama                      = "0.12.1"
axum-extra                  = "0.10.0-rc.1"
color-eyre                  = "0.6.3"
diesel                      = "2.2.6"
diesel_migrations           = "2.2.0"
dotenvy                     = "0.15.7"
hyper                       = "1.5.2"
hyper-util                  = "0.1.10"
image                       = "0.25.5"
itertools                   = "0.13.0"
jsonwebtoken                = "9.3.0"
lazy-regex                  = "3.3.0"
maud                        = "0.26.0"
rand                        = "=0.9.0-beta.1"
ring                        = "0.17.8"
rust-embed                  = "8.5.0"
secrecy                     = "0.10.3"
serde_with                  = "3.11.0"
tantivy                     = "0.22.0"
tempfile                    = "3.14.0"
tokio                       = "1.42.0"
tokio-util                  = "0.7.13"
tracing                     = "0.1.41"
tracing-error               = "0.2.1"
tracing-subscriber          = "0.3.19"
tracing-tree                = "0.4.0"
ulid                        = "1.1.3"
url                         = "2.5.4"

# dependencies in samba's frontend
anyhow                      = "1.0.94"
codee                       = { version = "0.2.0", default-features = false }
console_log                 = "1.0.0"
console_error_panic_hook    = "0.1.7"
gloo-net                    = "0.6.0"
gloo-timers                 = "0.3.0"
js-sys                      = "0.3.76"
leptos                      = { version = "0.6.15", default-features = false }
leptos_axum                 = { version = "0.6.15", default-features = false }
leptos_meta                 = { version = "0.6.15", default-features = false }
leptos_router               = { version = "0.6.15", default-features = false }
leptos-use                  = { version = "0.13.11" }
log                         = "0.4"
serde-wasm-bindgen          = "0.6.5"
tts                         = "0.26.3"
urlencoding                 = "2.1.3"
uuid                        = "1.11.0"
wasm-bindgen                = "0.2"
wasm-bindgen-test           = "0.3.49"
wasm-bindgen-futures        = "0.4"
web-sys                     = "0.3.76"

[profile.release]
lto = true

[profile.release.package.samba-frontend-webapp]
opt-level = 'z'
codegen-units = 1

[profile.wasm-release]
inherits = "release"
opt-level = 'z'
codegen-units = 1

[workspace.lints.rust]
rust-2018-compatibility = "deny"
rust-2018-idioms = "deny"
unsafe_code = "deny"
future_incompatible = { level = "warn", priority = -1 }
missing_debug_implementations = "warn"
nonstandard_style = { level = "warn", priority = -1 }
# -Wrust_2021_compatibility is a bit over-aggressive (especially because of the closure
# capturing which has been checked throughout the codebase, therefore we warn only on
# some of the group's items:
array-into-iter = "deny"
bare-trait-objects = "deny"
ellipsis-inclusive-range-patterns = "deny"
non-fmt-panics = "deny"
rust-2021-incompatible-or-patterns = "deny"
rust-2021-prefixes-incompatible-syntax = "deny"
rust-2021-prelude-collisions = "deny"
rust-2024-compatibility = "deny"

[workspace.lints.clippy]
panic_in_result_fn = "deny"
all = { level = "warn", priority = -1 }
cargo = { level = "warn", priority = -1 }
await_holding_lock = "warn"
char_lit_as_u8 = "warn"
checked_conversions = "warn"
clone_on_ref_ptr = "warn"
dbg_macro = "warn"
debug_assert_with_mut_call = "warn"
disallowed_methods = "warn"
doc_markdown = "warn"
empty_enum = "warn"
enum_glob_use = "warn"
equatable_if_let = "warn"
exit = "warn"
expl_impl_clone_on_copy = "warn"
explicit_deref_methods = "warn"
explicit_into_iter_loop = "warn"
explicit_iter_loop = "warn"
fallible_impl_from = "warn"
filter_map_next = "warn"
flat_map_option = "warn"
float_cmp_const = "warn"
fn_params_excessive_bools = "warn"
from_iter_instead_of_collect = "warn"
if_let_mutex = "warn"
implicit_clone = "warn"
imprecise_flops = "warn"
inefficient_to_string = "warn"
invalid_upcast_comparisons = "warn"
iter_not_returning_iterator = "warn"
large_digit_groups = "warn"
large_stack_arrays = "warn"
large_types_passed_by_value = "warn"
let_unit_value = "warn"
linkedlist = "warn"
lossy_float_literal = "warn"
macro_use_imports = "warn"
manual_ok_or = "warn"
map_err_ignore = "warn"
map_flatten = "warn"
map_unwrap_or = "warn"
match_on_vec_items = "warn"
match_same_arms = "warn"
match_wild_err_arm = "warn"
match_wildcard_for_single_variants = "warn"
mem_forget = "warn"
missing_const_for_fn = "warn"
missing_enforced_import_renames = "warn"
missing_errors_doc = "warn"
missing_safety_doc = "warn"
mod_module_files = "warn"
must_use_candidate = "warn"
mut_mut = "warn"
mutex_integer = "warn"
needless_borrow = "warn"
needless_continue = "warn"
needless_for_each = "warn"
needless_pass_by_value = "warn"
option_option = "warn"
path_buf_push_overwrite = "warn"
ptr_as_ptr = "warn"
rc_mutex = "warn"
ref_option_ref = "warn"
rest_pat_in_fully_bound_structs = "warn"
same_functions_in_if_condition = "warn"
semicolon_if_nothing_returned = "warn"
single_match_else = "warn"
string_add_assign = "warn"
string_add = "warn"
string_lit_as_bytes = "warn"
string_to_string = "warn"
todo = "warn"
trait_duplication_in_bounds = "warn"
unexpected_cfgs = "warn"
unimplemented = "warn"
unnested_or_patterns = "warn"
unused_self = "warn"
used_underscore_binding = "warn"
useless_transmute = "warn"
verbose_file_reads = "warn"
zero_sized_map_values = "warn"

[workspace.lints.rustdoc]
# TODO: enable for v1.0
# missing_crate_level_docs = "warn"
broken_intra_doc_links = "warn"
