use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct Library {
    pub id: String,
    pub name: String,
    pub path: String,
    pub aoide_collection: String,
    pub scan_subdirectories: bool,
    pub shared: bool,
    pub excluded_directories: Vec<String>,
}

/// The frontend request to create a library at the backend.
#[derive(Debug)]
#[cfg_attr(feature = "backend", derive(Deserialize))]
#[cfg_attr(feature = "frontend", derive(Serialize))]
pub struct CreateLibraryRequestBody {
    pub id: String,
    pub name: String,
    pub path: String,
    pub scan_subdirectories: bool,
    pub shared: bool,
}
