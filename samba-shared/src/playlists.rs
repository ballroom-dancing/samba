use std::{future::Future, time::Duration};

use aoide::{
    TrackEntity, TrackUid,
    media::content::ContentMetadata,
    playlist::{
        Entry as AoidePlaylistEntry, Item as AoidePlaylistItem,
        PlaylistWithEntries as AoidePlaylist, SeparatorItem as AoideSeparator,
        TrackItem as AoideTrackItem,
    },
    util::clock::OffsetDateTimeMs,
};

/// A playlist considering a sequence of entities that can be played which
/// means items that have a certain duration and are scheduled sequentially.
///
/// Actually, playlist items may overlap but at max. two at a time are played
/// overlaying each other. Use cases for these include voice announcements
/// being played at the beginning of a track itself (e.g. for practice
/// sessions).
#[derive(Debug, Clone, PartialEq)]
pub struct Playlist {
    /// A playlist needs a title to be displayed.
    pub title: String,
    /// The items of the playlist. Will be played in order.
    pub items: Vec<PlaylistItem>,
    /// A list of tags when this playlist would be/shall be played. May be
    /// event identifiers (like prom-2020) or even just plain tags.
    pub played_at: Vec<String>,
}

/// Estimated duration with a flag indicating whether this is actually
/// estimated. Estimates always underestimate, never overestimate.
#[derive(Debug, Clone)]
pub struct LengthEstimate {
    pub duration: Duration,
    pub is_estimate: bool,
}

impl Playlist {
    /// Estimate the length of the playlist without access to the database.
    /// In general, this will be an under-approximation.
    #[must_use]
    pub fn estimate_length(&self) -> LengthEstimate {
        let mut duration = Duration::ZERO;
        let mut is_estimate = false;

        for entry in &self.items {
            duration += match entry {
                PlaylistItem::Playback(audio) => {
                    let duration = audio.playback_properties.duration;
                    if duration == Duration::ZERO {
                        if let AudioSource::ResolvedTrack(ref entity) = audio.audio_source {
                            let ContentMetadata::Audio(audio_content) =
                                &entity.body.track.media_source.content.metadata;
                            audio_content.duration.map_or_else(
                                || {
                                    is_estimate = true;
                                    Duration::ZERO
                                },
                                |millis| Duration::from_millis(millis.value().round() as u64),
                            )
                        } else {
                            is_estimate = true;
                            Duration::ZERO
                        }
                    } else {
                        duration
                    }
                },
                PlaylistItem::Pause(duration) => *duration,
                _ => Duration::ZERO,
            };
        }

        LengthEstimate {
            duration,
            is_estimate,
        }
    }

    /// Resolve playlist items to `PlaylistItem::ResolvedTrack` by querying the
    /// data source using `query_function`. Returns `true` if *all* items could
    /// be resolved.
    pub async fn resolve_tracks<F, Fut>(&mut self, query_function: F) -> bool
    where
        F: Fn(&str) -> Fut,
        Fut: Future<Output = Option<TrackEntity>>,
    {
        let mut success = true;

        for item in &mut self.items {
            match item {
                PlaylistItem::Playback(AudioPlayback {
                    audio_source: AudioSource::TrackUid(uid),
                    playback_properties,
                }) => {
                    if let Some(entity) = query_function(&format!("uid:{uid}")).await {
                        *item = PlaylistItem::Playback(AudioPlayback {
                            audio_source: AudioSource::ResolvedTrack(Box::new(entity)),
                            playback_properties: playback_properties.clone(),
                        });
                    } else {
                        success = false;
                    };
                },
                PlaylistItem::Playback(AudioPlayback {
                    audio_source: AudioSource::TrackQuery(query),
                    playback_properties,
                }) => {
                    if let Some(entity) = query_function(query).await {
                        *item = PlaylistItem::Playback(AudioPlayback {
                            audio_source: AudioSource::ResolvedTrack(Box::new(entity)),
                            playback_properties: playback_properties.clone(),
                        });
                    } else {
                        success = false;
                    }
                },
                _ => {},
            }
        }

        success
    }
}

impl From<AoidePlaylist> for Playlist {
    fn from(playlist: AoidePlaylist) -> Self {
        Self {
            title: playlist.playlist.title,
            items: playlist
                .entries
                .iter()
                .map(|entry| entry.item.clone())
                .map(Into::into)
                .collect(),
            played_at: playlist
                .playlist
                .kind
                .map_or_else(Vec::new, |kind| vec![kind]),
        }
    }
}

/// An item in the playlist. This controls the execution flow of the playlist
/// playback.
#[derive(Debug, Clone, PartialEq)]
pub enum PlaylistItem {
    /// Regular playback items playing audio (e.g. music tracks or pre-recorded
    /// announcements).
    Playback(AudioPlayback),
    /// Pausing the playback for a certain duration. The playlist will resume
    /// with the next item automatically.
    Pause(Duration),
    /// Pausing the playback for an indeterminate amount. This equals pressing
    /// the pause key in a media player. The playback must be resumed manually.
    /// Useful for planned moderation breaks.
    Break,
    /// Stop the playback completely. This equals pressing the stop key in a
    /// media player and resets the playback state.
    Stop,
    /// A visual separator item with an optional label. Should be the default
    /// for unknown separator kinds imported from third-party aoide clients.
    Visual(Option<String>),
}

// TODO: use TryFrom and don't convert to visual separator on error
// automatically?
impl From<AoidePlaylistItem> for PlaylistItem {
    fn from(item: AoidePlaylistItem) -> Self {
        match item {
            AoidePlaylistItem::Separator(AoideSeparator { kind }) => {
                match kind.as_deref() {
                    Some("break") => PlaylistItem::Break,
                    Some("stop") => PlaylistItem::Stop,
                    Some(p) if p.starts_with("pause(") && p.ends_with("s)") => p
                        .strip_prefix("pause(")
                        .and_then(|p| p.strip_suffix("s)"))
                        .and_then(|p| {
                            // TODO: more sophisticated parsing depending on unit suffix
                            p.parse::<u64>().ok()
                        })
                        .map_or_else(
                            || PlaylistItem::Visual(Some(format!("Invalid pause entry '{p}'"))),
                            |s| PlaylistItem::Pause(Duration::from_secs(s)),
                        ),
                    Some(q) if q.starts_with("track(") && q.ends_with(')') => q
                        .strip_prefix("track(")
                        .and_then(|q| q.strip_suffix(')'))
                        .map_or_else(
                            || PlaylistItem::Visual(Some(format!("Invalid track query '{q}'"))),
                            |s| {
                                PlaylistItem::Playback(AudioPlayback::new_from_query(s.to_string()))
                            },
                        ),
                    Some(q) if q.starts_with("tts(") && q.ends_with(')') => q
                        .strip_prefix("tts(")
                        .and_then(|q| q.strip_suffix(')'))
                        .map_or_else(
                            || PlaylistItem::Visual(Some(format!("Invalid tts field '{q}'"))),
                            |s| PlaylistItem::Playback(AudioPlayback::new_from_tts(s.to_string())),
                        ),
                    Some(k) => PlaylistItem::Visual(Some(k.to_string())),
                    None => PlaylistItem::Visual(None),
                }
            },
            AoidePlaylistItem::Track(item) => {
                PlaylistItem::Playback(AudioPlayback::new_from_track_id(item.uid))
            },
        }
    }
}

impl From<AoidePlaylistEntry> for PlaylistItem {
    fn from(entry: AoidePlaylistEntry) -> Self {
        let item: Self = entry.item.into();

        let (PlaylistItem::Playback(mut playback), Some(ref notes)) = (item.clone(), entry.notes)
        else {
            return item;
        };

        for line in notes.lines() {
            if let Ok(props) = line.parse() {
                playback.playback_properties = props;
                break;
            }
        }

        PlaylistItem::Playback(playback)
    }
}

impl From<PlaylistItem> for AoidePlaylistItem {
    fn from(item: PlaylistItem) -> Self {
        match item {
            PlaylistItem::Playback(AudioPlayback {
                audio_source: AudioSource::TrackUid(uid),
                ..
            }) => AoidePlaylistItem::Track(AoideTrackItem { uid }),
            PlaylistItem::Playback(AudioPlayback {
                audio_source: AudioSource::ResolvedTrack(entity),
                ..
            }) => AoidePlaylistItem::Track(AoideTrackItem {
                uid: entity.hdr.uid.clone(),
            }),
            PlaylistItem::Playback(AudioPlayback {
                audio_source: AudioSource::TrackQuery(query),
                ..
            }) => AoidePlaylistItem::Separator(AoideSeparator {
                kind: Some(format!("track({query})")),
            }),
            PlaylistItem::Playback(AudioPlayback {
                audio_source: AudioSource::TextToSpeech(text),
                ..
            }) => AoidePlaylistItem::Separator(AoideSeparator {
                kind: Some(format!("tts({text})")),
            }),
            PlaylistItem::Break => AoidePlaylistItem::Separator(AoideSeparator {
                kind: Some("break".into()),
            }),
            PlaylistItem::Stop => AoidePlaylistItem::Separator(AoideSeparator {
                kind: Some("stop".into()),
            }),
            PlaylistItem::Pause(duration) => AoidePlaylistItem::Separator(AoideSeparator {
                kind: Some(format!("pause({}s)", duration.as_secs())),
            }),
            PlaylistItem::Visual(label) => {
                AoidePlaylistItem::Separator(AoideSeparator { kind: label })
            },
        }
    }
}

impl From<PlaylistItem> for AoidePlaylistEntry {
    fn from(item: PlaylistItem) -> Self {
        let notes = if let PlaylistItem::Playback(AudioPlayback {
            ref playback_properties,
            ..
        }) = item
        {
            playback_properties.to_notes_string()
        } else {
            None
        };

        let item: AoidePlaylistItem = item.into();
        AoidePlaylistEntry {
            added_at: OffsetDateTimeMs::now_utc(),
            title: None,
            notes,
            item,
        }
    }
}

/// An audio source for playback.
#[derive(Debug, Clone, PartialEq)]
pub enum AudioSource {
    /// A textual query against the music collection. Should be valid tantivy
    /// query syntax.
    TrackQuery(String),
    /// The result of a query `uid:something`. Mostly a more performant
    /// version of the `TrackQuery` variant.
    TrackUid(TrackUid),
    /// A resolved track, completely resolved from the database. Useful for
    /// usage without database access and to reduce queries.
    ///
    /// Boxed because of the size of [`TrackEntity`] compared to the other
    /// variants.
    ResolvedTrack(Box<TrackEntity>),
    /// Text that shall be synthesized (e.g. announcements like “Heat 1”).
    TextToSpeech(String),
}

/// A playlist item representing audio playback. Its duration is implicitly
/// given by the track object. It can be customized by optional playback hints.
/// For all those playback hints, a duration of zero means that the effect is
/// disabled.
#[derive(Debug, Clone, PartialEq)]
pub struct AudioPlayback {
    /// The audio source to be played back. Basically, this must represent an
    /// audio file or something else with similar properties which supports
    /// getting a length estimate and a playable stream.
    pub audio_source: AudioSource,
    /// Properties of the playback which model basic transitions and cutting.
    pub playback_properties: PlaybackProperties,
}

impl AudioPlayback {
    /// Create a new playback item from a track only.
    #[must_use]
    pub fn new_from_track_id(track_uid: TrackUid) -> Self {
        Self {
            audio_source: AudioSource::TrackUid(track_uid),
            playback_properties: Default::default(),
        }
    }

    /// Create a new playback item from a track query only.
    #[must_use]
    pub fn new_from_query(query: String) -> Self {
        Self {
            audio_source: AudioSource::TrackQuery(query),
            playback_properties: Default::default(),
        }
    }

    /// Create a new playback item from a text to speech text only.
    #[must_use]
    pub fn new_from_tts(query: String) -> Self {
        Self {
            audio_source: AudioSource::TextToSpeech(query),
            playback_properties: Default::default(),
        }
    }
}

#[derive(Debug, Clone, PartialEq, Default)]
pub struct PlaybackProperties {
    /// Play from `start_offset` (inclusive) onwards, i.e. set the cursor to
    /// this track position before starting the playback.
    pub start_at: Duration,
    /// Play until `start_offset` + `duration` (exclusive), i.e. stop just
    /// before the cursor reaches the position. A duration of zero will be
    /// interpreted as playing the whole track because zero-length playlist
    /// items are not useful and at most separators.
    pub duration: Duration,
    /// Add a fade in effect in the time range [`start_at`, `start_at` +
    /// `fade_in`).
    pub fade_in: Duration,
    /// Add a fade out effect in the time range [`start_at` + `duration` -
    /// `fade_out`, `start_at` + `duration`).
    pub fade_out: Duration,
}

impl PlaybackProperties {
    /// Get a compact string representation of this object which may be used
    /// for serialization.
    #[must_use]
    pub fn to_notes_string(&self) -> Option<String> {
        let items = [
            (!self.start_at.is_zero()).then(|| format!(">{}", self.start_at.as_secs())),
            (!self.fade_in.is_zero()).then(|| format!("+{}", self.fade_in.as_secs())),
            (!self.fade_out.is_zero()).then(|| format!("-{}", self.fade_out.as_secs())),
            (!self.duration.is_zero()).then(|| format!("{}", self.duration.as_secs())),
        ]
        .into_iter()
        .flatten()
        .collect::<Vec<String>>()
        .join(",");

        (!items.is_empty()).then_some(items)
    }
}

impl std::str::FromStr for PlaybackProperties {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut out: Self = Default::default();

        for part in s.split(',').map(|p| p.trim()) {
            if part.starts_with('+') {
                out.fade_in = part
                    .strip_prefix('+')
                    .and_then(|d| d.parse::<u64>().ok())
                    .map(Duration::from_secs)
                    .ok_or("Invalid fade in duration")?;
            } else if part.starts_with('-') {
                out.fade_out = part
                    .strip_prefix('-')
                    .and_then(|d| d.parse::<u64>().ok())
                    .map(Duration::from_secs)
                    .ok_or("Invalid fade out duration")?;
            } else if part.starts_with('>') {
                out.start_at = part
                    .strip_prefix('>')
                    .and_then(|d| d.parse::<u64>().ok())
                    .map(Duration::from_secs)
                    .ok_or("Invalid start offset duration")?;
            } else {
                out.duration = part
                    .parse::<u64>()
                    .map(Duration::from_secs)
                    .map_err(|_parse_error| "Invalid item duration")?;
            }
        }

        Ok(out)
    }
}

/// Request parameters to create a playlist from a track query.
#[derive(Clone, Debug)]
#[cfg_attr(feature = "frontend", derive(serde::Serialize))]
#[cfg_attr(feature = "backend", derive(serde::Deserialize))]
pub struct CreationFromTrackQueryParameters {
    pub query: String,
    #[cfg_attr(any(feature = "frontend", feature = "backend"), serde(default))]
    pub offset: usize,
    #[cfg_attr(any(feature = "frontend", feature = "backend"), serde(default))]
    pub limit: Option<usize>,
    #[cfg_attr(any(feature = "frontend", feature = "backend"), serde(default))]
    pub randomize_order: bool,
}

/// The playlist kind assigned to aoide playlists representing the playback of
/// a track query search, usually playing back what the library currently
/// shows.
pub const TRACK_QUERY_PLAYBACK_KIND: &str = "search-playback";

/// The playlist kind assigned to aoide playlists representing unresolved
/// practice sessions (i.e., those with placeholder entries for track queries
/// and similar items).
pub const PRACTICE_SESSION_KIND: &str = "practice-session";

/// The playlist kind assigned to aoide playlists representing resolved
/// practice sessions. Intended to represent an instantiation of a playlist
/// of [`PRACTICE_SESSION_KIND`].
pub const PRACTICE_HISTORY_KIND: &str = "practice-history";
