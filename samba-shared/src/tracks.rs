use std::num::NonZeroU8;

mod artist;
mod title;

pub mod lint;
pub mod metadata;
pub mod query;
pub use self::{artist::parse as parse_artist_candidate, title::parse as parse_title_candidate};

/// Update a track field of type `T` by adding a new value (if the field
/// supports multiple values, else it should be treated as replacement),
/// removing the existing value (if the field is then left without value
/// where this cannot be represented, the update is supposed to fail), or
/// by replacing the old (specified) value by a new one (failing if there
/// it no matching old value).
#[derive(Clone, Debug, PartialEq, Eq)]
pub enum TrackUpdateOperation<T> {
    Add(T),
    Remove(T),
    Replace(T, T),
}

/// A track update operation as a result of a lint or track analysis.
///
/// Some fields are wrapped in a `TrackUpdateOperation`, especially if there
/// are multiple possible values. For mandatory metadata, the respective
/// variants like `Title` will only contain the new value after the update.
#[derive(Clone, Debug, PartialEq)]
pub enum TrackUpdate {
    Title(String),
    Comment(TrackUpdateOperation<String>),
    /// Dances are aoide faceted tags consisting of a dance identifier and an
    /// associated score (weight).
    Dance(TrackUpdateOperation<(String, f64)>),
    MeasuresPerMinute(TrackUpdateOperation<u8>),
}

/// Metadata as extracted from the file's name or its title attribute.
#[derive(Clone, Debug)]
pub struct ExtractedMetadata {
    pub title: Option<String>,
    pub artists: Vec<String>,
    pub remix: Option<String>,
    pub movie: Option<String>,
    pub comment: Option<String>,
    pub dance_abbreviations: Vec<String>,
    pub measures_per_minute: Option<NonZeroU8>,
}

/// The waveform data as provided by the `audiowaveform` program and as
/// required by websurfer-js. However, the latter only accesses the `data`
/// field, so this type may be reduced further in the future.
#[derive(Clone, Debug, serde::Deserialize)]
#[cfg_attr(feature = "backend", derive(serde::Serialize))]
pub struct WebsurferJsWaveformData {
    pub version: u32,
    pub channels: u8,
    pub sample_rate: u32,
    pub samples_per_pixel: u32,
    pub bits: u8,
    pub length: u32,
    pub data: Vec<f64>,
}
