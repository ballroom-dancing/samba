use serde::{Deserialize, Serialize};

use super::Stored;

// TODO: add modification metadata?
pub type StoredRoutine = Stored<i32, Routine>;

/// A dancing routine (or program/choreography/…) is a sequence of
/// [`RoutineElement`]s (basically figures or individual steps). There
/// are different kinds of routines like show routines on a specific
/// track or competition routines on a specific dance with strict tempo.
///
/// Example (short standard routine):
///
/// ```
/// use samba_shared::routines::{
///     Figure, Foot, LevelRequirement, Routine, RoutineElement, Step, StepTiming, WeightTransfer,
/// };
///
/// let rtn = Routine {
///     title: Some("Standard Example".into()),
///     description: None,
///     choreographer: None,
///     performers: vec![],
///     demonstration: vec![],
///     steps: vec![
///         // Wait and do one preparation step before starting the routine.
///         // May also be used to describe starting on a 5 (against phrase)
///         // and similar choreographic elements.
///         RoutineElement::Pause(StepTiming::Slow),
///         RoutineElement::Step(
///             Step::new(Foot::Left, StepTiming::Quick, WeightTransfer::Full)
///                 .with_comment("start diagonally facing wall on long side".into()),
///         ),
///         // Start the routine with a fully described figure.
///         RoutineElement::Figure(
///             Figure::new("Natural Turn Steps 1–3".into())
///                 .with_steps(
///                     vec![
///                         Step::new(Foot::Right, StepTiming::Quick, WeightTransfer::Full),
///                         Step::new(Foot::Left, StepTiming::Quick, WeightTransfer::Full),
///                         Step::new(Foot::Right, StepTiming::Quick, WeightTransfer::Full),
///                     ],
///                     vec![
///                         Step::new(Foot::Left, StepTiming::Quick, WeightTransfer::Full),
///                         Step::new(Foot::Right, StepTiming::Quick, WeightTransfer::Full),
///                         Step::new(Foot::Left, StepTiming::Quick, WeightTransfer::Full),
///                     ],
///                 )
///                 .expect("The step counts are equal for leader and follower.")
///                 .with_level_requirement(LevelRequirement::WdsfDCSyllabus),
///         ),
///         // Figures can also be added in a prototype fashion without steps
///         // although this may make it impossible to determine the length of
///         // the routine and similar calculations.
///         RoutineElement::Figure(
///             Figure::new("Overturned Natural Spin Turn".into())
///                 .with_level_requirement(LevelRequirement::WdsfDCSyllabus),
///         ),
///         RoutineElement::Figure(
///             Figure::new("Reverse Turn Steps 4–6".into())
///                 .with_level_requirement(LevelRequirement::WdsfDCSyllabus),
///         ),
///         RoutineElement::Figure(
///             Figure::new("Whisk".into())
///                 .with_level_requirement(LevelRequirement::WdsfDCSyllabus),
///         ),
///         RoutineElement::Figure(
///             Figure::new("Progressive Chasse to Left".into())
///                 .with_level_requirement(LevelRequirement::WdsfDCSyllabus),
///         ),
///         RoutineElement::Figure(
///             Figure::new("Natural Turn Steps 1–3".into())
///                 .with_level_requirement(LevelRequirement::WdsfDCSyllabus),
///         ),
///         RoutineElement::Separator(Some("start of the short side".into())),
///         RoutineElement::Figure(
///             Figure::new("Impetus".into())
///                 .with_level_requirement(LevelRequirement::WdsfDCSyllabus),
///         ),
///         RoutineElement::Figure(
///             Figure::new("Weave".into())
///                 .with_level_requirement(LevelRequirement::WdsfDCSyllabus),
///         ),
///         RoutineElement::Figure(
///             Figure::new("Curved Feather".into())
///                 .with_level_requirement(LevelRequirement::WdsfDCSyllabus),
///         ),
///         RoutineElement::Figure(
///             Figure::new("Outside Spin".into())
///                 .with_level_requirement(LevelRequirement::WdsfDCSyllabus),
///         ),
///         RoutineElement::Figure(
///             Figure::new("Progressive Chasse to Left".into())
///                 .with_level_requirement(LevelRequirement::WdsfDCSyllabus),
///         ),
///         // Skip 3 full quicks/beats (i.e. the preparation steps) when
///         // starting this routine again on the long side.
///         RoutineElement::Repeat(StepTiming::Custom(3 * 4)),
///     ],
///     notes: None,
/// };
/// // Because of the prototyped nature the length of the routine is an
/// // underestimate.
/// assert_eq!(6, rtn.beat_length());
/// ```
#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct Routine {
    pub title: Option<String>,
    pub description: Option<String>,
    pub choreographer: Option<String>,
    /// The couples or dancers performing this routine or at least those who
    /// know it well enough.
    pub performers: Vec<String>,
    /// The URI pointing to some kind of demonstration. May be multiple sources
    /// if there are e.g. different perspectives.
    pub demonstration: Vec<String>,
    pub steps: Vec<RoutineElement>,
    pub notes: Option<String>,
}

impl Routine {
    /// Create a new, empty routine.
    #[must_use]
    pub const fn empty() -> Self {
        Self {
            title: None,
            description: None,
            choreographer: None,
            performers: vec![],
            demonstration: vec![],
            steps: vec![],
            notes: None,
        }
    }

    /// Add steps to the routine. Shortcut for testing purposes which need
    /// steps. To be extended to a builder API before removing config guard.
    #[cfg(test)]
    #[must_use]
    pub fn with_steps(mut self, steps: Vec<RoutineElement>) -> Self {
        self.steps = steps;
        self
    }

    /// Determine the minimum level of dancing required for this routine.
    ///
    /// Returns [`LevelRequirement::ToBeDetermined`] as soon as one part
    /// of the routine is not cleared for another level.
    ///
    /// All singular steps that are not part of a figure are evaluated as
    /// cleared for all levels. Same for pauses (regardless of length that
    /// may be an issue).
    // TODO: think about summing up pauses and checking against class
    //       requirements? maybe wrong place here → CompetitionRoutine?
    #[must_use]
    pub fn level_requirement(&self) -> LevelRequirement {
        let mut minimum_level = LevelRequirement::WdsfDCSyllabus;

        for step in &self.steps {
            let RoutineElement::Figure(Figure {
                level_requirement, ..
            }) = step
            else {
                continue;
            };
            minimum_level = match (minimum_level, *level_requirement) {
                (LevelRequirement::ToBeDetermined, _) | (_, LevelRequirement::ToBeDetermined) => {
                    LevelRequirement::ToBeDetermined
                },
                (LevelRequirement::WdsfDCSyllabus, LevelRequirement::WdsfDCSyllabus) => {
                    LevelRequirement::WdsfDCSyllabus
                },
                (LevelRequirement::WdsfDCSyllabus, higher_level) => higher_level,
                (LevelRequirement::Open, _)
                | (LevelRequirement::WdsfBSyllabus, LevelRequirement::Open) => {
                    LevelRequirement::Open
                },
                (LevelRequirement::WdsfBSyllabus, _) => LevelRequirement::WdsfBSyllabus,
            };
        }

        minimum_level
    }

    /// Determine the length of the routine in beats (rounding up to full beats
    /// in the music if ending with stray semiquavers).
    #[must_use]
    pub fn beat_length(&self) -> usize {
        let mut semiquavers = 0usize;

        for step in &self.steps {
            match step {
                RoutineElement::Stop | RoutineElement::Repeat(_) => {
                    break;
                },
                RoutineElement::Separator(_) => {
                    continue;
                },
                // Requires domain assumption that leaders steps and followers
                // steps are of equal duration.
                RoutineElement::Figure(Figure { leaders_steps, .. }) => {
                    semiquavers = semiquavers - semiquavers % 4
                        + 4 * calculate_beat_length(
                            leaders_steps
                                .iter()
                                .map(|Step { timing, .. }| timing)
                                .copied(),
                        );
                },
                RoutineElement::Step(Step { timing, .. }) | RoutineElement::Pause(timing) => {
                    semiquavers = semiquavers - semiquavers % 4 + timing.to_semiquavers();
                },
            };
        }

        semiquavers.next_multiple_of(4) / 4
    }
}

/// A routine as performed on a competition (strict tempo, one dance).
#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct CompetitionRoutine {
    /// The routine itself including all the steps.
    pub routine: Routine,
    /// The dance this competition routine covers.
    // TODO: use dance identifier type
    pub dance: String,
}

/// A routine as performed for show or show dance competitions (not necessarily
/// strict tempo, multiple dances possible).
#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct ShowRoutine {
    /// The routine itself including all the steps.
    pub routine: Routine,
    /// The aoide track identifier of the show/performance track if in the
    /// database.
    // TODO: use track identifier type
    pub track: Option<String>,
    /// The names of the dances used in the routine.
    // TODO: use dance identifier type
    pub dances: Vec<String>,
}

/// an element
#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub enum RoutineElement {
    /// Dance the given figure at this point.
    Figure(Figure),
    /// Dance the given step at this point.
    ///
    /// This assumes leader and follower are acting synchronously and leader's
    /// left foot movement means follower's right foot movement and vice versa)
    /// which represents the most common case in dancing.
    ///
    /// Use a [`RoutineElement::Figure`] if you need to represent a more
    /// complex movement pattern (even if it only represents one step).
    Step(Step),
    /// Pause for the given amount of music, meaning that no steps are
    /// performed that could be described.
    Pause(StepTiming),
    /// Ends the choreography (may be used to end it prematurely even if not
    /// the whole song would be filled, e.g., in a performance routine
    /// setting).
    Stop,
    /// Indicates that at this point the steps can be repeated from the
    /// beginning (offset from the beginning by the given amount of time).
    /// Everything following this entry in a routine can be ignored.
    Repeat(StepTiming),
    /// Add a visual separator item (optionally with a comment, e.g., “start
    /// of the long side”).
    Separator(Option<String>),
}

/// Print a timing summary for a sequence of [`RoutineElement`]s. Results in a
/// string that is segmented by figures or separate step elements.
pub fn timing_summary_for_routine<I: IntoIterator<Item = RoutineElement>>(steps: I) -> String {
    // TODO: indicate measures and phrases
    let mut summary = String::new();

    /// Modified step timing to string conversion to account for compact
    /// display (i.e., consecutive printing of multiple timings).
    fn step_timing_to_string(timing: StepTiming) -> String {
        match timing {
            StepTiming::A => 'a'.into(),
            StepTiming::And => '&'.into(),
            StepTiming::Quick => 'Q'.into(),
            StepTiming::Slow => 'S'.into(),
            StepTiming::Custom(semiquavers) => format!("{:.1}Q", semiquavers as f64 / 4.0),
        }
    }

    for element in steps {
        match element {
            RoutineElement::Figure(figure) => {
                let (leaders_steps, _) = figure.steps();
                if !leaders_steps.is_empty() {
                    for step in leaders_steps {
                        summary += &step_timing_to_string(step.timing);
                    }
                    summary += " ";
                }
            },
            RoutineElement::Step(Step { timing, .. }) | RoutineElement::Pause(timing) => {
                summary += &step_timing_to_string(timing);
                summary += " ";
            },
            RoutineElement::Stop | RoutineElement::Repeat(_) => {
                // After any of these elements, the routine is ended in practice.
                // The summary should reflect that.
                break;
            },
            RoutineElement::Separator(_) => {
                // ignore on purpose as separators do not contribute timing
            },
        }
    }

    summary.trim().to_owned()
}

/// A sequence of steps that is known as a figure, e.g., from the WDSF
/// syllabus.
///
/// Example:
///
/// ```
/// use samba_shared::routines::{
///     Figure, Foot, LevelRequirement, Step, StepTiming, WeightTransfer,
/// };
///
/// Figure::new("New Yorker to Right".into())
///     .with_steps(
///         vec![
///             Step::new(Foot::Left, StepTiming::Quick, WeightTransfer::Full),
///             Step::new(Foot::Right, StepTiming::Quick, WeightTransfer::Full),
///         ],
///         vec![
///             Step::new(Foot::Right, StepTiming::Quick, WeightTransfer::Full),
///             Step::new(Foot::Left, StepTiming::Quick, WeightTransfer::Full),
///         ],
///     )
///     .expect("The step counts are equal for leader and follower.")
///     .with_level_requirement(LevelRequirement::WdsfDCSyllabus);
/// ```
// TODO: allow skipping steps (partial execution)?
#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct Figure {
    /// A title is a figure's identifier and its main purpose (naming a
    /// sequence of steps).
    pub title: String,
    /// How to start this figure (e.g., body position, hold, direction of
    /// dance, …).
    pub start: Option<String>,
    /// How this figure will end (similar to start).
    pub end: Option<String>,
    /// General notes on this figure (e.g., hints for dancing it).
    pub notes: Vec<String>,
    /// Whether this figure is only to be danced past a certain level of
    /// dancing.
    pub level_requirement: LevelRequirement,
    // The following two fields are private to disallow setting an inconsistent
    // length. A getter and builder method are provided.
    /// The steps to perform when dancing this figure as a leader, including
    /// the specific timing used in this particular instance of the figure.
    leaders_steps: Vec<Step>,
    /// The steps to perform when dancing this figure as a follower, including
    /// the specific timing used in this particular instance of the figure.
    followers_steps: Vec<Step>,
}

impl Figure {
    #[must_use]
    pub fn new(title: String) -> Self {
        Self {
            title,
            start: None,
            end: None,
            notes: vec![],
            level_requirement: LevelRequirement::default(),
            leaders_steps: vec![],
            followers_steps: vec![],
        }
    }

    /// Add steps to this figure. Leader's steps and follower's steps must add
    /// up to the same beat count. If they do not, returns `None` (because the
    /// figure would be invalid).
    #[must_use]
    pub fn with_steps(
        mut self,
        leaders_steps: Vec<Step>,
        followers_steps: Vec<Step>,
    ) -> Option<Self> {
        let leader_step_count = calculate_beat_length(
            leaders_steps
                .iter()
                .map(|Step { timing, .. }| timing)
                .copied(),
        );
        let follower_step_count = calculate_beat_length(
            followers_steps
                .iter()
                .map(|Step { timing, .. }| timing)
                .copied(),
        );
        if leader_step_count == follower_step_count {
            self.leaders_steps = leaders_steps;
            self.followers_steps = followers_steps;
            Some(self)
        } else {
            None
        }
    }

    #[must_use]
    pub fn with_start_comment(mut self, start: String) -> Self {
        self.start = Some(start);
        self
    }

    #[must_use]
    pub fn with_end_comment(mut self, end: String) -> Self {
        self.end = Some(end);
        self
    }

    #[must_use]
    pub fn with_notes(mut self, notes: Vec<String>) -> Self {
        self.notes = notes;
        self
    }

    #[must_use]
    pub const fn with_level_requirement(mut self, level_requirement: LevelRequirement) -> Self {
        self.level_requirement = level_requirement;
        self
    }

    /// Get the leader and follower steps (in that order) for this figure.
    #[must_use]
    pub fn steps(&self) -> (&[Step], &[Step]) {
        (&self.leaders_steps, &self.followers_steps)
    }
}

/// An “atomic” step in the dance, e.g., forward walk.
#[derive(Debug, Clone, Hash, PartialEq, Eq, Serialize, Deserialize)]
pub struct Step {
    pub foot: Foot,
    pub timing: StepTiming,
    pub weight_transfer: WeightTransfer,
    pub comment: Option<String>,
}

impl Step {
    #[must_use]
    pub const fn new(foot: Foot, timing: StepTiming, weight_transfer: WeightTransfer) -> Self {
        Self {
            foot,
            timing,
            weight_transfer,
            comment: None,
        }
    }

    #[must_use]
    pub fn with_comment(mut self, comment: String) -> Self {
        self.comment = Some(comment);
        self
    }
}

/// The foot to move during a step.
#[derive(Debug, Copy, Clone, Hash, PartialEq, Eq, Serialize, Deserialize)]
pub enum Foot {
    Left,
    Right,
}

/// The amount of weight to transfer.
#[derive(Debug, Copy, Clone, Default, Hash, PartialEq, Eq, Serialize, Deserialize)]

pub enum WeightTransfer {
    /// Move foot but do not transfer (much) weight. Used for instance in
    /// taps or points.
    None,
    /// Move approximately half-way to the moved foot. Used for instance in
    /// Cucarachas.
    Half,
    /// Move the full weight to the moved foot. Used in most steps.
    #[default]
    Full,
    /// Move a custom percentage of weight to the new foot (whole percent
    /// granularity). Must be between 0 (`WeightTransfer::None`) and 100
    /// (`WeightTransfer::Full`), both bounds inclusive.
    // TODO: newtype for whole percents?
    Custom(u8),
}

/// The timing of a step, primarily affecting the duration.
// TODO: considering dances like samba with different interpretations of
//       quick, maybe break this down to the musical level?
#[derive(Debug, Copy, Clone, Hash, PartialEq, Eq, Serialize, Deserialize)]
pub enum StepTiming {
    /// A usually denotes half an `And`.
    A,
    /// And usually denotes half of the previous crotchet.
    And,
    /// A quick step, usually a crotchet.
    Quick,
    /// A slow step, usually two crotchets.
    Slow,
    /// A custom number of semiquavers (`A` elements; 4 semiquavers are one
    /// quick).
    Custom(usize),
}

impl StepTiming {
    /// Get the number of semiquavers represented by this timing unit.
    #[must_use]
    pub const fn to_semiquavers(&self) -> usize {
        match self {
            StepTiming::A => 1,
            StepTiming::And => 2,
            StepTiming::Quick => 4,
            StepTiming::Slow => 8,
            StepTiming::Custom(s) => *s,
        }
    }
}

/// Calculate the number of whole beats needed to execute the sequence of
/// timings given in the iterator.
fn calculate_beat_length<I: Iterator<Item = StepTiming>>(timings: I) -> usize {
    timings
        .fold(0, |semiquavers, timing| {
            semiquavers - semiquavers % 4 + timing.to_semiquavers()
        })
        .next_multiple_of(4)
        / 4
}

/// The level of dancing required to dance a figure on a competition.
#[derive(Debug, Copy, Clone, Default, Hash, PartialEq, Eq, Serialize, Deserialize)]
pub enum LevelRequirement {
    #[default]
    ToBeDetermined,
    WdsfDCSyllabus,
    WdsfBSyllabus,
    Open,
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn level_requirement_to_be_determined_dominates() {
        let test_routine = Routine::empty().with_steps(vec![
            RoutineElement::Figure(
                Figure::new("test".into()).with_level_requirement(LevelRequirement::WdsfDCSyllabus),
            ),
            RoutineElement::Figure(
                Figure::new("test".into()).with_level_requirement(LevelRequirement::WdsfBSyllabus),
            ),
            RoutineElement::Figure(
                Figure::new("test".into()).with_level_requirement(LevelRequirement::Open),
            ),
            RoutineElement::Pause(StepTiming::Quick),
            RoutineElement::Figure(
                Figure::new("test".into()).with_level_requirement(LevelRequirement::ToBeDetermined),
            ),
            RoutineElement::Stop,
        ]);

        assert_eq!(
            LevelRequirement::ToBeDetermined,
            test_routine.level_requirement()
        );
    }

    #[test]
    fn level_requirement_open_is_highest() {
        let test_routine = Routine::empty().with_steps(vec![
            RoutineElement::Figure(
                Figure::new("test".into()).with_level_requirement(LevelRequirement::WdsfDCSyllabus),
            ),
            RoutineElement::Figure(
                Figure::new("test".into()).with_level_requirement(LevelRequirement::WdsfBSyllabus),
            ),
            RoutineElement::Pause(StepTiming::Quick),
            RoutineElement::Figure(
                Figure::new("test".into()).with_level_requirement(LevelRequirement::Open),
            ),
            RoutineElement::Stop,
        ]);

        assert_eq!(LevelRequirement::Open, test_routine.level_requirement());
    }

    #[test]
    fn level_requirement_b_syllabus_is_larger_than_dc_syllabus() {
        let test_routine = Routine::empty().with_steps(vec![
            RoutineElement::Figure(
                Figure::new("test".into()).with_level_requirement(LevelRequirement::WdsfDCSyllabus),
            ),
            RoutineElement::Pause(StepTiming::Quick),
            RoutineElement::Figure(
                Figure::new("test".into()).with_level_requirement(LevelRequirement::WdsfBSyllabus),
            ),
            RoutineElement::Stop,
        ]);

        assert_eq!(
            LevelRequirement::WdsfBSyllabus,
            test_routine.level_requirement()
        );
    }

    #[test]
    fn level_requirement_dc_syllabus_is_stable() {
        let test_routine = Routine::empty().with_steps(vec![
            RoutineElement::Figure(
                Figure::new("test".into()).with_level_requirement(LevelRequirement::WdsfDCSyllabus),
            ),
            RoutineElement::Pause(StepTiming::Quick),
            RoutineElement::Figure(
                Figure::new("test".into()).with_level_requirement(LevelRequirement::WdsfDCSyllabus),
            ),
            RoutineElement::Stop,
        ]);

        assert_eq!(
            LevelRequirement::WdsfDCSyllabus,
            test_routine.level_requirement()
        );
    }

    #[test]
    fn beat_lengths_full_beats() {
        assert_eq!(
            1,
            calculate_beat_length(vec![StepTiming::Quick].into_iter())
        );
        assert_eq!(
            4,
            calculate_beat_length(
                vec![StepTiming::Slow, StepTiming::Quick, StepTiming::Quick].into_iter()
            )
        );
        assert_eq!(
            3,
            calculate_beat_length(
                vec![StepTiming::Custom(4), StepTiming::Quick, StepTiming::Quick].into_iter()
            )
        );
    }

    #[test]
    fn beat_lengths_with_ands() {
        assert_eq!(
            1,
            calculate_beat_length(vec![StepTiming::Custom(2), StepTiming::Quick].into_iter())
        );
        assert_eq!(
            2,
            calculate_beat_length(vec![StepTiming::Quick, StepTiming::And].into_iter())
        );
        assert_eq!(
            2,
            calculate_beat_length(
                vec![StepTiming::Quick, StepTiming::And, StepTiming::Quick].into_iter()
            )
        );
        assert_eq!(
            2,
            calculate_beat_length(
                vec![
                    StepTiming::And,
                    StepTiming::Quick,
                    StepTiming::And,
                    StepTiming::Quick
                ]
                .into_iter()
            )
        );
    }

    #[test]
    fn beat_lengths_with_as() {
        assert_eq!(
            1,
            calculate_beat_length(vec![StepTiming::Custom(1), StepTiming::Quick].into_iter())
        );
        assert_eq!(
            2,
            calculate_beat_length(vec![StepTiming::Quick, StepTiming::A].into_iter())
        );
        assert_eq!(
            2,
            calculate_beat_length(
                vec![StepTiming::Quick, StepTiming::A, StepTiming::Quick].into_iter()
            )
        );
        assert_eq!(
            2,
            calculate_beat_length(
                vec![
                    StepTiming::Quick,
                    StepTiming::And,
                    StepTiming::A,
                    StepTiming::Quick
                ]
                .into_iter()
            )
        );
        assert_eq!(
            2,
            calculate_beat_length(
                vec![
                    StepTiming::A,
                    StepTiming::Quick,
                    StepTiming::A,
                    StepTiming::Quick
                ]
                .into_iter()
            )
        );
    }

    fn default_step() -> RoutineElement {
        RoutineElement::Step(Step::new(
            Foot::Left,
            StepTiming::Quick,
            WeightTransfer::Full,
        ))
    }

    #[test]
    fn timing_summary_break_element() {
        assert_eq!(
            "",
            &timing_summary_for_routine(vec![RoutineElement::Stop, default_step()])
        );
        assert_eq!(
            "",
            &timing_summary_for_routine(vec![
                RoutineElement::Repeat(StepTiming::Quick),
                default_step()
            ])
        );
    }

    #[test]
    fn timing_summary_separate_steps() {
        assert_eq!(
            "Q Q",
            &timing_summary_for_routine(vec![default_step(), default_step()])
        );
        assert_eq!(
            "QQ QQ",
            &timing_summary_for_routine(vec![
                RoutineElement::Figure(
                    Figure::new("Test 1".into())
                        .with_steps(
                            vec![
                                Step::new(Foot::Right, StepTiming::Quick, WeightTransfer::Full),
                                Step::new(Foot::Left, StepTiming::Quick, WeightTransfer::Full),
                            ],
                            vec![
                                Step::new(Foot::Left, StepTiming::Quick, WeightTransfer::Full),
                                Step::new(Foot::Right, StepTiming::Quick, WeightTransfer::Full),
                            ],
                        )
                        .expect("The step counts are equal for leader and follower.")
                ),
                RoutineElement::Figure(
                    Figure::new("Test 1".into())
                        .with_steps(
                            vec![
                                Step::new(Foot::Right, StepTiming::Quick, WeightTransfer::Full),
                                Step::new(Foot::Left, StepTiming::Quick, WeightTransfer::Full),
                            ],
                            vec![
                                Step::new(Foot::Left, StepTiming::Quick, WeightTransfer::Full),
                                Step::new(Foot::Right, StepTiming::Quick, WeightTransfer::Full),
                            ],
                        )
                        .expect("The step counts are equal for leader and follower.")
                )
            ])
        )
    }
}
