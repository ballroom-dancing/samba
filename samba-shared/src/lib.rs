//! # samba's shared library
//!
//! This crate is intended to be the common domain model used by samba's
//! frontend and backend as well as a specification of the message types
//! sent between frontend and backend.
//!
//! As a direct consequence, there are three serialization modes relating to
//! the crate features:
//!
//! * `backend`: `Serialize` for everything sent from the backend, `Deserialize`
//!   for everything received by the backend.
//! * `frontend`: `Serialize` for everything sent to the backend, `Deserialize`
//!   for everything received from the backend.
//! * regardless of features: `Serialize` and `Deserialize` for domain objects
//!   as they will be retrieved in full from the backend and for update
//!   operations they will be sent to the backend.

use std::ops::Deref;

pub mod dancing;
pub mod events;
pub mod libraries;
pub mod playlists;
pub mod preferences;
pub mod routines;
pub mod tracks;
pub mod utils;

/// A paginated response to queries. Contains details about the pagination used
/// to get this fragment and the actual result as well as a count of the number
/// of objects to be returned if the query was not paginated.
#[derive(Clone, Debug)]
#[cfg_attr(feature = "backend", derive(serde::Serialize))]
#[cfg_attr(feature = "frontend", derive(serde::Deserialize))]
pub struct Paginated<T> {
    pub page: usize,
    pub page_size: usize,
    pub total_count: usize,
    pub result: T,
}

/// Represent a stored object `value` from the database which is identified by
/// `identifier`. Behaves as a smart pointer around `V` using `Deref` coercion
/// to ease usability.
#[derive(Clone, Debug, serde::Serialize, serde::Deserialize)]
pub struct Stored<K, V> {
    pub identifier: K,
    pub value: V,
}

impl<K, V> Deref for Stored<K, V> {
    type Target = V;

    fn deref(&self) -> &Self::Target {
        &self.value
    }
}

/// The status of a (long-running) background task executed on the server.
#[derive(Clone, Debug)]
#[cfg_attr(feature = "backend", derive(serde::Serialize))]
#[cfg_attr(feature = "frontend", derive(serde::Deserialize))]
pub enum BackgroundTaskStatus {
    Queued,
    Processing,
    FinishedSuccessfully(Option<String>),
    ExecutionError(Option<String>),
    Aborted,
}

/// The samba server version/status information communicated from backend
/// to frontend.
#[derive(Clone, Debug)]
#[cfg_attr(feature = "backend", derive(serde::Serialize))]
#[cfg_attr(feature = "frontend", derive(serde::Deserialize))]
pub struct SambaBackendVersion {
    pub version: String,
}
