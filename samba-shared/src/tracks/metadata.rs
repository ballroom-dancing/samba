use std::{borrow::Cow, num::NonZeroU8};

use aoide::{
    Track,
    tag::{FacetId, Label},
};

use crate::dancing::FACET_ID_DANCE;

/// The facet (in aoide terms) of the movie tags, i.e. where a song was used.
pub const FACET_ID_MOVIE: &FacetId<'_> = &FacetId::new_unchecked(Cow::Borrowed("movie"));

/// The facet (in aoide terms) of the kind of remix it is (e.g. house
/// remix/techno remix/…).
pub const FACET_ID_REMIX: &FacetId<'_> = &FacetId::new_unchecked(Cow::Borrowed("remix"));

/// Measures Per Minute (MPM): A faceted tag with no label and only a value
/// encoded into the score.
pub const FACET_ID_MPM: &FacetId<'_> = &FacetId::new_unchecked(Cow::Borrowed("mpm"));

/// The track's language tag when the track is instrumental.
pub const TRACK_LANGUAGE_INSTRUMENTAL: &str = "instrumental";

/// Get MPM from tags of a track. Relies on the normalized representation
/// to retrieve it.
#[must_use]
pub fn get_mpm(track: &Track) -> Option<NonZeroU8> {
    if let Some((bpm, signature)) = track.metrics.tempo_bpm.zip(track.metrics.time_signature) {
        let mpm = (bpm.value() / signature.beats_per_measure as f64).round();
        if mpm > 0.0 || mpm <= 255.0 {
            NonZeroU8::new(mpm as u8)
        } else {
            None
        }
    } else {
        track
            .tags
            .as_ref()
            .facets
            .iter()
            .find(|faceted| faceted.facet_id == *FACET_ID_MPM)
            .and_then(|faceted| {
                debug_assert!(faceted.tags.len() <= 1);
                faceted.tags.first()
            })
            .and_then(|tag| {
                debug_assert!(tag.label.is_none());
                let u8_val = tag.score.to_percentage();
                if u8_val <= 0.0 || u8_val > 255.0 {
                    return None;
                }
                NonZeroU8::new(u8_val as u8)
            })
    }
}

/// Get the primary dance (i.e. the one with highest priority) for a track, if
/// any.
#[must_use]
pub fn get_primary_dance(track: &Track) -> Option<&str> {
    track
        .tags
        .facets
        .iter()
        .filter_map(|faceted_tags| {
            (faceted_tags.facet_id == *FACET_ID_DANCE).then_some(faceted_tags.tags.iter())
        })
        .flatten()
        .reduce(|tag_with_max_score, next_tag| {
            if next_tag.score > tag_with_max_score.score {
                next_tag
            } else {
                tag_with_max_score
            }
        })
        .and_then(|tag| tag.label.as_ref().map(Label::as_str))
}
