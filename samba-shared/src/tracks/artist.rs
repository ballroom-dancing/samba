use aoide::track::actor::{Actor, ActorNamesSummarySplitter, Kind as ActorKind, Role as ActorRole};

#[cfg(test)]
mod tests;

const ACTOR_SEPARATOR_STRINGS: &[&str] = &[
    // multiple versions of “and” which are ambiguous and need some testing
    // before enabling more of them (e.g., once there is a way to deal with
    // false positive splits)
    " & ",
    // " + ",
    // " and ",
    // " with ",
    ", ", // without leading whitespace
    " ft ",
    " ft. ",
    " feat. ",
    " featuring ",
    " vs ",
    " vs. ",
];

#[derive(Debug, PartialEq)]
struct ParensParseResult<'a> {
    original_artists: Vec<String>,
    remix: Option<&'a str>,
}

/// Parse optional (single) parentheses' content of the title.
/// In “abc (def)” this would be passed the “def” part and parse
/// it for potential original artists' names and remixers.
fn parse_paren<'p>(paren: &'p str, splitter: &ActorNamesSummarySplitter) -> ParensParseResult<'p> {
    let mut result = ParensParseResult {
        original_artists: vec![],
        remix: None,
    };

    // TODO: do we want to parse this?
    let end_pos = if paren.ends_with("Remix") {
        let start_pos = paren.rfind(',').unwrap_or(0);
        let remix = paren[start_pos..]
            .strip_prefix(", ")
            .unwrap_or(&paren[start_pos..]);
        result.remix = Some(remix.trim());

        start_pos
    } else {
        paren.len()
    };

    if paren.starts_with("orig.") {
        result.original_artists = splitter
            .split_all(paren["orig.".len()..end_pos].trim())
            .map(ToOwned::to_owned)
            .collect();
    } else if !paren[..end_pos].is_empty() {
        result.original_artists = splitter
            .split_all(&paren[..end_pos])
            .map(ToOwned::to_owned)
            .collect();
    }

    result
}

/// Try to parse the `artists` according to the following structure:
///
/// * Someone → an artist with name Someone
/// * Someone ft. Somebody → an artist featuring a second one
/// * Someone (orig. Somebody) → an artist of a song which was originally
///   performed by Somebody
/// * Someone (orig. Somebody ft. …) → apply the featuring concept to original
///   artists
/// * Someone (Somebody Remix) → Someone performing in a song remixed by
///   Somebody
// TODO: parse "(A B C Remix)" and recursive features
#[must_use]
pub fn parse(artists: &str) -> Vec<Actor> {
    // For our purposes, we need a trimmed string, for instance to detect
    // if the last character closes an (orig. parenthesis expression.
    let artists = artists.trim();

    let mut parsed_actors = vec![];
    let splitter =
        ActorNamesSummarySplitter::new(ACTOR_SEPARATOR_STRINGS.iter().copied(), std::iter::empty());

    let end_pos = artists
        .find('(')
        .filter(|&pos| artists.ends_with("Remix)") || artists[pos..].starts_with("(orig."))
        .map_or(artists.len(), |pos| {
            let paren_content = &artists[(pos + 1).min(artists.len() - 1)..artists.len() - 1];
            let ParensParseResult {
                original_artists,
                remix,
            } = parse_paren(paren_content, &splitter);

            parsed_actors.extend(
                original_artists
                    .into_iter()
                    .map(|artist| Actor {
                        role: ActorRole::Performer,
                        kind: ActorKind::Individual,
                        name: artist,
                        role_notes: None,
                    })
                    .chain(remix.into_iter().map(|remixer| Actor {
                        role: ActorRole::Remixer,
                        kind: ActorKind::Individual,
                        name: remixer.trim().to_owned(),
                        role_notes: None,
                    })),
            );

            pos
        });

    parsed_actors.extend(
        splitter
            .split_all(artists[0..end_pos].trim())
            .map(|artist| Actor {
                role: ActorRole::Artist,
                kind: ActorKind::Individual,
                name: artist.trim().to_owned(),
                role_notes: None,
            }),
    );

    parsed_actors
}
