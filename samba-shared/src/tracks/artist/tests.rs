use aoide::track::actor::{Actor, ActorNamesSummarySplitter, Kind as ActorKind, Role as ActorRole};

use super::*;

fn separator_only_splitter() -> ActorNamesSummarySplitter {
    ActorNamesSummarySplitter::new(ACTOR_SEPARATOR_STRINGS.iter().copied(), std::iter::empty())
}

// Test parsing artist parentheses

#[test]
fn extract_original_artist_from_paren() {
    assert_eq!(
        ParensParseResult {
            original_artists: vec!["Me".to_string(), "You".to_string()],
            remix: None
        },
        parse_paren("orig. Me & You", &separator_only_splitter())
    );
}

#[test]
fn extract_remixer_from_paren() {
    assert_eq!(
        ParensParseResult {
            original_artists: vec![],
            remix: Some("Someone Remix")
        },
        parse_paren("Someone Remix", &separator_only_splitter())
    );
}

#[test]
fn extract_original_artist_and_remixer_from_paren() {
    assert_eq!(
        ParensParseResult {
            original_artists: vec!["Me".to_string(), "You".to_string()],
            remix: Some("Someone Remix")
        },
        parse_paren(
            "orig. Me ft. You, Someone Remix",
            &separator_only_splitter()
        )
    );
}

// Test the composite parse function

fn actor(name: &str) -> Actor {
    Actor {
        role: ActorRole::Artist,
        kind: ActorKind::Individual,
        name: name.to_owned(),
        role_notes: None,
    }
}
fn orig_actor(name: &str) -> Actor {
    Actor {
        role: ActorRole::Performer,
        kind: ActorKind::Individual,
        name: name.to_owned(),
        role_notes: None,
    }
}

#[test]
fn identify_single_artist() {
    assert_eq!(parse("My name"), vec![actor("My name")]);
}
#[test]
fn identify_featured_artist() {
    assert_eq!(
        vec![actor("My name"), actor("A guest")],
        parse("My name ft. A guest"),
    );
}

#[test]
fn identify_original_artist() {
    assert_eq!(
        vec![orig_actor("A guest"), actor("My name")],
        parse("My name (orig. A guest)"),
    );
}

#[test]
fn identify_original_and_featured_artist() {
    assert_eq!(
        vec![orig_actor("A guest"), actor("My name"), actor("You")],
        parse("My name ft. You (orig. A guest)"),
    );
}
#[test]
fn identify_composed_original_and_featured_artist() {
    assert_eq!(
        vec![
            orig_actor("A guest"),
            orig_actor("Someone else"),
            actor("My name"),
            actor("You"),
        ],
        parse("My name ft. You (orig. A guest ft. Someone else)"),
    );
}
