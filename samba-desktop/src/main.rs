#![cfg_attr(
    all(not(debug_assertions), target_os = "windows"),
    windows_subsystem = "windows"
)]

use app_dirs2::{AppDataType, data_root};
use samba_backend_websrv::start_webserver;
use tauri::{
    CustomMenuItem, Manager, Menu, Submenu, SystemTray, SystemTrayEvent, SystemTrayMenu,
    SystemTrayMenuItem,
};

struct Port(u16);

fn main() {
    let port = portpicker::pick_unused_port().expect("failed to find unused port");
    std::env::set_var("SAMBA_BACKEND_BIND_ADDRESS", format!("127.0.0.1:{port}"));

    if let Ok(data_root) = data_root(AppDataType::UserData) {
        std::env::set_var(
            "SAMBA_BACKEND_SQLITE_FILE",
            format!("{}", data_root.join("samba.sqlite").display()),
        );
        std::env::set_var(
            "AOIDE_SQLITE_FILE_ENV_VAR",
            format!("{}", data_root.join("aoide.sqlite").display()),
        );
    } else {
        eprintln!(
            "Could not determine user location for data content, defaulting to current directory."
        )
    }

    tauri::async_runtime::spawn(async { start_webserver().await });
    tauri::Builder::default()
        .manage(Port(port))
        // the application's menu bar at the top and related menu actions
        .menu(application_main_menu())
        .on_menu_event(|event| match event.menu_item_id() {
            "quit" => {
                std::process::exit(0);
            },
            "close" => {
                event.window().close().unwrap();
            },
            _ => {},
        })
        // the system tray icon and related menu actions
        // TODO: enable once dependencies figured out
        // .system_tray(system_tray())
        .on_system_tray_event(|app, event| match event {
            SystemTrayEvent::MenuItemClick { id, .. } => {
                let item_handle = app.tray_handle().get_item(&id);
                match id.as_str() {
                    "hide" => {
                        let window = app.get_window("main").unwrap();
                        window.hide().unwrap();
                        item_handle.set_title("Show").unwrap();
                    },
                    "quit" => {
                        std::process::exit(0);
                    },
                    _ => {}
                }
            }
            _ => {}
        })
        // handlers for the communication with the frontend
        .invoke_handler(tauri::generate_handler![get_backend_port])
        .build(tauri::generate_context!())
        .expect("error while building tauri application")
        // prevent closing (unless exited using the quit handler)
        .run(|_app_handle, event| match event {
            tauri::RunEvent::ExitRequested { api, .. } => {
              api.prevent_exit();
            }
            _ => {}
        });
}

fn application_main_menu() -> Menu {
    let quit = CustomMenuItem::new("quit".to_string(), "Quit");
    let close = CustomMenuItem::new("close".to_string(), "Close");
    let submenu = Submenu::new("File", Menu::new().add_item(close).add_item(quit));
    Menu::new().add_submenu(submenu)
}

fn system_tray() -> SystemTray {
    let quit = CustomMenuItem::new("quit".to_string(), "Quit");
    let hide = CustomMenuItem::new("hide".to_string(), "Hide");
    let tray_menu = SystemTrayMenu::new()
        .add_item(hide)
        .add_native_item(SystemTrayMenuItem::Separator)
        .add_item(quit);
    SystemTray::new().with_menu(tray_menu)
}

/// A command to get the port of the backend as chosen by portpicker.
#[tauri::command]
fn get_backend_port(port: tauri::State<'_, Port>) -> u16 {
    port.0
}
