use std::sync::Arc;

use aoide::{
    Collection,
    collection::MediaSourceConfig,
    media::content::{ContentPath, ContentPathConfig, VirtualFilePathConfig},
    util::url::BaseUrl,
};
use axum::{
    Json,
    body::Body,
    extract::{Path, State},
    http::StatusCode,
    response::{IntoResponse, Response},
};
use samba_backend_database::{
    GatekeeperError,
    queries::{
        QueryError,
        dances::get_all as get_all_dances,
        libraries::{
            create as create_library, delete as delete_library, get_all as get_all_libraries,
            get_by_id as get_library_by_id, update as update_library,
        },
    },
};
use samba_shared::libraries::{CreateLibraryRequestBody, Library};
use tantivy::{IndexWriter, TantivyDocument, TantivyError};
use thiserror::Error;
use url::Url;

use crate::{
    SAMBA_AOIDE_COLLECTION_KIND, SharedData,
    aoide::{
        AoideError, create_collection, decode_collection_uid, delete_collection,
        get_collection_by_id, synchronize_collection_with_file_system, update_collection,
    },
    server::{
        background_tasks::{BackgroundTask, BackgroundTaskSendError},
        data::TANTIVY_INDEX_WRITER_CAPACITY,
    },
};

mod background_scan;
use background_scan::create_postupdate_task;

#[derive(Error, Debug)]
pub enum LibraryError {
    #[error("Interaction with aoide failed for: {0:?}")]
    AoideError(#[from] AoideError),
    #[error("Failed to initiate background process: {0}")]
    BackgroundTaskError(#[from] BackgroundTaskSendError),
    #[error("Connection to the database failed: {0}")]
    DatabaseConnection(#[from] GatekeeperError),
    #[error("Querying the database failed: {0}")]
    DatabaseQuery(#[from] QueryError),
    #[error("Received bad request.")]
    ParsingRequest,
    #[error("Interacting with tantivy failed: {0}")]
    TantivyError(#[from] TantivyError),
}

impl IntoResponse for LibraryError {
    fn into_response(self) -> Response {
        let body = Body::from(format!("{:?}", self));

        Response::builder()
            .status(match self {
                LibraryError::AoideError(_) | LibraryError::DatabaseConnection(_) => {
                    StatusCode::SERVICE_UNAVAILABLE
                },
                LibraryError::ParsingRequest => StatusCode::BAD_REQUEST,
                LibraryError::DatabaseQuery(QueryError::RowIdentification(_)) => {
                    StatusCode::NOT_FOUND
                },
                _ => StatusCode::INTERNAL_SERVER_ERROR,
            })
            .body(body)
            .unwrap()
    }
}

#[tracing::instrument]
pub async fn index(State(state): State<SharedData>) -> Result<impl IntoResponse, LibraryError> {
    get_all_libraries(&state.database_gatekeeper)
        .await
        .map(Json)
        .map_err(Into::into)
}

#[tracing::instrument]
pub async fn scan_all(
    State(state): State<SharedData>,
) -> Result<(StatusCode, String), LibraryError> {
    let library_collections = get_all_libraries(&state.database_gatekeeper)
        .await?
        .into_iter()
        .map(|lib| lib.aoide_collection)
        .collect::<Vec<_>>();

    let dances = Arc::new(get_all_dances(&state.database_gatekeeper).await?);

    // TODO: query status beforehand?
    let aoide_gatekeeper = Arc::clone(&state.aoide_gatekeeper);
    let database_gatekeeper = Arc::clone(&state.database_gatekeeper);
    let tantivy_cache = Arc::clone(&state.tantivy_cache);
    let background_task_channel = Arc::clone(&state.background_channel);
    let background_task = BackgroundTask::new(tokio::task::spawn(async move {
        for collection in library_collections {
            let collection_uid = decode_collection_uid(&collection)?;

            synchronize_collection_with_file_system(
                &aoide_gatekeeper,
                &collection_uid,
                Arc::clone(&dances),
            )
            .await?;

            background_task_channel.send(
                create_postupdate_task(
                    Arc::clone(&aoide_gatekeeper),
                    Arc::clone(&database_gatekeeper),
                    Arc::clone(&tantivy_cache),
                    collection_uid,
                )
                .await?,
            )?;
        }

        Ok(None)
    }));

    let task_id = background_task.id.to_string();
    state.background_channel.send(background_task)?;

    Ok((StatusCode::OK, task_id))
}

#[tracing::instrument]
pub async fn create(
    State(state): State<SharedData>,
    Json(new): Json<CreateLibraryRequestBody>,
) -> Result<impl IntoResponse, LibraryError> {
    let url = Url::parse(&new.path).map_err(|e| {
        tracing::error!("invalid url provided: {}", e);
        LibraryError::ParsingRequest
    })?;
    let collection = Collection {
        title: new.name.clone(),
        notes: None,
        kind: Some(SAMBA_AOIDE_COLLECTION_KIND.to_owned()),
        color: None,
        media_source_config: MediaSourceConfig {
            content_path: ContentPathConfig::VirtualFilePath(VirtualFilePathConfig {
                root_url: BaseUrl::new(url),
                excluded_paths: vec![],
            }),
        },
    };

    let creation_result = create_collection(&state.aoide_gatekeeper, collection).await?;
    let collection_uid_serde: aoide::json::entity::EntityUid =
        creation_result.hdr.uid.clone().into();

    create_library(&state.database_gatekeeper, Library {
        id: new.id.clone(),
        name: new.name.clone(),
        path: new.path.clone(),
        aoide_collection: creation_result.hdr.uid.to_string(),
        scan_subdirectories: new.scan_subdirectories,
        shared: new.shared,
        excluded_directories: vec![],
    })
    .await
    .map(|_| (StatusCode::CREATED, Json(collection_uid_serde)))
    .map_err(Into::into)
}

#[tracing::instrument]
pub async fn scan_by_id(
    State(state): State<SharedData>,
    Path(library_id): Path<String>,
) -> Result<(StatusCode, String), LibraryError> {
    let library = get_library_by_id(&state.database_gatekeeper, &library_id).await?;
    let collection_uid = decode_collection_uid(&library.aoide_collection)?;
    let dances = Arc::new(get_all_dances(&state.database_gatekeeper).await?);

    let aoide_gatekeeper = Arc::clone(&state.aoide_gatekeeper);
    let database_gatekeeper = Arc::clone(&state.database_gatekeeper);
    let tantivy_cache = Arc::clone(&state.tantivy_cache);
    let background_task_channel = Arc::clone(&state.background_channel);
    let background_task = BackgroundTask::new(tokio::task::spawn(async move {
        synchronize_collection_with_file_system(&aoide_gatekeeper, &collection_uid, dances).await?;

        background_task_channel
            .send(
                create_postupdate_task(
                    Arc::clone(&aoide_gatekeeper),
                    Arc::clone(&database_gatekeeper),
                    Arc::clone(&tantivy_cache),
                    collection_uid,
                )
                .await?,
            )
            .map(|_| None)
            .map_err(Into::into)
    }));

    let task_id = background_task.id.to_string();
    state.background_channel.send(background_task)?;

    Ok((StatusCode::OK, task_id))
}

#[tracing::instrument]
pub async fn get_by_id(
    State(state): State<SharedData>,
    Path(library_id): Path<String>,
) -> Result<impl IntoResponse, LibraryError> {
    get_library_by_id(&state.database_gatekeeper, &library_id)
        .await
        .map(Json)
        .map_err(Into::into)
}

#[tracing::instrument]
pub async fn update(
    State(state): State<SharedData>,
    Path(library_id): Path<String>,
    Json(updated): Json<Library>,
) -> Result<impl IntoResponse, LibraryError> {
    if updated.id != library_id.as_str() {
        return Ok(StatusCode::BAD_REQUEST);
    }

    let current_state = get_library_by_id(&state.database_gatekeeper, &library_id).await?;
    let collection_uid = decode_collection_uid(&current_state.aoide_collection)?;
    let mut aoide_collection =
        get_collection_by_id(&state.aoide_gatekeeper, collection_uid).await?;

    let updated_url = Url::parse(&updated.path)
        .inspect_err(|_e| {
            tracing::warn!(
                "The path given for updating library {} is invalid: {}",
                updated.id,
                updated.path
            );
        })
        .map_err(|_e| LibraryError::ParsingRequest)?;
    if current_state.path != updated.path {
        // TODO: update aoide collection to point to new directory
        // TODO: add background task to rescan collection (purging orphans)
        // TODO: add background task to post-process collection files
    }
    if current_state.excluded_directories != updated.excluded_directories {
        aoide_collection.body.media_source_config = MediaSourceConfig {
            content_path: ContentPathConfig::VirtualFilePath(VirtualFilePathConfig {
                root_url: BaseUrl::new(updated_url),
                excluded_paths: updated
                    .excluded_directories
                    .iter()
                    .map(|dir| ContentPath::new(dir.to_owned().into()))
                    .collect(),
            }),
        };
    }
    // We are not interested in the updated collection in the success case
    // because we already have the UID we need for further processing.
    let _ = update_collection(&state.aoide_gatekeeper, aoide_collection).await?;

    update_library(&state.database_gatekeeper, updated)
        .await
        .map(|_| StatusCode::ACCEPTED)
        .map_err(Into::into)
}

#[tracing::instrument]
pub async fn delete(
    State(state): State<SharedData>,
    Path(library_id): Path<String>,
) -> Result<impl IntoResponse, LibraryError> {
    let library = get_library_by_id(&state.database_gatekeeper, &library_id).await?;
    let collection_uid = decode_collection_uid(&library.aoide_collection)?;

    // Delete all collection items from the index.
    let mut index_writer: IndexWriter<TantivyDocument> = state
        .tantivy_cache
        .index
        .index
        .writer(TANTIVY_INDEX_WRITER_CAPACITY)?;
    index_writer.delete_term(
        state
            .tantivy_cache
            .index
            .fields
            .collection_uid_term(&collection_uid),
    );
    index_writer.commit()?;

    // Delete the samba library and afterwards purge the aoide collection
    // now that nothing references it anymore.
    delete_library(&state.database_gatekeeper, &library_id).await?;
    delete_collection(&state.aoide_gatekeeper, collection_uid).await?;

    Ok(StatusCode::OK)
}
