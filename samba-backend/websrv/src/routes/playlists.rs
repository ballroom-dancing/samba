use aoide::{
    json::playlist::{
        Entity as SerialPlaylistEntity, EntityWithEntries as SerialPlaylistEntityWithEntries,
        PlaylistWithEntries as SerialPlaylistWithEntries,
    },
    playlist::{
        EntityWithEntries as PlaylistEntityWithEntries, Entry as AoidePlaylistEntry,
        Item as AoidePlaylistItem, Playlist as AoidePlaylist,
        PlaylistWithEntries as AoidePlaylistWithEntries, TrackItem as AoidePlaylistTrackItem,
    },
    util::clock::OffsetDateTimeMs as AoideDateTime,
};
use axum::{
    Json,
    body::Body,
    extract::{Path, Query, State},
    http::{HeaderValue, StatusCode, header},
    response::{IntoResponse, Response},
};
use samba_backend_reports::generate_playlist_export_at;
use samba_shared::playlists::{
    AudioPlayback, AudioSource, CreationFromTrackQueryParameters, Playlist, PlaylistItem,
    TRACK_QUERY_PLAYBACK_KIND,
};
use tempfile::NamedTempFile;
use thiserror::Error;
use tokio_util::io::ReaderStream;

use super::Pagination;
use crate::{
    SharedData,
    aoide::{
        AoideError, Gatekeeper as AoideGatekeeper, create_playlist_with_entries,
        decode_playlist_uid, delete_playlist, get_all_playlists, get_playlist_by_id,
        get_track_by_id, update_playlist_with_entries,
    },
    library::track_queries::{TrackQueryError, TrackQueryParameters, get_tracks_matching_query},
};

#[derive(Error, Debug)]
pub enum PlaylistError {
    #[error("Interacting with aoide failed: {0}")]
    AoideInteraction(#[from] AoideError),
    #[error("Invalid request.")]
    BadRequest,
    #[error("Failed to commit I/O transaction.")]
    IOError,
    #[error("Failed to create an export for playlist {0}")]
    ReportGeneration(String),
    #[error("Invalid track query result: {0}")]
    TrackQuery(#[from] TrackQueryError),
}

impl IntoResponse for PlaylistError {
    fn into_response(self) -> Response {
        let body = Body::from(format!("{:?}", self));

        Response::builder()
            .status(match self {
                PlaylistError::AoideInteraction(_) => StatusCode::SERVICE_UNAVAILABLE,
                PlaylistError::BadRequest => StatusCode::BAD_REQUEST,
                _ => StatusCode::INTERNAL_SERVER_ERROR,
            })
            .body(body)
            .unwrap()
    }
}

#[tracing::instrument]
pub async fn index(
    State(state): State<SharedData>,
    pagination: Option<Query<Pagination>>,
) -> Result<Json<Vec<SerialPlaylistEntityWithEntries>>, PlaylistError> {
    Ok(Json(
        get_all_playlists(&state.aoide_gatekeeper, None)
            .await?
            .into_iter()
            .map(Into::into)
            .collect(),
    ))
}

#[tracing::instrument]
pub async fn create(
    State(state): State<SharedData>,
    Json(new): Json<SerialPlaylistWithEntries>,
) -> Result<impl IntoResponse, PlaylistError> {
    create_playlist_with_entries(&state.aoide_gatekeeper, new.into())
        .await
        .map(Into::into)
        .map(|playlist: SerialPlaylistEntity| (StatusCode::CREATED, Json(playlist)))
        .map_err(Into::into)
}

#[tracing::instrument]
pub async fn create_from_query(
    State(state): State<SharedData>,
    Query(params): Query<CreationFromTrackQueryParameters>,
) -> Result<Json<SerialPlaylistEntity>, PlaylistError> {
    // TODO: we only need their header (can be more efficient)
    let entries = get_tracks_matching_query(
        &state.aoide_gatekeeper,
        &state.tantivy_cache,
        &params.query,
        TrackQueryParameters {
            offset: params.offset,
            limit: params.limit,
            randomize: params.randomize_order,
        },
    )
    .await?
    .into_iter()
    .map(|entry| AoidePlaylistEntry {
        added_at: AoideDateTime::now_utc(),
        title: None,
        notes: None,
        item: AoidePlaylistItem::Track(AoidePlaylistTrackItem {
            uid: entry.hdr.uid.clone(),
        }),
    })
    .collect();

    create_playlist_with_entries(&state.aoide_gatekeeper, AoidePlaylistWithEntries {
        playlist: AoidePlaylist {
            title: format!(
                "Playing library tracks{}",
                (!params.query.is_empty())
                    .then(|| format!(" for query “{}”", params.query))
                    .unwrap_or_default()
            ),
            kind: Some(TRACK_QUERY_PLAYBACK_KIND.into()),
            notes: None,
            color: None,
            flags: Default::default(),
        },
        entries,
    })
    .await
    .map(Into::into)
    .map(Json)
    .map_err(Into::into)
}

#[tracing::instrument]
pub async fn get_by_id(
    State(state): State<SharedData>,
    Path(playlist_id): Path<String>,
) -> Result<Json<SerialPlaylistEntityWithEntries>, PlaylistError> {
    let uid = decode_playlist_uid(&playlist_id)?;
    get_playlist_by_id(&state.aoide_gatekeeper, &uid)
        .await
        .map(Into::into)
        .map(Json)
        .map_err(Into::into)
}

#[tracing::instrument]
pub async fn export(
    State(state): State<SharedData>,
    Path(playlist_id): Path<String>,
) -> Result<impl IntoResponse, PlaylistError> {
    let uid = decode_playlist_uid(&playlist_id)?;
    let mut playlist = get_playlist_by_id(&state.aoide_gatekeeper, &uid)
        .await?
        .body
        .clone()
        .into();
    resolve_playlist_entries(&state.aoide_gatekeeper, &mut playlist).await?;

    let pdf_file = NamedTempFile::new().map_err(|e| {
        tracing::debug!("failed to create new temp file: {}", e);
        PlaylistError::IOError
    })?;
    generate_playlist_export_at(&playlist, pdf_file.path(), false)
        .await
        .map_err(|e| {
            tracing::debug!("failed to export playlist {}: {}", playlist_id, e);
            PlaylistError::ReportGeneration(playlist_id)
        })?;

    let tokio_file = tokio::fs::File::open(pdf_file.path()).await.map_err(|e| {
        tracing::debug!("failed to open PDF report: {}", e);
        PlaylistError::IOError
    })?;
    let stream = ReaderStream::new(tokio_file);
    let body = Body::from_stream(stream);

    Ok((
        StatusCode::OK,
        [
            (
                header::CONTENT_TYPE,
                HeaderValue::from_static("application/pdf"),
            ),
            (
                header::CONTENT_DISPOSITION,
                format!("attachment; filename=\"{}.pdf\"", playlist.title)
                    .try_into()
                    .unwrap_or_else(|_| {
                        HeaderValue::from_static("attachment; filename=\"playlist.pdf\"")
                    }),
            ),
        ],
        body,
    ))
}

#[tracing::instrument]
pub async fn update(
    State(state): State<SharedData>,
    Path(playlist_id): Path<String>,
    Json(updated): Json<SerialPlaylistEntityWithEntries>,
) -> Result<Json<SerialPlaylistEntity>, PlaylistError> {
    let entity: PlaylistEntityWithEntries = updated.into();
    if entity.hdr.uid.to_string() != playlist_id {
        return Err(PlaylistError::BadRequest);
    }

    update_playlist_with_entries(&state.aoide_gatekeeper, entity)
        .await
        .map(Into::into)
        .map(Json)
        .map_err(Into::into)
}

#[tracing::instrument]
pub async fn delete(
    State(state): State<SharedData>,
    Path(playlist_id): Path<String>,
) -> Result<impl IntoResponse, PlaylistError> {
    delete_playlist(&state.aoide_gatekeeper, &playlist_id)
        .await
        .map(|_| StatusCode::OK)
        .map_err(Into::into)
}

#[tracing::instrument(skip(aoide_gatekeeper))]
async fn resolve_playlist_entries(
    aoide_gatekeeper: &AoideGatekeeper,
    playlist: &mut Playlist,
) -> Result<(), PlaylistError> {
    // TODO: fetch all tracks by UID first (and collect them into hash map) to avoid
    // multiple queries

    for item in playlist.items.iter_mut().filter(|item| {
        matches!(
            item,
            PlaylistItem::Playback(AudioPlayback {
                audio_source: AudioSource::TrackQuery(_) | AudioSource::TrackUid(_),
                ..
            })
        )
    }) {
        match item {
            PlaylistItem::Playback(AudioPlayback {
                audio_source: AudioSource::TrackQuery(_),
                ..
            }) => {
                // TODO: add comment about possible match for this query (as a
                //       note/comment)?
            },
            &mut PlaylistItem::Playback(
                ref playback @ AudioPlayback {
                    audio_source: AudioSource::TrackUid(ref uid),
                    ..
                },
            ) => {
                let resolved_track =
                    get_track_by_id(aoide_gatekeeper, &uid.to_string(), false).await?;

                let mut updated_playback = playback.clone();
                updated_playback.audio_source =
                    AudioSource::ResolvedTrack(Box::new(resolved_track));
                *item = PlaylistItem::Playback(updated_playback);
            },
            _ => {
                unreachable!("These items are filtered out in the iterator.");
            },
        }
    }

    Ok(())
}
