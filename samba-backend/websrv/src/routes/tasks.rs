use std::collections::HashMap;

use axum::{
    Json,
    body::Body,
    extract::{Path, State},
    http::StatusCode,
    response::{IntoResponse, Response},
};
use samba_shared::BackgroundTaskStatus;
use thiserror::Error;
use ulid::{DecodeError as UlidDecodeError, Ulid};

use crate::SharedData;

#[derive(Error, Debug)]
pub enum TaskError {
    #[error("Task with ID {0} not found.")]
    TaskNotFound(String),
    #[error("Parsing the ID failed: {0}")]
    UlidError(#[from] UlidDecodeError),
}

impl IntoResponse for TaskError {
    fn into_response(self) -> Response {
        let body = Body::from(format!("{:?}", self));

        Response::builder()
            .status(match self {
                TaskError::TaskNotFound(_) => StatusCode::NOT_FOUND,
                TaskError::UlidError(_) => StatusCode::BAD_REQUEST,
            })
            .body(body)
            .unwrap()
    }
}

#[tracing::instrument]
pub async fn index(State(state): State<SharedData>) -> Result<impl IntoResponse, TaskError> {
    let task_map: HashMap<String, BackgroundTaskStatus> = state
        .background_tasks
        .read()
        .await
        .iter()
        .map(|(ulid, status)| (ulid.to_string(), status.clone()))
        .collect();
    Ok(Json(task_map))
}

#[tracing::instrument]
pub async fn get_by_id(
    State(state): State<SharedData>,
    Path(task_id): Path<String>,
) -> Result<Json<BackgroundTaskStatus>, TaskError> {
    let ulid = Ulid::from_string(&task_id)?;
    let status = state
        .background_tasks
        .read()
        .await
        .get(&ulid)
        .cloned()
        .ok_or_else(|| TaskError::TaskNotFound(task_id.clone()))?;

    Ok(Json(status))
}
