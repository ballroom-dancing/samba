use axum::{
    Json,
    body::Body,
    extract::{Path, State},
    http::{StatusCode, header},
    response::{IntoResponse, Response},
};
use axum_extra::headers::HeaderValue;
use samba_backend_database::{
    GatekeeperError,
    queries::{
        QueryError,
        routines::{
            create as create_routine, delete as delete_routine, get_all as get_all_routines,
            get_by_id as get_routine_by_id, update as update_routine,
        },
    },
};
use samba_backend_reports::generate_routine_export_at;
use samba_shared::routines::Routine;
use tempfile::NamedTempFile;
use thiserror::Error;
use tokio_util::io::ReaderStream;

use crate::SharedData;

#[derive(Error, Debug)]
pub enum RoutineError {
    #[error("Connection to the database failed: {0}")]
    DatabaseConnection(#[from] GatekeeperError),
    #[error("Querying the database failed: {0}")]
    DatabaseQuery(#[from] QueryError),
    #[error("Failed to commit I/O transaction.")]
    IOError,
    #[error("Failed to create a report for event {0}")]
    ReportGeneration(i32),
}

impl IntoResponse for RoutineError {
    fn into_response(self) -> Response {
        let body = Body::from(format!("{:?}", self));

        Response::builder()
            .status(match self {
                RoutineError::DatabaseConnection(_) => StatusCode::SERVICE_UNAVAILABLE,
                RoutineError::DatabaseQuery(QueryError::RowIdentification(_)) => {
                    StatusCode::NOT_FOUND
                },
                _ => StatusCode::INTERNAL_SERVER_ERROR,
            })
            .body(body)
            .unwrap()
    }
}

#[tracing::instrument]
pub async fn index(State(state): State<SharedData>) -> Result<impl IntoResponse, RoutineError> {
    get_all_routines(&state.database_gatekeeper)
        .await
        .map(Json)
        .map_err(Into::into)
}

#[tracing::instrument]
pub async fn create(
    State(state): State<SharedData>,
    Json(new): Json<Routine>,
) -> Result<impl IntoResponse, RoutineError> {
    create_routine(&state.database_gatekeeper, new)
        .await
        .map(|routine_id| (StatusCode::CREATED, Json(routine_id)))
        .map_err(Into::into)
}

#[tracing::instrument]
pub async fn get_by_id(
    State(state): State<SharedData>,
    Path(routine_id): Path<i32>,
) -> Result<impl IntoResponse, RoutineError> {
    get_routine_by_id(&state.database_gatekeeper, routine_id)
        .await
        .map(Json)
        .map_err(Into::into)
}

#[tracing::instrument]
pub async fn update(
    State(state): State<SharedData>,
    Path(routine_id): Path<i32>,
    Json(updated): Json<Routine>,
) -> Result<impl IntoResponse, RoutineError> {
    update_routine(&state.database_gatekeeper, routine_id, updated)
        .await
        .map(|_ok| StatusCode::ACCEPTED)
        .map_err(Into::into)
}

#[tracing::instrument]
pub async fn delete(
    State(state): State<SharedData>,
    Path(routine_id): Path<i32>,
) -> Result<impl IntoResponse, RoutineError> {
    delete_routine(&state.database_gatekeeper, routine_id)
        .await
        .map(|_ok| StatusCode::OK)
        .map_err(Into::into)
}

#[tracing::instrument]
pub async fn build_report(
    State(state): State<SharedData>,
    Path(routine_id): Path<i32>,
) -> Result<impl IntoResponse, RoutineError> {
    let routine = get_routine_by_id(&state.database_gatekeeper, routine_id).await?;

    let pdf_file = NamedTempFile::new().map_err(|e| {
        tracing::debug!("failed to create new temp file: {}", e);
        RoutineError::IOError
    })?;
    generate_routine_export_at(&routine, pdf_file.path())
        .await
        .map_err(|e| {
            tracing::debug!("failed to generate report for {}: {}", routine_id, e);
            RoutineError::ReportGeneration(routine_id)
        })?;

    let tokio_file = tokio::fs::File::open(pdf_file.path()).await.map_err(|e| {
        tracing::debug!("failed to open PDF report: {}", e);
        RoutineError::IOError
    })?;
    let stream = ReaderStream::new(tokio_file);
    let body = Body::from_stream(stream);

    Ok((
        StatusCode::OK,
        [
            (
                header::CONTENT_TYPE,
                HeaderValue::from_static("application/pdf"),
            ),
            (
                header::CONTENT_DISPOSITION,
                format!(
                    "attachment; filename=\"{}.pdf\"",
                    routine.title.as_deref().unwrap_or("Untitled routine")
                )
                .try_into()
                .unwrap_or_else(|_| {
                    HeaderValue::from_static("attachment; filename=\"routine.pdf\"")
                }),
            ),
        ],
        body,
    ))
}
