use std::path::Path as FsPath;

use aoide::json::track::Entity as SerialTrackEntity;
use axum::{
    Json,
    body::Body,
    extract::{Path, Query, State},
    http::{Request, StatusCode, header},
    response::{IntoResponse, Response},
};
use axum_extra::headers::HeaderValue;
use samba_backend_database::{
    GatekeeperError,
    queries::{
        QueryError, libraries::get_all as get_all_libraries,
        track_cache::get_waveform_for_track as get_waveform_json,
    },
};
use samba_shared::{Paginated, tracks::WebsurferJsWaveformData};
use thiserror::Error;
use tower::ServiceExt;
use tower_http::services::ServeFile;

use super::Pagination;
use crate::{
    SharedData,
    aoide::get_track_by_id,
    library::track_queries::{
        TrackQueryError, TrackQueryParameters as MatchingTrackQueryParameters,
        count_tracks_matching_query, get_tracks_matching_query,
    },
};

mod artwork;
use artwork::{convert_color_to_artwork, extract_artwork};
mod queries;
use queries::{preprocess_track_query, surround_query_with_collections_ids};

#[derive(Error, Debug)]
pub enum TrackError {
    #[error("Fetching or processing the artwork failed: {0}")]
    ArtworkError(#[from] image::ImageError),
    #[error("Connection to the database failed: {0}")]
    DatabaseConnection(#[from] GatekeeperError),
    #[error("Query the database failed: {0}")]
    DatabaseQuery(#[from] QueryError),
    #[error("Requested track has not been found at {0}.")]
    FileNotFound(String),
    #[error("Fetching the track lacks file system path information.")]
    MissingPathResolution,
    #[error("Fetching specific track {0} failed.")]
    TrackIdentification(String),
    #[error("Error while matching tracks for query: {0}")]
    TrackQuery(#[from] TrackQueryError),
    #[error("Converting between types failed.")]
    TypeConversion,
}

impl IntoResponse for TrackError {
    fn into_response(self) -> Response {
        let body = Body::from(format!("{:?}", self));

        Response::builder()
            .status(match self {
                TrackError::FileNotFound(_)
                | TrackError::TrackIdentification(_)
                | TrackError::DatabaseQuery(QueryError::RowIdentification(_)) => {
                    StatusCode::NOT_FOUND
                },
                _ => StatusCode::INTERNAL_SERVER_ERROR,
            })
            .body(body)
            .unwrap()
    }
}

#[derive(Debug, serde::Deserialize)]
pub struct TrackQueryParameters {
    query: Option<String>,
    #[serde(flatten)]
    pagination: Pagination,
}

#[tracing::instrument]
pub async fn index(
    State(state): State<SharedData>,
    parameters: Query<TrackQueryParameters>,
) -> Result<Json<Paginated<Vec<SerialTrackEntity>>>, TrackError> {
    // Get all libraries a user has access to. This is used for the track query
    // preprocessing. Not only to translate `lib:…` queries into aoide
    // collection UIDs but also to prepend a query on all available collections
    // to the user query, in turn enabling user scoping on the omniscient track
    // index (and increasing robustness against an inconsistent samba DB <->
    // tantivy cache state).
    let libraries = get_all_libraries(&state.database_gatekeeper).await?;

    let query = parameters.query.clone().unwrap_or_default();
    let processed_query = preprocess_track_query(
        &state.database_gatekeeper,
        &libraries,
        &surround_query_with_collections_ids(&libraries, query.trim()),
    )
    .await?;
    tracing::debug!("Processed filter query '{query}' into '{processed_query}'");

    let total_count = count_tracks_matching_query(&state.tantivy_cache, &processed_query).await?;
    let page_size = parameters
        .pagination
        .page_size
        .min(total_count)
        // It turns out that the top collector panics (!) if the limit is 0, so
        // we have to ensure that even empty queries would allocate at least a
        // buffer for one element to be collected even if there won't be any.
        .max(1);

    let tracks = get_tracks_matching_query(
        &state.aoide_gatekeeper,
        &state.tantivy_cache,
        &processed_query,
        MatchingTrackQueryParameters {
            offset: page_size * parameters.pagination.page.saturating_sub(1),
            limit: Some(page_size),
            randomize: false,
        },
    )
    .await?;

    let track_result: Vec<SerialTrackEntity> = tracks.into_iter().map(Into::into).collect();

    Ok(Json(Paginated {
        page: parameters.pagination.page,
        page_size,
        total_count,
        result: track_result,
    }))
}

#[derive(Debug, serde::Deserialize)]
pub struct RandomTrackQueryParameters {
    query: Option<String>,
    number_of_tracks: usize,
}

#[tracing::instrument]
pub async fn random(
    State(state): State<SharedData>,
    parameters: Query<RandomTrackQueryParameters>,
) -> Result<Json<Vec<SerialTrackEntity>>, TrackError> {
    // The basic structure follows the `index` method, see there for
    // implementation details.
    let libraries = get_all_libraries(&state.database_gatekeeper).await?;

    let query = parameters.query.clone().unwrap_or_default();
    let processed_query = preprocess_track_query(
        &state.database_gatekeeper,
        &libraries,
        &surround_query_with_collections_ids(&libraries, query.trim()),
    )
    .await?;
    tracing::debug!("Processed filter query '{query}' into '{processed_query}'");

    let random_tracks = get_tracks_matching_query(
        &state.aoide_gatekeeper,
        &state.tantivy_cache,
        &processed_query,
        MatchingTrackQueryParameters {
            offset: 0,
            limit: Some(parameters.number_of_tracks),
            randomize: true,
        },
    )
    .await?;

    let track_result: Vec<SerialTrackEntity> = random_tracks.into_iter().map(Into::into).collect();

    Ok(Json(track_result))
}

#[tracing::instrument]
pub async fn get_by_id(
    State(state): State<SharedData>,
    Path(track_id): Path<String>,
) -> Result<Json<SerialTrackEntity>, TrackError> {
    get_track_by_id(&state.aoide_gatekeeper, &track_id, false)
        .await
        .map(SerialTrackEntity::from)
        .map(Json)
        .map_err(|e| {
            tracing::error!("Failed to reach aoide to resolve {track_id}: {}", e);
            TrackError::TrackIdentification(track_id)
        })
}

/// Get a track by its file system `path`. The content disposition file
/// name will be determined by looking at `file_name`, then the file name
/// component of `path` or default to some unspecific title.
async fn get_file_by_path(
    path: &FsPath,
    file_name: Option<String>,
) -> Result<Response, TrackError> {
    let file_exists = tokio::fs::metadata(&path).await.is_ok();
    tracing::debug!(
        "Determined file path {} that exists: {}",
        path.to_string_lossy(),
        file_exists
    );
    if !file_exists {
        return Err(TrackError::FileNotFound(path.display().to_string()));
    }

    let req = Request::builder()
        .body(Body::empty())
        .expect("Building a request with empty body cannot fail.");
    let res = ServeFile::new(path).oneshot(req).await.map_err(|e| {
        tracing::error!(
            "Failed to prepare oneshot serve request for {}: {e}",
            path.display()
        );
        // Cannot convert e (of type Infallible) to any useful error type.
        TrackError::TypeConversion
    })?;

    Ok((
        [(
            header::CONTENT_DISPOSITION,
            HeaderValue::from_str(&format!(
                "inline; filename=\"{}{}\"",
                file_name.unwrap_or_else(|| path
                    .file_name()
                    .and_then(|name| name.to_str())
                    .unwrap_or("Unknown file")
                    .to_string()),
                path.extension()
                    .and_then(|ext| ext.to_str().map(|s| format!(".{}", s)))
                    .unwrap_or_else(|| "".into())
            ))
            .map_err(|e| {
                tracing::error!(
                    "Failed to generate valid header for file serving of {}: {e}",
                    path.display()
                );
                TrackError::TypeConversion
            })?,
        )],
        res.map(Body::new),
    )
        .into_response())
}

#[tracing::instrument]
pub async fn get_file_by_id(
    State(state): State<SharedData>,
    Path(track_id): Path<String>,
) -> Result<impl IntoResponse, TrackError> {
    let (track_title, content_url) = get_track_by_id(&state.aoide_gatekeeper, &track_id, true)
        .await
        .map(|track| {
            track.body.content_url.clone().map(|url| {
                (
                    track.body.track.track_title().map(|title| title.to_owned()),
                    url,
                )
            })
        })
        .map_err(|e| {
            tracing::error!("failed to reach aoide: {}", e);
            TrackError::TrackIdentification(track_id)
        })?
        .ok_or(TrackError::MissingPathResolution)?;
    let file_path = content_url.to_file_path().map_err(|_unit_error| {
        tracing::error!("converting path from aoide to file path failed");
        TrackError::TypeConversion
    })?;
    get_file_by_path(&file_path, track_title).await
}

/// This returns a valid HTML `img` `src` attribute.
#[tracing::instrument]
pub async fn get_artwork_for_track(
    State(state): State<SharedData>,
    Path(track_id): Path<String>,
) -> Result<impl IntoResponse, TrackError> {
    let track = get_track_by_id(&state.aoide_gatekeeper, &track_id, true)
        .await
        .map_err(|e| {
            tracing::error!("failed to reach aoide: {}", e);
            TrackError::TrackIdentification(track_id.clone())
        })?;

    if let Some(ref artwork) = track.body.track.media_source.artwork {
        tracing::trace!("Found artwork in media source: {artwork:?}");
        // TODO: handle remote tracks
        if let Some(url) = track
            .body
            .content_url
            .as_ref()
            .and_then(|url| url.to_file_path().ok())
        {
            if let Ok(image) = extract_artwork(&url, artwork).await {
                return Ok(image.into_response());
            }
        }
    }

    if let Some(ref color) = track.body.track.color {
        if let Ok(image) = convert_color_to_artwork(color).await {
            return Ok(image.into_response());
        }
    }

    Err(TrackError::TrackIdentification(track_id))
}

#[tracing::instrument]
pub async fn get_waveform_for_track(
    State(state): State<SharedData>,
    Path(track_id): Path<String>,
) -> Result<impl IntoResponse, TrackError> {
    if let Some(ref waveform) = get_waveform_json(&state.database_gatekeeper, &track_id).await? {
        Ok(Json(
            serde_json::from_str::<WebsurferJsWaveformData>(waveform).map_err(|e| {
                tracing::error!(
                    "Received invalid waveform JSON for track {track_id} from database: {e}"
                );
                TrackError::TypeConversion
            })?,
        ))
    } else {
        Err(TrackError::TrackIdentification(track_id))
    }
}
