use std::path::Path;

use aoide::search_index_tantivy::{IndexStorage, TrackIndex};
use color_eyre::{
    Result,
    eyre::{Context, eyre},
};
use tantivy::query::QueryParser;
use tracing_error::ErrorLayer;
use tracing_subscriber::{EnvFilter, Registry, layer::SubscriberExt, util::SubscriberInitExt};
use tracing_tree::HierarchicalLayer;

use super::data::TantivyCache;
use crate::{SAMBA_LOG_FILE_ENV_VAR, SAMBA_TANTIVY_INDEX_DIR_ENV_VAR};

/// Initialize logging to stdout and file.
///
/// Mostly adapted from <https://fasterthanli.me/articles/request-coalescing-in-async-rust>.
pub fn initialize_logging() -> Result<()> {
    let log_layers = Registry::default()
        .with(ErrorLayer::default())
        .with(
            dotenvy::var("RUST_LOG")
                .map_or_else(|_| Ok(EnvFilter::new("info")), EnvFilter::try_new)
                .unwrap_or_else(|_| EnvFilter::new("info")),
        )
        .with(
            HierarchicalLayer::new(2)
                .with_targets(true)
                .with_bracketed_fields(true),
        );

    if let Ok(log_path) = dotenvy::var(SAMBA_LOG_FILE_ENV_VAR) {
        let log_file = std::fs::File::create(&log_path).context("Creating log file failed")?;
        log_layers
            .with(
                tracing_subscriber::fmt::layer()
                    .json()
                    .with_writer(log_file),
            )
            .init();
        tracing::info!("Starting logging into log file {log_path} ({SAMBA_LOG_FILE_ENV_VAR})");
    } else {
        log_layers.init();
    }

    Ok(())
}

/// Check whether a given `executable` can be run, i.e. is in the runtime path
/// accessible to the server. Useful to determine if runtime dependencies are
/// missing.
pub async fn check_program_in_path(executable: &str) -> bool {
    use std::process::Stdio;
    match tokio::process::Command::new(executable)
        .stdout(Stdio::null())
        .stderr(Stdio::null())
        .spawn()
    {
        Ok(mut handle) => {
            // Regardless of the result of the wait, e.g. error exit code of
            // the execution, we know at this point that the executable is
            // available in the PATH.
            let _ = handle.wait().await;
            true
        },
        Err(_) => false,
    }
}

// Try to initialize a tantivy track index based on the aoide data, which
// is unpopulated until actively populated from samba (not aoide).
pub fn open_tantivy_cache() -> Result<TantivyCache> {
    // `path_hack` is a workaround for the lifetime required to store the
    // environment variable in the `IndexStorage::FileDir` because solutions
    // in the if-let scope have been rejected by the borrow checker.
    let path_hack;
    let track_index_storage = if let Ok(index_dir) = dotenvy::var(SAMBA_TANTIVY_INDEX_DIR_ENV_VAR) {
        path_hack = Path::new(&index_dir).to_owned();
        IndexStorage::FileDir {
            dir_path: &path_hack,
        }
    } else {
        tracing::info!("No tantivy index directory provided, using temporary directory.");
        IndexStorage::TempDir
    };
    let track_index = TrackIndex::open_or_recreate(track_index_storage)
        .map_err(|e| eyre!("Error while creating track index: {e:?}"))?;
    let mut query_parser = QueryParser::for_index(&track_index.index, vec![
        track_index.fields.track_title,
        track_index.fields.track_artist,
        track_index.fields.album_title,
        track_index.fields.album_artist,
        track_index.fields.genre,
    ]);
    // Users expect queries like `dance:Samba mpm:50` to select Samba titles
    // with MPM 23, not any title that is a Samba or has 50 MPM (which would
    // match most Quicksteps as well).
    query_parser.set_conjunction_by_default();

    Ok(TantivyCache {
        index: track_index,
        parser: query_parser,
    })
}
