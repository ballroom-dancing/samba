use std::{
    collections::{HashMap, VecDeque},
    sync::Arc,
};

use samba_shared::BackgroundTaskStatus;
use tokio::{runtime::Runtime as TokioRuntime, sync::RwLock};
use ulid::Ulid;

pub type BackgroundTaskHandle = tokio::task::JoinHandle<color_eyre::Result<Option<String>>>;
pub type BackgroundTaskSender = tokio::sync::mpsc::UnboundedSender<BackgroundTask>;
pub type BackgroundTaskSendError = tokio::sync::mpsc::error::SendError<BackgroundTask>;

#[derive(Debug)]
pub struct BackgroundTask {
    pub id: Ulid,
    pub join_handle: BackgroundTaskHandle,
}

impl BackgroundTask {
    #[must_use]
    pub fn new(join_handle: BackgroundTaskHandle) -> Self {
        Self {
            id: Ulid::new(),
            join_handle,
        }
    }
}

/// Create a thread to process tasks in background which may be passed in using
/// `task_channel_receiver`. Status updates will be posted to
/// `background_tasks`. The thread can be gracefully shut down using
/// `shutdown_channel_receiver`.
pub fn create_background_task_thread(
    background_tasks: Arc<RwLock<HashMap<Ulid, BackgroundTaskStatus>>>,
    task_channel_receiver: tokio::sync::mpsc::UnboundedReceiver<BackgroundTask>,
    shutdown_channel_receiver: tokio::sync::oneshot::Receiver<()>,
) -> std::thread::JoinHandle<()> {
    std::thread::spawn(move || {
        tracing::info!("Starting listening for background tasks.");

        let runtime = match TokioRuntime::new() {
            Ok(runtime) => runtime,
            Err(e) => {
                tracing::error!(
                    "Failed to create new tokio runtime for background tasks: {}",
                    e
                );
                return;
            },
        };

        runtime.block_on(process_background_tasks(
            background_tasks,
            task_channel_receiver,
            shutdown_channel_receiver,
        ));

        tracing::debug!("Finished async processing, exiting background thread.");
    })
}

async fn process_background_tasks(
    background_tasks: Arc<RwLock<HashMap<Ulid, BackgroundTaskStatus>>>,
    mut task_channel_receiver: tokio::sync::mpsc::UnboundedReceiver<BackgroundTask>,
    mut shutdown_channel_receiver: tokio::sync::oneshot::Receiver<()>,
) {
    let mut job_queue: VecDeque<BackgroundTask> = VecDeque::new();
    loop {
        let current_task = job_queue.pop_front();
        let current_task_id = current_task.as_ref().map(|task| task.id);
        let current_task_handle = current_task.map(|task| task.join_handle);

        tokio::select! {
            result = async {
                let task_id = current_task_id.expect("async blocks are lazy so the task must exist");
                {
                    background_tasks
                        .write()
                        .await
                        .insert(task_id, BackgroundTaskStatus::Processing);
                }
                tracing::debug!("Executing task with ID {task_id}");
                current_task_handle.expect("async blocks are lazy so the task must exist").await
            }, if current_task_handle.is_some() => {
                let task_id = current_task_id.expect("the task has been executed, hence it exists");
                match result {
                    Ok(result) => {
                        background_tasks.write().await.insert(task_id, match result {
                            Ok(ret) => BackgroundTaskStatus::FinishedSuccessfully(ret),
                            Err(e) => BackgroundTaskStatus::ExecutionError(Some(format!("{e}"))),
                        });
                    },
                    Err(e) => {
                        tracing::error!("Failed to join task with ID {}: {:?}", task_id, e);
                    }
                };
            },
            Some(task) = task_channel_receiver.recv() => {
                tracing::debug!("Received a new background task with ID {}. Adding to {} jobs in queue.", &task.id, job_queue.len());
                background_tasks.write().await.insert(task.id, BackgroundTaskStatus::Queued);
                job_queue.push_back(task);
            },
            _ = &mut shutdown_channel_receiver => {
                tracing::info!("Received shutdown signal for background tasks. Exiting with {} jobs unfinished.", job_queue.len());
                break;
            }
        };
    }

    let mut task_map = background_tasks.write().await;
    for remaining_job in job_queue {
        (*task_map).insert(remaining_job.id, BackgroundTaskStatus::Aborted);
    }
}
