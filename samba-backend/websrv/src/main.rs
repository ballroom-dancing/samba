use color_eyre::Result;

/// Main function running the logic from the library implementation. See the
/// documentation of [crate::main] and the library in general for details.
#[tokio::main]
async fn main() -> Result<()> {
    samba_backend_websrv::start_webserver().await
}
