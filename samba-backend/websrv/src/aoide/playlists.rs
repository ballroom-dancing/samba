use aoide::{
    Playlist, PlaylistEntity, PlaylistUid,
    api::Pagination,
    backend_embedded::playlist::{
        create as create_aoide_playlist, entries::patch as patch_playlist,
        load_all as load_all_playlists, load_one as load_playlist, purge as delete_aoide_playlist,
        update as update_aoide_playlist,
    },
    playlist::{EntityWithEntries as PlaylistEntityWithEntries, PlaylistWithEntries},
    repo::playlist::KindFilter,
    usecases::playlist::entries::PatchOperation,
};

use super::{AoideError, Gatekeeper};

#[tracing::instrument(skip(gatekeeper))]
pub async fn get_playlist_by_id(
    gatekeeper: &Gatekeeper,
    playlist_id: &PlaylistUid,
) -> Result<PlaylistEntityWithEntries, AoideError> {
    load_playlist(gatekeeper, playlist_id.clone())
        .await
        .map_err(|e| {
            tracing::debug!("Failed to load playlist for {playlist_id}: {e}");
            AoideError::EntityNotFound(playlist_id.to_string())
        })
}

#[tracing::instrument(skip(gatekeeper))]
pub async fn create_playlist(
    gatekeeper: &Gatekeeper,
    playlist: Playlist,
) -> Result<PlaylistEntity, AoideError> {
    create_aoide_playlist(gatekeeper, None, playlist)
        .await
        .map_err(|e| {
            tracing::debug!("Creating playlist failed: {e}");
            AoideError::DatabaseConnectionFailed
        })
}

#[tracing::instrument(skip(gatekeeper))]
pub async fn create_playlist_with_entries(
    gatekeeper: &Gatekeeper,
    playlist: PlaylistWithEntries,
) -> Result<PlaylistEntity, AoideError> {
    let entity = create_aoide_playlist(gatekeeper, None, playlist.playlist)
        .await
        .map_err(|e| {
            tracing::debug!("Creating playlist failed: {e}");
            AoideError::DatabaseConnectionFailed
        })?;

    let operations = vec![PatchOperation::Append {
        entries: playlist.entries,
    }];
    patch_playlist(gatekeeper, entity.hdr.clone(), operations)
        .await
        .map_err(|e| {
            tracing::debug!("Patching {} failed: {e}", entity.hdr.uid.to_string());
            AoideError::PatchingFailed(entity.hdr.uid.to_string())
        })?;

    Ok(entity)
}

#[tracing::instrument(skip(gatekeeper))]
pub async fn update_playlist(
    gatekeeper: &Gatekeeper,
    playlist: PlaylistEntity,
) -> Result<PlaylistEntity, AoideError> {
    update_aoide_playlist(gatekeeper, playlist.hdr.clone(), playlist.body.clone())
        .await
        .map_err(|e| {
            tracing::debug!("Updating playlist failed: {}", e);
            AoideError::DatabaseConnectionFailed
        })
}

#[tracing::instrument(skip(gatekeeper))]
pub async fn update_playlist_with_entries(
    gatekeeper: &Gatekeeper,
    playlist: PlaylistEntityWithEntries,
) -> Result<PlaylistEntity, AoideError> {
    let entity = update_aoide_playlist(
        gatekeeper,
        playlist.hdr.clone(),
        playlist.body.playlist.clone(),
    )
    .await
    .map_err(|e| {
        tracing::debug!("Updating playlist failed: {}", e);
        AoideError::DatabaseConnectionFailed
    })?;

    let operations = vec![PatchOperation::RemoveAll, PatchOperation::Append {
        entries: playlist.body.entries.clone(),
    }];
    patch_playlist(gatekeeper, entity.hdr.clone(), operations)
        .await
        .map_err(|e| {
            tracing::debug!("Patching {} failed: {e}", entity.hdr.uid.to_string());
            AoideError::PatchingFailed(entity.hdr.uid.to_string())
        })?;

    Ok(entity)
}

#[tracing::instrument(skip(gatekeeper))]
pub async fn delete_playlist(gatekeeper: &Gatekeeper, playlist_id: &str) -> Result<(), AoideError> {
    let uid = playlist_id.parse::<PlaylistUid>().map_err(|e| {
        tracing::debug!("Failed to decode entity uid from {}: {}", playlist_id, e);
        AoideError::TypeConversionFailed
    })?;
    delete_aoide_playlist(gatekeeper, uid).await.map_err(|e| {
        tracing::debug!("Removing playlist {} failed: {}", playlist_id, e);
        AoideError::EntityNotFound(playlist_id.to_string())
    })
}

#[tracing::instrument(skip(gatekeeper))]
pub async fn get_all_playlists(
    gatekeeper: &Gatekeeper,
    kind: Option<String>,
) -> Result<Vec<PlaylistEntityWithEntries>, AoideError> {
    // Explicitly define an offset with no limit to prevent using
    // the default limit if no pagination is given!
    let no_pagination = Pagination {
        offset: Some(0),
        limit: None, // unlimited
    };

    let kind_filter = kind.map(|kind| KindFilter::Equal(kind.into()));
    let playlists = load_all_playlists(gatekeeper, None, kind_filter, Some(no_pagination))
        .await
        .map_err(|e| {
            tracing::debug!("Failed to fetch playlists from aoide: {e}");
            AoideError::PlaylistListingFailed
        })?;

    let mut playlists_with_entries = vec![];
    for playlist in playlists {
        playlists_with_entries.push(
            load_playlist(gatekeeper, playlist.entity.hdr.uid.clone())
                .await
                .map_err(|_load_error| {
                    AoideError::EntityNotFound(playlist.entity.hdr.uid.to_string())
                })?,
        );
    }

    Ok(playlists_with_entries)
}
