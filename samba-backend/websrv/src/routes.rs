use std::sync::Arc;

use axum::{
    Router,
    extract::{Request, State},
    http::{self, StatusCode},
    middleware::Next,
    response::IntoResponse,
    routing::{get, post},
};
use axum_extra::extract::CookieJar;
use jsonwebtoken::Validation;
use serde_with::{DisplayFromStr, serde_as};

use crate::{
    SharedData,
    server::data::{Claims, Keys},
};

mod dances;
mod events;
mod libraries;
mod management;
mod playlists;
mod routines;
mod tasks;
mod tracks;

// Axum uses serde_urlencoded which has problems with flattening. As flattening
// is very common for the pagination struct, the workaround using `serde_as`
// has to be applied as the corresponding issue is still unresolved:
// <https://github.com/nox/serde_urlencoded/issues/33>
#[serde_as]
#[derive(Debug, serde::Deserialize)]
pub struct Pagination {
    #[serde_as(as = "DisplayFromStr")]
    page: usize,
    #[serde_as(as = "DisplayFromStr")]
    page_size: usize,
}

pub fn get_application_router(jwt_keys: &Arc<Keys>) -> Router<SharedData> {
    let dance_routes = Router::new().route("/", get(dances::index));

    let single_event_routes = Router::new()
        .route(
            "/",
            get(events::get_by_id)
                .put(events::update)
                .delete(events::delete),
        )
        .route("/report", get(events::build_report));
    let event_routes = Router::new()
        .route("/", get(events::index))
        .route("/create", post(events::create))
        .nest("/{event_id}", single_event_routes);

    let single_library_routes = Router::new()
        .route(
            "/",
            get(libraries::get_by_id)
                .put(libraries::update)
                .delete(libraries::delete),
        )
        .route("/scan", get(libraries::scan_by_id));
    let library_routes = Router::new()
        .route("/", get(libraries::index))
        .route("/create", post(libraries::create))
        .route("/scan", get(libraries::scan_all))
        .nest("/{library_id}", single_library_routes);

    let single_playlist_routes = Router::new()
        .route(
            "/",
            get(playlists::get_by_id)
                // This update is not idempotent (may change entity revision in aoide).
                .post(playlists::update)
                .delete(playlists::delete),
        )
        .route("/export", get(playlists::export));
    let playlist_routes = Router::new()
        .route("/", get(playlists::index))
        .route(
            "/create",
            get(playlists::create_from_query).post(playlists::create),
        )
        .nest("/{playlist_id}", single_playlist_routes);

    let single_routine_routes = Router::new()
        .route(
            "/",
            get(routines::get_by_id)
                .put(routines::update)
                .delete(routines::delete),
        )
        .route("/report", get(routines::build_report));
    let routine_routes = Router::new()
        .route("/", get(routines::index))
        .route("/create", post(routines::create))
        .nest("/{routine_id}", single_routine_routes);

    // TODO: allow to cancel tasks
    let task_routes = Router::new()
        .route("/", get(tasks::index))
        .route("/{task_id}", get(tasks::get_by_id));

    let track_routes = Router::new()
        .route("/", get(tracks::index))
        .route("/random", get(tracks::random))
        .route("/{track_id}", get(tracks::get_by_id))
        .route("/{track_id}/artwork", get(tracks::get_artwork_for_track))
        .route("/{track_id}/file", get(tracks::get_file_by_id))
        .route("/{track_id}/waveform", get(tracks::get_waveform_for_track));

    let user_routes = Router::new()
        .route("/logout", get(management::logout))
        .route(
            "/preferences",
            get(management::preferences).put(management::store_preferences),
        );

    let protected_routes = Router::new()
        .nest("/dances", dance_routes)
        .nest("/events", event_routes)
        .nest("/libraries", library_routes)
        .nest("/playlists", playlist_routes)
        .nest("/routines", routine_routes)
        .nest("/tasks", task_routes)
        .nest("/tracks", track_routes)
        .nest("/user", user_routes)
        .route_layer(axum::middleware::from_fn_with_state(
            Arc::clone(jwt_keys),
            authorize,
        ));

    let admin_routes = Router::new()
        .route("/shutdown", get(management::shutdown))
        .route_layer(axum::middleware::from_fn_with_state(
            Arc::clone(jwt_keys),
            authorize,
        ));

    Router::new()
        // add public management routes
        .route("/version", get(management::version))
        .route("/login", post(management::login))
        // add protected routes
        .merge(admin_routes)
        .merge(protected_routes)
}

/// Authorize a user by checking for a JWT bearer header, falling back to
/// trying to read a cookie if that fails.
///
/// Users of the endpoints are encouraged to use the actual bearer
/// authentication, however, browsers will make requests where this is
/// impractical, such as for img-tags, hence the cookie fallback.
async fn authorize(
    State(keys): State<Arc<Keys>>,
    mut req: Request,
    next: Next,
) -> Result<impl IntoResponse, StatusCode> {
    let jwt = if let Some(bearer) = req
        .headers()
        .get(http::header::AUTHORIZATION)
        .and_then(|header| header.to_str().ok())
    {
        tracing::trace!("Trying to extract JWT from bearer.");
        bearer
            .strip_prefix("Bearer ")
            .filter(|token| !token.is_empty())
            .ok_or(StatusCode::UNAUTHORIZED)?
            .to_owned()
    } else {
        tracing::trace!("Trying to extract JWT from cookies.");
        let jar = CookieJar::from_headers(req.headers());
        let cookie = jar.get("jwt").ok_or(StatusCode::UNAUTHORIZED)?;

        cookie.value().to_owned()
    };
    tracing::trace!("Extracted JWT from bearer or cookie '{jwt}'");

    let current_user =
        jsonwebtoken::decode::<Claims>(&jwt, &keys.decoding, &Validation::new(Default::default()))
            .map_err(|validation_error| {
                tracing::debug!("Failed to validate JWT: {validation_error}");
                StatusCode::UNAUTHORIZED
            })?
            .claims;

    // Insert the current user's claims into the request extensions so that
    // handlers which rely on them can extract them.
    tracing::debug!("Extracted current user from request: {current_user:?}");
    req.extensions_mut().insert(current_user);
    Ok(next.run(req).await)
}
