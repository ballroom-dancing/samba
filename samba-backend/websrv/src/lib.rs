//! # Samba's backend server
//!
//! ## Embedding
//!
//! This project is provided as library and as binary crate. The use of the
//! library is only supported for embedding it into the desktop application.
//! There will be no further support for embedded usage. When in doubt, run
//! the binary.
//!
//! ## Configuration
//!
//! The configuration is performed exclusively by environment variables, there
//! is no command-line interface. The following environment variables are
//! respected:
//!
//! * `RUST_LOG` in its ecosystem-wide accepted form (see [the tracing
//!   docs][tracing-envfilter] for more details).
//! * `SAMBA_BACKEND_LOG_FILE` to determine the location of a log file. If none
//!   is provided, no log file will be written.
//! * `SAMBA_BACKEND_BIND_ADDRESS` to determine the address and port to bind the
//!   server to. Defaults to `127.0.0.1:8081` if not provided.
//! * `SAMBA_BACKEND_SQLITE_FILE` to determine the location of the database
//!   file. Defaults to `./samba.sqlite` if not provided.
//! * `AOIDE_SQLITE_FILE_ENV_VAR` to determine the location of the aoide
//!   database to use. Defaults to `./aoide.sqlite` if not provided.
//!
//! [tracing-envfilter]: https://docs.rs/tracing-subscriber/latest/tracing_subscriber/filter/struct.EnvFilter.html
#![feature(iter_intersperse)]

use std::{collections::HashMap, net::SocketAddr, sync::Arc, time::Duration};

use axum::extract::Request;
use color_eyre::{Result, eyre::eyre};
use hyper::body::Incoming;
use hyper_util::rt::TokioIo;
use samba_backend_database::initialize_and_migrate_database;
use tokio::{net::TcpListener, sync::RwLock};
use tower::Service;
use tower_http::{
    compression::CompressionLayer, cors::CorsLayer, decompression::DecompressionLayer,
    timeout::TimeoutLayer, trace::TraceLayer,
};

mod aoide;
use crate::aoide::provision_aoide;
mod library;
mod routes;
mod server;
use server::{
    background_tasks::{BackgroundTask, create_background_task_thread},
    data::{Keys, SharedData},
    initialization::{check_program_in_path, initialize_logging, open_tantivy_cache},
    shutdown_signal,
};
#[cfg(test)]
mod tests;

pub const SAMBA_AOIDE_COLLECTION_KIND: &str = "samba";
pub const SAMBA_BIND_ADDR_ENV_VAR: &str = "SAMBA_BACKEND_BIND_ADDRESS";
pub const SAMBA_JWT_SECRET_ENV_VAR: &str = "SAMBA_JWT_SECRET";
pub const SAMBA_LOG_FILE_ENV_VAR: &str = "SAMBA_BACKEND_LOG_FILE";
pub const SAMBA_SQLITE_FILE_ENV_VAR: &str = "SAMBA_BACKEND_SQLITE_FILE";
pub const SAMBA_TANTIVY_INDEX_DIR_ENV_VAR: &str = "SAMBA_TANTIVY_INDEX_DIR";
pub const AOIDE_SQLITE_FILE_ENV_VAR: &str = "AOIDE_SQLITE_FILE";

/// Start the samba webserver and initialize all its dependencies (aoide,
/// database, logging).
///
/// This is not intended for public use. Please read the module notes on
/// embedding samba's webserver.
///
/// # Errors
///
/// This function aggregates all errors in an [`color_eyre::Result`] which
/// is intended for further propagation to the user/exiting the program run.
pub async fn start_webserver() -> Result<()> {
    color_eyre::install()?;

    if let Err(e) = dotenvy::dotenv() {
        return Err(eyre!(
            "Could not initialize environment from dotenv. Exiting.\n{}",
            e
        ));
    }

    initialize_logging()?;

    // initialize resources for the server's runtime and start up dependencies
    let bind_addr = if let Ok(addr) = dotenvy::var(SAMBA_BIND_ADDR_ENV_VAR) {
        match addr.parse() {
            Ok(socket_addr) => {
                tracing::info!(
                    "Starting listening on {SAMBA_BIND_ADDR_ENV_VAR}={}",
                    &socket_addr
                );
                socket_addr
            },
            Err(e) => return Err(eyre!("Failed to convert to socket address: {}", e)),
        }
    } else {
        tracing::warn!(
            "Could not retrieve bind address from env ({SAMBA_BIND_ADDR_ENV_VAR}), binding to \
             default 127.0.0.1:8081."
        );
        SocketAddr::from(([127, 0, 0, 1], 8081))
    };

    let sqlite_file = dotenvy::var(SAMBA_SQLITE_FILE_ENV_VAR).unwrap_or_else(|_| {
        tracing::warn!(
            "Could not retrieve sqlite file name ({SAMBA_SQLITE_FILE_ENV_VAR}), defaulting to \
             ./samba.sqlite."
        );
        "./samba.sqlite".to_string()
    });

    let aoide_gatekeeper = Arc::new(provision_aoide().await?);
    let database_gatekeeper = Arc::new(initialize_and_migrate_database(&sqlite_file).await?);

    // Check whether runtime dependencies are present.
    for runtime_dependency in ["weasyprint", "audiowaveform"] {
        if !check_program_in_path(runtime_dependency).await {
            tracing::warn!(
                "Runtime dependency {} missing, some requests may fail.",
                runtime_dependency
            );
        }
    }

    // Get jwt secret which initializes the encoding *and* decoding keys for JWTs.
    let jwt_session_key = dotenvy::var(SAMBA_JWT_SECRET_ENV_VAR).unwrap_or_else(|_| {
        tracing::warn!("Using fallback JWT secret susceptible to timing attacks.");
        format!(
            "{}",
            std::time::SystemTime::now()
                .duration_since(std::time::UNIX_EPOCH)
                .expect("Time does not go backwards.")
                .as_millis()
        )
    });
    let jwt_session_keys = Arc::new(Keys::new(jwt_session_key.as_bytes()));

    // Initialize communication channels; sender can be used to send `()`
    // with the only one intended meaning: shutdown the server gracefully.
    let (shutdown_channel_sender, mut shutdown_channel_receiver) =
        tokio::sync::oneshot::channel::<()>();
    // And initialize a second channel pair to move background tasks into
    // the background thread. Additionally, the background thread gets its
    // own shutdown channel as well (same semantics as the main shutdown
    // channel).
    let (background_channel_sender, background_channel_receiver) =
        tokio::sync::mpsc::unbounded_channel::<BackgroundTask>();
    let (background_channel_shutdown_sender, background_channel_shutdown_receiver) =
        tokio::sync::oneshot::channel::<()>();

    let tantivy_cache = Arc::new(open_tantivy_cache()?);

    // Initialize session data to be shared among all routes.
    let shutdown_channel_sender = Arc::new(shutdown_channel_sender);
    let background_channel = Arc::new(background_channel_sender);
    let background_tasks = Arc::new(RwLock::new(HashMap::new()));
    let shared_state = SharedData {
        aoide_gatekeeper: Arc::clone(&aoide_gatekeeper),
        database_gatekeeper: Arc::clone(&database_gatekeeper),
        tantivy_cache: Arc::clone(&tantivy_cache),
        background_channel: Arc::clone(&background_channel),
        background_tasks: Arc::clone(&background_tasks),
        shutdown_channel: shutdown_channel_sender,
        jwt_session_keys: Arc::clone(&jwt_session_keys),
    };

    // Start a thread for background tasks; basic idea is fifo scheduling
    // which might not be the fastest mode of operation but it avoids running
    // multiple background tasks in parallel, which in turn reduces the risk
    // of locking the database when it should not be locked.
    let background_task_handle = create_background_task_thread(
        background_tasks,
        background_channel_receiver,
        background_channel_shutdown_receiver,
    );

    let aoide_gatekeeper_clone = Arc::clone(&aoide_gatekeeper);
    let database_gatekeeper_clone = Arc::clone(&database_gatekeeper);
    let tantivy_cache_clone = Arc::clone(&tantivy_cache);
    let update_tantivy_task = BackgroundTask::new(tokio::task::spawn(async {
        tracing::info!("Starting updating the tantivy cache in the background.");
        library::track_index::update(
            aoide_gatekeeper_clone,
            database_gatekeeper_clone,
            tantivy_cache_clone,
        )
        .await
        .map(|_| None)
        .map_err(Into::into)
    }));
    background_channel.send(update_tantivy_task)?;

    let app = routes::get_application_router(&jwt_session_keys)
        // TODO: check if we want CORS restrictions
        .layer(CorsLayer::permissive())
        .layer(TraceLayer::new_for_http())
        .layer(CompressionLayer::new().gzip(true).deflate(true).br(true).zstd(true))
        .layer(DecompressionLayer::new().gzip(true).deflate(true).br(true).zstd(true))
        // Graceful shutdown will wait for outstanding requests to complete.
        // Add a timeout so requests don't hang forever.
        .layer(TimeoutLayer::new(Duration::from_secs(10)))
        .with_state(shared_state)
        // TODO: serve OpenAPI config at root
        // .route("/", get(handler))
        ;

    // run the server until receiving a shutdown signal
    let listener = TcpListener::bind(bind_addr)
        .await
        .map_err(|e| eyre!("Could not start listening on {bind_addr}: {e:?}"))?;

    // --- BEGIN graceful shutdown handling (previously provided by hyper's
    //     Server implementing a graceful_shutdown trigger) from the axum
    //     graceful-shutdown example

    // Track tasks that are handling connections and later on wait for them
    // to complete.
    let (close_tx, close_rx) = tokio::sync::watch::channel(());
    // Continuously accept new connections.
    loop {
        let (socket, remote_addr) = tokio::select! {
            // Either accept a new connection...
            result = listener.accept() => {
                result.map_err(|e| eyre!("Failed to accept a new connection: {e:?}"))?
            }
            // ... or wait to receive an OS shutdown signal ...
            _ = shutdown_signal() => {
                tracing::debug!("Shutdown signal received, not accepting new connections.");
                break;
            }
            // ... or wait for an HTTP request from within the application
            // and stop the accept loop in the latter two cases.
            result = &mut shutdown_channel_receiver => {
                match result {
                    Ok(_) => {
                        tracing::debug!("Received HTTP shutdown signal");
                    },
                    Err(e) => {
                        tracing::error!("Error while receiving shutdown signal: {}", e);
                    },
                }
                break;
            },
        };

        tracing::debug!("Connection at {remote_addr} accepted.");

        let tower_service = app.clone();
        let close_rx = close_rx.clone();
        // Spawn a task to handle the connection. That way we can serve
        // multiple connections concurrently.
        tokio::spawn(async move {
            let socket = TokioIo::new(socket);
            let hyper_service = hyper::service::service_fn(move |request: Request<Incoming>| {
                tower_service.clone().call(request)
            });

            // `hyper_util::server::conn::auto::Builder` supports both http1 and http2 but
            // doesn't support graceful so we have to use hyper directly and
            // unfortunately pick between http1 and http2. Requires
            // `with_upgrades()` for websockets.
            let conn =
                hyper::server::conn::http1::Builder::new().serve_connection(socket, hyper_service);
            // Graceful shutdown requires a pinned connection.
            let mut conn = std::pin::pin!(conn);

            loop {
                tokio::select! {
                    // Poll the connection. This completes when the client has
                    // closed the connection, graceful shutdown has completed,
                    // or we encounter a TCP error.
                    result = conn.as_mut() => {
                        if let Err(err) = result {
                            tracing::debug!("Failed to serve connection: {err:#}");
                        }
                        break;
                    }
                    // Start graceful shutdown when we receive a shutdown signal.
                    //
                    // We use a loop to continue polling the connection to
                    // allow requests to finish after starting graceful shutdown.
                    // Our `Router` has `TimeoutLayer` so requests will not hang
                    // indefinitely.
                    _ = shutdown_signal() => {
                        tracing::debug!("Shutdown signal received, starting graceful shutdown.");
                        conn.as_mut().graceful_shutdown();
                    }
                }
            }

            tracing::debug!("Connection at {remote_addr} closed");
            // Drop the watch receiver to signal to `main` that this task is done.
            drop(close_rx);
        });
    }

    // We only care about the watch receivers that were moved into the tasks so
    // close the residual receiver.
    drop(close_rx);
    // Close the listener to stop accepting new connections.
    drop(listener);

    tracing::info!("Server stopped accepting connections, gracefully shutting down.");

    // Wait for all tasks to complete.
    tracing::debug!("Waiting for {} tasks to finish.", close_tx.receiver_count());
    close_tx.closed().await;

    // --- END graceful shutdown handling from axum example, finish custom
    //     graceful shutdown tasks

    tracing::info!("Finishing up background tasks.");
    background_channel_shutdown_sender
        .send(())
        .map_err(|_unit_error| eyre!("Failed to shut down background tasks."))?;
    background_task_handle
        .join()
        .map_err(|_join_error| eyre!("Error while completing background tasks."))?;

    tracing::debug!("Decommissioning databases.");
    aoide_gatekeeper.decommission();
    database_gatekeeper.decommission();

    Ok(())
}
