use std::{
    path::{Path, PathBuf},
    sync::Arc,
};

use aoide::{CollectionUid, EntityRevision as TrackRevision, TrackUid};
use axum::{
    body::Body,
    http::StatusCode,
    response::{IntoResponse, Response},
};
use ring::digest::{Context as HashContext, Digest, SHA256};
use samba_backend_database::{
    Gatekeeper as DbGatekeeper, GatekeeperError,
    models::TrackCacheEntry,
    queries::{
        QueryError,
        track_cache::{create_entry, get_cache_entry, update_entry},
    },
};
use thiserror::Error;
use tokio::{io::AsyncReadExt, process::Command};

use super::audiowaveform::{WaveformError, postprocess_json as postprocess_waveform_data};
use crate::{
    aoide::{AoideError, Gatekeeper as AoideGatekeeper, get_tracks_for_collection},
    server::background_tasks::BackgroundTaskSendError,
};

#[derive(Error, Debug)]
pub enum TrackCacheError {
    #[error("Failed to initiate background process: {0}")]
    BackgroundTaskError(#[from] BackgroundTaskSendError),
    #[error("Interacting with the database failed: {0}")]
    DatabasePoolingFailed(#[from] GatekeeperError),
    #[error("Querying the database failed: {0}")]
    DatabaseQueryError(#[from] QueryError),
    #[error("Getting a digest for a file failed: {0}")]
    HashingError(#[from] std::io::Error),
    #[error("Could not convert process output to UTF-8: {0}")]
    OutputError(#[from] std::string::FromUtf8Error),
    #[error("Listing available tracks for collection failed: {0}")]
    TrackFetchingFromCollectionFailed(#[from] AoideError),
    #[error("Failed to process the waveform JSON: {0}")]
    WaveformProcessingFailed(#[from] WaveformError),
}

impl IntoResponse for TrackCacheError {
    fn into_response(self) -> Response {
        let body = Body::from(format!("{:?}", self));

        Response::builder()
            .status(StatusCode::INTERNAL_SERVER_ERROR)
            .body(body)
            .unwrap()
    }
}

async fn sha256_digest_for(file_path: &Path) -> Result<Digest, TrackCacheError> {
    let mut context = HashContext::new(&SHA256);
    let mut buffer = [0; 1024];

    let mut readable = tokio::fs::File::open(file_path).await?;
    loop {
        let count = readable.read(&mut buffer).await?;
        if count == 0 {
            break;
        }
        context.update(&buffer[..count]);
    }

    Ok(context.finish())
}

async fn get_waveform_json(file_path: &Path) -> Result<Option<String>, TrackCacheError> {
    let std::process::Output { status, stdout, .. } = Command::new("audiowaveform")
        .arg("-i")
        .arg(file_path)
        .arg("--pixels-per-second")
        .arg("20")
        .arg("--bits")
        .arg("8")
        .arg("--output-format")
        .arg("json")
        .stdout(std::process::Stdio::piped())
        .stderr(std::process::Stdio::null())
        .spawn()?
        .wait_with_output()
        .await?;
    let waveform_json = String::from_utf8(stdout)?;

    Ok(status
        .success()
        .then_some(postprocess_waveform_data(&waveform_json)?))
}

async fn update_waveform_from_track(
    gatekeeper: &DbGatekeeper,
    track_id: &TrackUid,
    track_revision: &TrackRevision,
    track_path: &Path,
) -> Result<bool, TrackCacheError> {
    let cache_entry = get_cache_entry(gatekeeper, &track_id.to_string()).await;
    let digest = sha256_digest_for(track_path).await?;

    if let Ok(ref cache_entry) = cache_entry {
        if cache_entry.content_checksum == digest.as_ref()
            && cache_entry.track_rev as u64 == track_revision.value()
        {
            // everything up to date, we can skip this entry
            return Ok(false);
        }
    }

    let peak_data = get_waveform_json(track_path).await?;

    if let Ok(cache_entry) = cache_entry {
        let new_cache_entry = TrackCacheEntry {
            track_uid: cache_entry.track_uid,
            track_rev: cache_entry.track_rev,
            content_checksum: digest.as_ref().to_vec(),
            waveform_data: peak_data,
        };
        update_entry(gatekeeper, new_cache_entry).await?;
    } else {
        let new_cache_entry = TrackCacheEntry {
            track_uid: track_id.to_string().as_bytes().to_vec(),
            // TODO: use 64 bit type here (need to fix diesel's schema for that)
            track_rev: track_revision.value() as i32,
            content_checksum: digest.as_ref().to_vec(),
            waveform_data: peak_data,
        };
        create_entry(gatekeeper, new_cache_entry).await?;
    }

    Ok(true)
}

#[derive(Debug)]
struct WaveformUpdateQueryData {
    track_uid: TrackUid,
    track_rev: TrackRevision,
    track_path: PathBuf,
}

#[tracing::instrument(skip(database_gatekeeper, tracks))]
async fn update_waveforms(
    database_gatekeeper: Arc<DbGatekeeper>,
    tracks: &[WaveformUpdateQueryData],
) -> Result<usize, TrackCacheError> {
    // TODO: do not query database on each track

    let mut number_of_updates = 0;
    for track in tracks {
        if update_waveform_from_track(
            &database_gatekeeper,
            &track.track_uid,
            &track.track_rev,
            &track.track_path,
        )
        .await?
        {
            number_of_updates += 1;
        }
    }

    Ok(number_of_updates)
}

#[tracing::instrument(skip(aoide_gatekeeper, database_gatekeeper))]
pub async fn update_collection_track_cache(
    aoide_gatekeeper: Arc<AoideGatekeeper>,
    database_gatekeeper: Arc<DbGatekeeper>,
    collection_uid: CollectionUid,
) -> Result<usize, TrackCacheError> {
    let tracks = get_tracks_for_collection(&aoide_gatekeeper, collection_uid).await?;
    let track_waveform_query = tracks
        .iter()
        .map(|entity| WaveformUpdateQueryData {
            track_uid: entity.hdr.uid.clone(),
            track_rev: entity.hdr.rev,
            track_path: entity
                .body
                .content_url
                .as_ref()
                .and_then(|url| url.to_file_path().ok())
                .unwrap_or_else(|| {
                    entity
                        .body
                        .track
                        .media_source
                        .content
                        .link
                        .path
                        .to_string()
                        .into()
                }),
        })
        .collect::<Vec<_>>();

    update_waveforms(database_gatekeeper, &track_waveform_query).await
}
