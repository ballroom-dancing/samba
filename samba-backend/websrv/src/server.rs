pub mod background_tasks;
pub mod data;
pub mod initialization;

/// A shutdown handler that reacts to CTRL + C, a unix terminate signal, or
/// a signal through the oneshot channel.
pub async fn shutdown_signal() {
    let ctrl_c = async {
        tokio::signal::ctrl_c()
            .await
            .expect("failed to install Ctrl+C handler");
    };

    #[cfg(unix)]
    let terminate = async {
        tokio::signal::unix::signal(tokio::signal::unix::SignalKind::terminate())
            .expect("failed to install signal handler")
            .recv()
            .await;
    };
    #[cfg(not(unix))]
    let terminate = std::future::pending::<()>();

    tokio::select! {
        // terminate based on user request
        _ = ctrl_c => {},
        // terminate based on polite termination request
        // from OS
        _ = terminate => {},
    }
}
