use diesel::{prelude::*, result::Error as DieselError};

use super::QueryError;
use crate::{Gatekeeper, models::TrackCacheEntry};

// TODO: use actual wavesurfer type instead of returning stringified JSON?
#[tracing::instrument(skip(db_gatekeeper))]
pub async fn get_waveform_for_track(
    db_gatekeeper: &Gatekeeper,
    track_id: &str,
) -> Result<Option<String>, QueryError> {
    let query_track_id = track_id.to_string();

    db_gatekeeper
        .spawn_blocking_read_task(move |mut pooled_connection| {
            let connection = &mut *pooled_connection;
            connection.transaction::<_, DieselError, _>(|connection| {
                use crate::schema::track_cache::dsl::*;

                track_cache
                    .select(waveform_data)
                    .filter(track_uid.eq(&query_track_id.as_bytes()))
                    .first::<Option<String>>(connection)
            })
        })
        .await?
        .map_err(|_err| QueryError::RowIdentification(track_id.to_string()))
}

#[tracing::instrument(skip(db_gatekeeper))]
pub async fn get_cache_entry(
    db_gatekeeper: &Gatekeeper,
    track_id: &str,
) -> Result<TrackCacheEntry, QueryError> {
    let query_track_id = track_id.to_string();

    db_gatekeeper
        .spawn_blocking_read_task(move |mut pooled_connection| {
            let connection = &mut *pooled_connection;
            connection.transaction::<_, DieselError, _>(|connection| {
                use crate::{models::TrackCacheEntry, schema::track_cache::dsl::*};

                track_cache
                    .filter(track_uid.eq(&query_track_id.as_bytes()))
                    .first::<TrackCacheEntry>(connection)
            })
        })
        .await?
        .map_err(|_err| QueryError::RowIdentification(track_id.to_string()))
}

#[tracing::instrument(skip(db_gatekeeper))]
pub async fn create_entry(
    db_gatekeeper: &Gatekeeper,
    entry: TrackCacheEntry,
) -> Result<(), QueryError> {
    db_gatekeeper
        .spawn_blocking_write_task(move |mut pooled_connection| {
            let connection = &mut *pooled_connection;
            connection.transaction::<_, DieselError, _>(|connection| {
                use crate::schema::track_cache::dsl::*;

                diesel::insert_into(track_cache)
                    .values(&entry)
                    .execute(connection)
                    .map(|_| ())
            })
        })
        .await?
        .map_err(QueryError::DatabaseInsertion)
}

#[tracing::instrument(skip(db_gatekeeper))]
pub async fn update_entry(
    db_gatekeeper: &Gatekeeper,
    entry: TrackCacheEntry,
) -> Result<(), QueryError> {
    db_gatekeeper
        .spawn_blocking_write_task(move |mut pooled_connection| {
            let connection = &mut *pooled_connection;
            connection.transaction::<_, DieselError, _>(|connection| {
                use crate::schema::track_cache::dsl::*;

                diesel::update(track_cache.filter(track_uid.eq(&entry.track_uid)))
                    .set(&entry)
                    .execute(connection)
                    .map(|_ok| ())
            })
        })
        .await?
        .map_err(QueryError::DatabaseUpdate)
}

#[tracing::instrument(skip(db_gatekeeper))]
pub async fn delete_entry(db_gatekeeper: &Gatekeeper, track_id: &str) -> Result<(), QueryError> {
    let query_track_id = track_id.to_string();

    db_gatekeeper
        .spawn_blocking_write_task(move |mut pooled_connection| {
            let connection = &mut *pooled_connection;
            connection.transaction::<_, DieselError, _>(|connection| {
                use crate::schema::track_cache::dsl::*;

                diesel::delete(track_cache.filter(track_uid.eq(query_track_id.as_bytes())))
                    .execute(connection)
                    .map(|_ok| ())
            })
        })
        .await?
        .map_err(QueryError::DatabaseDeletion)
}
