use diesel::{prelude::*, result::Error as DieselError};
use samba_shared::events::{Event, StoredEvent};

use super::QueryError;
use crate::{Gatekeeper, models::Event as DbEvent};

#[tracing::instrument(skip(db_gatekeeper))]
pub async fn get_by_id(
    db_gatekeeper: &Gatekeeper,
    event_id: i32,
) -> Result<StoredEvent, QueryError> {
    db_gatekeeper
        .spawn_blocking_read_task(move |mut pooled_connection| {
            let connection = &mut *pooled_connection;
            connection.transaction::<_, DieselError, _>(|connection| {
                use crate::schema::events::dsl::*;

                events
                    .filter(id.eq(event_id))
                    .first::<DbEvent>(connection)
                    .map(Into::into)
            })
        })
        .await?
        .map_err(|_diesel_error| QueryError::RowIdentification(format!("{event_id}")))
}

#[tracing::instrument(skip(db_gatekeeper))]
pub async fn get_all(db_gatekeeper: &Gatekeeper) -> Result<Vec<StoredEvent>, QueryError> {
    db_gatekeeper
        .spawn_blocking_read_task(move |mut pooled_connection| {
            let connection = &mut *pooled_connection;
            connection.transaction::<_, DieselError, _>(|connection| {
                use crate::schema::events::dsl::*;

                events
                    .load::<DbEvent>(connection)
                    .map(|vec| vec.into_iter().map(Into::into).collect::<Vec<_>>())
            })
        })
        .await?
        .map_err(QueryError::ResultListing)
}

#[tracing::instrument(skip(db_gatekeeper))]
pub async fn create(db_gatekeeper: &Gatekeeper, event: Event) -> Result<i32, QueryError> {
    let insertable = DbEvent::to_be_created(event);

    db_gatekeeper
        .spawn_blocking_write_task(move |mut pooled_connection| {
            let connection = &mut *pooled_connection;
            connection.transaction::<_, DieselError, _>(|connection| {
                use crate::schema::events::dsl::*;

                diesel::insert_into(events)
                    .values(&insertable)
                    .get_result::<DbEvent>(connection)
                    .map(|event| event.id)
            })
        })
        .await?
        .map_err(QueryError::DatabaseInsertion)
}

#[tracing::instrument(skip(db_gatekeeper))]
pub async fn update(
    db_gatekeeper: &Gatekeeper,
    event_id: i32,
    updated_value: Event,
) -> Result<(), QueryError> {
    let updateable = DbEvent::with_id(event_id, updated_value);

    db_gatekeeper
        .spawn_blocking_write_task(move |mut pooled_connection| {
            let connection = &mut *pooled_connection;
            connection.transaction::<_, DieselError, _>(|connection| {
                use crate::schema::events::dsl::*;

                diesel::update(events.filter(id.eq(event_id)))
                    .set(&updateable)
                    .execute(connection)
                    .map(|_ok| ())
            })
        })
        .await?
        .map_err(QueryError::DatabaseUpdate)
}

#[tracing::instrument(skip(db_gatekeeper))]
pub async fn delete(db_gatekeeper: &Gatekeeper, event_id: i32) -> Result<(), QueryError> {
    db_gatekeeper
        .spawn_blocking_write_task(move |mut pooled_connection| {
            let connection = &mut *pooled_connection;
            connection.transaction::<_, DieselError, _>(|connection| {
                use crate::schema::events::dsl::*;

                diesel::delete(events.filter(id.eq(event_id)))
                    .execute(connection)
                    .map(|_ok| ())
            })
        })
        .await?
        .map_err(QueryError::DatabaseDeletion)
}
