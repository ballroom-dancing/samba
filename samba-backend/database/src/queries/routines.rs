use diesel::{prelude::*, result::Error as DieselError};
use samba_shared::routines::{Routine, StoredRoutine};

use super::QueryError;
use crate::{Gatekeeper, models::Routine as DbRoutine};

#[tracing::instrument(skip(db_gatekeeper))]
pub async fn get_by_id(
    db_gatekeeper: &Gatekeeper,
    routine_id: i32,
) -> Result<StoredRoutine, QueryError> {
    db_gatekeeper
        .spawn_blocking_read_task(move |mut pooled_connection| {
            let connection = &mut *pooled_connection;
            connection.transaction::<_, DieselError, _>(|connection| {
                use crate::schema::routines::dsl::*;

                routines
                    .filter(id.eq(routine_id))
                    .first::<DbRoutine>(connection)
                    .map(Into::into)
            })
        })
        .await?
        .map_err(|_diesel_error| QueryError::RowIdentification(format!("{routine_id}")))
}

#[tracing::instrument(skip(db_gatekeeper))]
pub async fn get_all(db_gatekeeper: &Gatekeeper) -> Result<Vec<StoredRoutine>, QueryError> {
    db_gatekeeper
        .spawn_blocking_read_task(move |mut pooled_connection| {
            let connection = &mut *pooled_connection;
            connection.transaction::<_, DieselError, _>(|connection| {
                use crate::schema::routines::dsl::*;

                routines
                    .load::<DbRoutine>(connection)
                    .map(|vec| vec.into_iter().map(Into::into).collect::<Vec<_>>())
            })
        })
        .await?
        .map_err(QueryError::ResultListing)
}

#[tracing::instrument(skip(db_gatekeeper))]
pub async fn create(db_gatekeeper: &Gatekeeper, routine: Routine) -> Result<i32, QueryError> {
    let insertable = DbRoutine::to_be_created(routine);

    db_gatekeeper
        .spawn_blocking_write_task(move |mut pooled_connection| {
            let connection = &mut *pooled_connection;
            connection.transaction::<_, DieselError, _>(|connection| {
                use crate::schema::routines::dsl::*;

                diesel::insert_into(routines)
                    .values(&insertable)
                    .get_result::<DbRoutine>(connection)
                    .map(|routine| routine.id)
            })
        })
        .await?
        .map_err(QueryError::DatabaseInsertion)
}

#[tracing::instrument(skip(db_gatekeeper))]
pub async fn update(
    db_gatekeeper: &Gatekeeper,
    routine_id: i32,
    routine: Routine,
) -> Result<(), QueryError> {
    let updateable = DbRoutine::with_id(routine_id, routine);

    db_gatekeeper
        .spawn_blocking_write_task(move |mut pooled_connection| {
            let connection = &mut *pooled_connection;
            connection.transaction::<_, DieselError, _>(|connection| {
                use crate::schema::routines::dsl::*;

                diesel::update(routines.filter(id.eq(routine_id)))
                    .set(&updateable)
                    .execute(connection)
                    .map(|_ok| ())
            })
        })
        .await?
        .map_err(QueryError::DatabaseUpdate)
}

#[tracing::instrument(skip(db_gatekeeper))]
pub async fn delete(db_gatekeeper: &Gatekeeper, routine_id: i32) -> Result<(), QueryError> {
    db_gatekeeper
        .spawn_blocking_write_task(move |mut pooled_connection| {
            let connection = &mut *pooled_connection;
            connection.transaction::<_, DieselError, _>(|connection| {
                use crate::schema::routines::dsl::*;

                diesel::delete(routines.filter(id.eq(routine_id)))
                    .execute(connection)
                    .map(|_ok| ())
            })
        })
        .await?
        .map_err(QueryError::DatabaseDeletion)
}
