#[macro_use]
extern crate diesel;
#[macro_use]
extern crate diesel_migrations;

use std::io::BufWriter;

use aoide::storage_sqlite::connection::pool::gatekeeper::Config as DatabaseConnectionGatekeeperConfig;
use diesel::{
    backend::Backend,
    prelude::*,
    r2d2::{ConnectionManager as DieselConnectionManager, Pool},
    result::Error as DieselError,
};
use diesel_migrations::{EmbeddedMigrations, HarnessWithOutput, MigrationHarness};
use thiserror::Error;

const MIGRATIONS: EmbeddedMigrations = embed_migrations!("./migrations/");

mod schema;

pub mod models;
pub mod queries;

pub use aoide::storage_sqlite::{
    Error as GatekeeperError, connection::pool::gatekeeper::Gatekeeper,
};

#[derive(Error, Debug)]
pub enum DatabaseError {
    #[error("Failed to commit migrations because of {0:?}.")]
    RunMigrationsError(Box<dyn std::error::Error + Send + Sync>),
    #[error("Failed to collect migration output to string.")]
    OutputCollectionFailed,
    #[error("r2d2 failed to pool the database collection: {0}.")]
    PoolingFailed(#[from] diesel::r2d2::PoolError),
    #[error("Diesel failed to query the database: {0}.")]
    SqlQueryError(#[from] DieselError),
    #[error("Type conversion between aoide types failed.")]
    TypeConversionFailed,
}

#[allow(clippy::panic_in_result_fn)] // TODO: remove when not needed for tracing::instrument
#[tracing::instrument(skip(conn))]
pub fn migrate_database<B>(conn: &mut impl MigrationHarness<B>) -> Result<String, DatabaseError>
where
    B: Backend,
{
    let mut output_writer = BufWriter::new(Vec::new());

    HarnessWithOutput::new(conn, &mut output_writer)
        .run_pending_migrations(MIGRATIONS)
        .map_err(DatabaseError::RunMigrationsError)?;

    let bytes = output_writer
        .into_inner()
        .map_err(|_unwrap_failed| DatabaseError::OutputCollectionFailed)?;
    let string = String::from_utf8(bytes).map_err(|e| {
        tracing::debug!("Database migration yielded invalid UTF-8: {}", e);
        DatabaseError::OutputCollectionFailed
    })?;
    Ok(string)
}

#[tracing::instrument]
pub async fn initialize_and_migrate_database(
    sqlite_file: &str,
) -> Result<Gatekeeper, DatabaseError> {
    let manager = DieselConnectionManager::<SqliteConnection>::new(sqlite_file);
    let pool = match Pool::builder().max_size(8).build(manager) {
        Ok(pool) => {
            tracing::info!("Successfully established database connection to {sqlite_file}");
            diesel::sql_query(r#"
              -- enforce same pragmas as aoide
              PRAGMA journal_mode = WAL;        -- better write-concurrency
              PRAGMA synchronous = NORMAL;      -- fsync only in critical moments, safe for journal_mode = WAL
              PRAGMA wal_autocheckpoint = 1000; -- write WAL changes back every 1000 pages (default), for an in average 1MB WAL file
              PRAGMA wal_checkpoint(TRUNCATE);  -- free some space by truncating possibly massive WAL files from the last run
              PRAGMA secure_delete = 0;         -- avoid some disk I/O
              PRAGMA automatic_index = 1;       -- detect and log missing indexes
              PRAGMA foreign_keys = 1;          -- check foreign key constraints
              PRAGMA defer_foreign_keys = 1;    -- delay enforcement of foreign key constraints until commit
              PRAGMA recursive_triggers = 1;    -- for recursive ON CASCADE DELETE actions
              PRAGMA encoding = 'UTF-8';
            "#).execute(&mut *pool.get()?)?;
            migrate_database(&mut *pool.get()?)?;
            pool
        },
        Err(e) => {
            tracing::error!("Failed to create data base pool. Error: {}", e);
            return Err(e.into());
        },
    };

    Ok(Gatekeeper::new(pool, DatabaseConnectionGatekeeperConfig {
        acquire_read_timeout_millis: 10_000
            .try_into()
            .map_err(|_conversion_failure| DatabaseError::TypeConversionFailed)?,
        acquire_write_timeout_millis: 30_000
            .try_into()
            .map_err(|_conversion_failure| DatabaseError::TypeConversionFailed)?,
    }))
}
