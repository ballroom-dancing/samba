use argon2::{
    Argon2,
    password_hash::{PasswordHash, PasswordVerifier},
};
use secrecy::{ExposeSecret, SecretString};

use crate::schema::{members, track_cache};

mod dances;
mod events;
mod libraries;
mod routines;
pub use dances::*;
pub use events::*;
pub use libraries::*;
pub use routines::*;

#[derive(Debug, Clone, Identifiable, Insertable, Queryable, AsChangeset)]
#[diesel(primary_key(id))]
pub struct Member {
    pub id: i32,
    pub display_name: String,
    pub username: String,
    pub salted_pass: String,
    pub preferences: Option<String>,
}

impl Member {
    #[must_use]
    pub fn password_matches(&self, pass: &SecretString) -> bool {
        let Ok(parsed_hash) = PasswordHash::new(&self.salted_pass) else {
            return false;
        };
        Argon2::default()
            .verify_password(pass.expose_secret().as_bytes(), &parsed_hash)
            .is_ok()
    }
}

#[derive(Debug, Clone, Insertable, Queryable, AsChangeset)]
#[diesel(primary_key(track_uid), table_name = track_cache)]
pub struct TrackCacheEntry {
    pub track_uid: Vec<u8>,
    pub track_rev: i32,
    pub content_checksum: Vec<u8>,
    pub waveform_data: Option<String>,
}
