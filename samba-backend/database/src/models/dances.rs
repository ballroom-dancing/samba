use samba_shared::dancing::DanceCategory as SharedDanceCategory;

use crate::schema::{dance_categories, dances};

#[derive(Debug, Clone, Identifiable, Insertable, Queryable, AsChangeset)]
#[diesel(primary_key(id))]
pub struct Dance {
    pub id: i32,
    pub abbreviation: Option<String>,
    pub display_name: String,
    pub category: String,
    pub crotchets_per_measure: i32,
    pub measures_per_minute_min: i32,
    pub measures_per_minute_max: i32,
}

impl From<Dance> for samba_shared::dancing::Dance {
    fn from(db_dance: Dance) -> Self {
        Self {
            name: db_dance.display_name.clone(),
            abbreviation: db_dance.abbreviation.clone().unwrap_or_default(),
            category: match db_dance.category.as_str() {
                "ballroom" => SharedDanceCategory::Ballroom,
                "latin" => SharedDanceCategory::Latin,
                "social" => SharedDanceCategory::Social,
                _ => unreachable!("Database contained corrupt dance category."),
            },
            mpm_range: db_dance.measures_per_minute_min as u8
                ..db_dance.measures_per_minute_max as u8,
            number_of_crotchets: db_dance.crotchets_per_measure as u8,
        }
    }
}

#[derive(Debug, Clone, Associations, Identifiable, Insertable, Queryable)]
#[diesel(
  primary_key(id),
  belongs_to(Dance, foreign_key = id),
  table_name = dance_categories
)]
pub struct DanceCategory {
    pub id: String,
    pub display_name: Option<String>,
}
