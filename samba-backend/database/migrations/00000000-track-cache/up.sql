CREATE TABLE track_cache (
  -- aoide track identification
  track_uid        BINARY(24) NOT NULL PRIMARY KEY,
  track_rev        INTEGER    NOT NULL,
  -- track on file system, sha256 hash
  content_checksum BINARY(32) NOT NULL,
  -- waveforms may be missing because the backend
  -- system may not have audiowaveform installed
  waveform_data    TEXT
);
