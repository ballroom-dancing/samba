CREATE TABLE dances (
  id                      INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  abbreviation            TEXT,
  display_name            TEXT    NOT NULL,
  category                TEXT    NOT NULL,
  crotchets_per_measure   INTEGER NOT NULL,
  measures_per_minute_min INTEGER NOT NULL,
  measures_per_minute_max INTEGER NOT NULL,
  FOREIGN KEY(category) REFERENCES dance_categories(id)
  -- there can only between 1 and 4 (inclusive) crotchets per measure
  CONSTRAINT check_cpm CHECK (crotchets_per_measure > 0 AND crotchets_per_measure <= 4)
  CONSTRAINT check_mpm_positive CHECK (measures_per_minute_min > 0 AND measures_per_minute_max > 0)
  CONSTRAINT check_mpm_relation CHECK (measures_per_minute_max >= measures_per_minute_min)
  -- check that we are in range of a Rust u8, noone will have
  -- competition dances with more than 256 measures per minute
  CONSTRAINT check_mpm_max CHECK (measures_per_minute_max < 256)
);

CREATE TABLE dance_categories (
  id           TEXT NOT NULL PRIMARY KEY,
  display_name TEXT
);
