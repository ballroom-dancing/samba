-- useful for resets when testing:
-- DELETE FROM members;
-- DELETE FROM dances;
-- DELETE FROM dance_categories;
-- DELETE FROM event_competition_timetables;
-- DELETE FROM events;
-- DELETE FROM routines;

INSERT INTO members
  (display_name, username, salted_pass, preferences)
VALUES
  ('Demo user', 'demo', '$argon2id$v=19$m=19456,t=2,p=1$37FLWZtlHJ1/tB3Sw8TZag$G0qKGxRW5wG8gfX6IOAo6tpkGkNV+O1URvxdUDMeoYQ', NULL);

INSERT INTO dance_categories (id, display_name)
VALUES ('ballroom', 'Ballroom dances'), ('latin', 'Latin dances'), ('social', 'Social dances');

INSERT INTO dances
  (abbreviation, display_name, category, crotchets_per_measure, measures_per_minute_min, measures_per_minute_max)
VALUES
  ('SW', '(Slow) Waltz', 'ballroom', 3, 28, 30),
  ('TG', 'Tango', 'ballroom', 4, 31, 33),
  ('VW', 'Viennese Waltz', 'ballroom', 3, 58, 60),
  ('SF', 'Slow Foxtrot', 'ballroom', 4, 28, 30),
  ('QS', 'Quickstep', 'ballroom', 4, 50, 52),
  ('SA', 'Samba', 'latin', 2, 50, 52),
  ('CC', 'Cha Cha Cha', 'latin', 4, 30, 32),
  ('RB', 'Rumba', 'latin', 4, 25, 27),
  ('PD', 'Paso Doble', 'latin', 2, 60, 62),
  ('JI', 'Jive', 'latin', 4, 42, 44),
  ('DF', 'Disco Fox', 'social', 4, 28, 32),
  ('SALSA', 'Salsa', 'social', 4, 28, 38),
  ('RR', 'Rock & Roll', 'social', 4, 42, 52),
  ('FT', '(Social) Foxtrot', 'social', 4, 28, 32);

INSERT INTO routines
  (title, description, notes, choreographer, performers, demonstrations, steps)
VALUES
  ('Standard Routine', 'A simple standard routine that does not need to make sense', NULL, 'My trainer', '', '', '[{"Pause":"Slow"},{"Step":{"foot":"Left","timing":"Quick","weight_transfer":"Full","comment":"start diagonally facing wall on long side"}},{"Figure":{"title":"Natural Turn Steps 1–3","start":null,"end":null,"notes":[],"level_requirement":"WdsfDCSyllabus","leaders_steps":[{"foot":"Right","timing":"Quick","weight_transfer":"Full","comment":null},{"foot":"Left","timing":"Quick","weight_transfer":"Full","comment":null},{"foot":"Right","timing":"Quick","weight_transfer":"Full","comment":null}],"followers_steps":[{"foot":"Left","timing":"Quick","weight_transfer":"Full","comment":null},{"foot":"Right","timing":"Quick","weight_transfer":"Full","comment":null},{"foot":"Left","timing":"Quick","weight_transfer":"Full","comment":null}]}},{"Figure":{"title":"Overturned Natural Spin Turn","start":null,"end":null,"notes":[],"level_requirement":"WdsfDCSyllabus","leaders_steps":[],"followers_steps":[]}},{"Figure":{"title":"Reverse Turn Steps 4–6","start":null,"end":null,"notes":[],"level_requirement":"WdsfDCSyllabus","leaders_steps":[],"followers_steps":[]}},{"Figure":{"title":"Whisk","start":null,"end":null,"notes":[],"level_requirement":"WdsfDCSyllabus","leaders_steps":[],"followers_steps":[]}},{"Figure":{"title":"Progressive Chasse to Left","start":null,"end":null,"notes":[],"level_requirement":"WdsfDCSyllabus","leaders_steps":[],"followers_steps":[]}},{"Figure":{"title":"Natural Turn Steps 1–3","start":null,"end":null,"notes":[],"level_requirement":"WdsfDCSyllabus","leaders_steps":[],"followers_steps":[]}},{"Separator":"start of the short side"},{"Figure":{"title":"Impetus","start":null,"end":null,"notes":[],"level_requirement":"WdsfDCSyllabus","leaders_steps":[],"followers_steps":[]}},{"Figure":{"title":"Weave","start":null,"end":null,"notes":[],"level_requirement":"WdsfDCSyllabus","leaders_steps":[],"followers_steps":[]}},{"Figure":{"title":"Curved Feather","start":null,"end":null,"notes":[],"level_requirement":"WdsfDCSyllabus","leaders_steps":[],"followers_steps":[]}},{"Figure":{"title":"Outside Spin","start":null,"end":null,"notes":[],"level_requirement":"WdsfDCSyllabus","leaders_steps":[],"followers_steps":[]}},{"Figure":{"title":"Progressive Chasse to Left","start":null,"end":null,"notes":[],"level_requirement":"WdsfDCSyllabus","leaders_steps":[],"followers_steps":[]}},{"Repeat":{"Custom":12}}]');

INSERT INTO events
  (date, title, description, location)
VALUES
  ('2022-03-12T00:00:00.000', 'Comp (day 1)', 'Dancing competition', 'Near home'),
  ('2022-03-13T00:00:00.000', 'Comp (day 2)', 'Dancing competition', 'Near home');
INSERT INTO event_competition_timetables
  (event_id, start_time, end_time, comment)
VALUES
  ((SELECT id FROM events WHERE title = "Comp (day 1)"), '2022-03-26T10:00:00.000', '2022-03-26T10:30:00.000', 'HGR II D LAT'),
  ((SELECT id FROM events WHERE title = "Comp (day 1)"), '2022-03-26T10:00:00.000', '2022-03-26T10:30:00.000', 'HGR II C LAT'),
  ((SELECT id FROM events WHERE title = "Comp (day 1)"), '2022-03-26T11:00:00.000', '2022-03-26T11:30:00.000', 'HGR II D STD'),
  ((SELECT id FROM events WHERE title = "Comp (day 1)"), '2022-03-26T11:00:00.000', '2022-03-26T11:30:00.000', 'HGR II C STD'),
  ((SELECT id FROM events WHERE title = "Comp (day 1)"), '2022-03-26T12:30:00.000', '2022-03-26T13:00:00.000', 'HGR D LAT'),
  ((SELECT id FROM events WHERE title = "Comp (day 1)"), '2022-03-26T12:30:00.000', '2022-03-26T13:00:00.000', 'HGR C LAT'),
  ((SELECT id FROM events WHERE title = "Comp (day 1)"), '2022-03-26T13:30:00.000', '2022-03-26T14:00:00.000', 'HGR D STD'),
  ((SELECT id FROM events WHERE title = "Comp (day 1)"), '2022-03-26T13:30:00.000', '2022-03-26T14:00:00.000', 'HGR C STD'),
  ((SELECT id FROM events WHERE title = "Comp (day 1)"), '2022-03-26T14:45:00.000', '2022-03-26T15:15:00.000', 'HGR II A LAT'),
  ((SELECT id FROM events WHERE title = "Comp (day 1)"), '2022-03-26T14:45:00.000', '2022-03-26T15:15:00.000', 'HGR II B LAT'),
  ((SELECT id FROM events WHERE title = "Comp (day 1)"), '2022-03-26T16:00:00.000', '2022-03-26T16:30:00.000', 'HGR B STD'),
  ((SELECT id FROM events WHERE title = "Comp (day 1)"), '2022-03-26T16:00:00.000', '2022-03-26T16:30:00.000', 'HGR A STD'),
  ((SELECT id FROM events WHERE title = "Comp (day 1)"), '2022-03-26T17:00:00.000', '2022-03-26T17:30:00.000', 'HGR B LAT'),
  ((SELECT id FROM events WHERE title = "Comp (day 1)"), '2022-03-26T17:00:00.000', '2022-03-26T17:30:00.000', 'HGR A LAT'),
  ((SELECT id FROM events WHERE title = "Comp (day 1)"), '2022-03-26T18:00:00.000', '2022-03-26T18:30:00.000', 'HGR II B STD'),
  ((SELECT id FROM events WHERE title = "Comp (day 1)"), '2022-03-26T18:00:00.000', '2022-03-26T18:30:00.000', 'HGR II A STD'),
  ((SELECT id FROM events WHERE title = "Comp (day 2)"), '2022-03-27T10:00:00.000', '2022-03-27T10:30:00.000', 'HGR II D LAT'),
  ((SELECT id FROM events WHERE title = "Comp (day 2)"), '2022-03-27T10:00:00.000', '2022-03-27T10:30:00.000', 'HGR II C LAT'),
  ((SELECT id FROM events WHERE title = "Comp (day 2)"), '2022-03-27T11:00:00.000', '2022-03-27T11:30:00.000', 'HGR II D STD'),
  ((SELECT id FROM events WHERE title = "Comp (day 2)"), '2022-03-27T11:00:00.000', '2022-03-27T11:30:00.000', 'HGR II C STD'),
  ((SELECT id FROM events WHERE title = "Comp (day 2)"), '2022-03-27T12:30:00.000', '2022-03-27T13:00:00.000', 'HGR D LAT'),
  ((SELECT id FROM events WHERE title = "Comp (day 2)"), '2022-03-27T12:30:00.000', '2022-03-27T13:00:00.000', 'HGR C LAT'),
  ((SELECT id FROM events WHERE title = "Comp (day 2)"), '2022-03-27T13:30:00.000', '2022-03-27T14:00:00.000', 'HGR D STD'),
  ((SELECT id FROM events WHERE title = "Comp (day 2)"), '2022-03-27T13:30:00.000', '2022-03-27T14:00:00.000', 'HGR C STD'),
  ((SELECT id FROM events WHERE title = "Comp (day 2)"), '2022-03-27T14:45:00.000', '2022-03-27T15:15:00.000', 'HGR II A LAT'),
  ((SELECT id FROM events WHERE title = "Comp (day 2)"), '2022-03-27T14:45:00.000', '2022-03-27T15:15:00.000', 'HGR II B LAT'),
  ((SELECT id FROM events WHERE title = "Comp (day 2)"), '2022-03-27T16:00:00.000', '2022-03-27T16:30:00.000', 'HGR B STD'),
  ((SELECT id FROM events WHERE title = "Comp (day 2)"), '2022-03-27T16:00:00.000', '2022-03-27T16:30:00.000', 'HGR A STD'),
  ((SELECT id FROM events WHERE title = "Comp (day 2)"), '2022-03-27T17:00:00.000', '2022-03-27T17:30:00.000', 'HGR B LAT'),
  ((SELECT id FROM events WHERE title = "Comp (day 2)"), '2022-03-27T17:00:00.000', '2022-03-27T17:30:00.000', 'HGR A LAT'),
  ((SELECT id FROM events WHERE title = "Comp (day 2)"), '2022-03-27T18:00:00.000', '2022-03-27T18:30:00.000', 'HGR II B STD'),
  ((SELECT id FROM events WHERE title = "Comp (day 2)"), '2022-03-27T18:00:00.000', '2022-03-27T18:30:00.000', 'HGR II A STD');
