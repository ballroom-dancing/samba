CREATE TABLE libraries (
  id                  TEXT    NOT NULL PRIMARY KEY,
  name                TEXT    NOT NULL,
  path                TEXT    NOT NULL,
  aoide_collection    TEXT    NOT NULL,
  scan_subdirectories BOOLEAN NOT NULL DEFAULT 0,
  shared              BOOLEAN NOT NULL DEFAULT 0
);
