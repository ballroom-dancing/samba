use std::path::Path;

use rust_embed::RustEmbed;
use thiserror::Error;
use tokio::process::Command;

mod events;
mod playlists;
mod routines;

pub use self::{
    events::generate_event_report_at, playlists::generate_playlist_export_at,
    routines::generate_routine_export_at,
};

#[derive(RustEmbed)]
#[folder = "template-data/"]
struct TemplateAssets;

#[derive(Debug, Error)]
pub enum ReportError {
    #[error("The call execution failed.")]
    ExecutionFailed,
    #[error("File operations failed.")]
    IOError,
    #[error("Generating the report failed.")]
    ReportGenerationFailed,
}

async fn call_weasyprint(
    document: &str,
    working_dir: &Path,
    target: &Path,
) -> Result<(), ReportError> {
    let out_dir = tokio::fs::canonicalize(working_dir).await.map_err(|e| {
        tracing::debug!(
            "Failed to canonicalize path {}: {}",
            working_dir.to_string_lossy(),
            e
        );
        ReportError::IOError
    })?;

    let in_file_path = working_dir.join("in.html");
    tokio::fs::write(&in_file_path, document.as_bytes())
        .await
        .map_err(|e| {
            tracing::debug!(
                "Failed to write report input to file {}: {}",
                in_file_path.to_string_lossy(),
                e
            );
            ReportError::IOError
        })?;

    let status = Command::new("weasyprint")
        .current_dir(out_dir)
        .arg(&in_file_path)
        .arg(target)
        .spawn()
        .map_err(|e| {
            tracing::debug!("Spawning weasyprint failed: {}", e);
            ReportError::ExecutionFailed
        })?
        .wait()
        .await
        .map_err(|e| {
            tracing::debug!("Waiting for weasyprint failed: {}", e);
            ReportError::ExecutionFailed
        })?;

    let output_file_exists = tokio::fs::metadata(&target).await.is_ok();
    if status.success() && output_file_exists {
        Ok(())
    } else {
        Err(ReportError::ReportGenerationFailed)
    }
}

async fn prepare_report_directory(location: &Path) -> Result<(), ReportError> {
    debug_assert!(location.is_dir());

    for file_name in TemplateAssets::iter() {
        tokio::fs::write(
            location.join(&*file_name),
            TemplateAssets::get(&file_name)
                .expect("File name from statically inserted asset must be available.")
                .data,
        )
        .await
        .map_err(|e| {
            tracing::debug!("Could not write weasyprint asset {}: {}", file_name, e);
            ReportError::IOError
        })?;
    }

    Ok(())
}

#[tracing::instrument]
pub async fn render_plain_document(document: &str, target: &Path) -> Result<(), ReportError> {
    let working_dir = tempfile::tempdir().map_err(|e| {
        tracing::debug!("Failed to create temp dir: {}", e);
        ReportError::IOError
    })?;

    prepare_report_directory(working_dir.path()).await?;
    call_weasyprint(document, working_dir.path(), target).await
}
