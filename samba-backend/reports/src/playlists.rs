use std::path::Path;

use askama::Template;
use samba_shared::{
    playlists::{AudioPlayback, AudioSource, Playlist, PlaylistItem},
    tracks::{
        metadata::{get_mpm, get_primary_dance},
        query::{extract_dances, extract_mpm_range},
    },
    utils::format_duration_millis,
};

use super::{ReportError, call_weasyprint, prepare_report_directory};

#[derive(Template)]
#[template(path = "playlist.html")]
struct PlaylistTemplate<'a> {
    title: &'a str,
    formatted_length: &'a str,
    notes: &'a str,
    tags: Vec<&'a str>,
    playlist: Vec<(&'a str, &'a str, Option<&'a str>)>,
}

#[derive(Template)]
#[template(path = "playlist-landscape.html")]
struct LandscapePlaylistTemplate<'a> {
    title: &'a str,
    tags: Vec<&'a str>,
    playlist: Vec<(&'a str, &'a str, Option<&'a str>)>,
}

#[tracing::instrument]
pub async fn generate_playlist_export_at(
    playlist: &Playlist,
    target: &Path,
    landscape: bool,
) -> Result<(), ReportError> {
    let working_dir = tempfile::tempdir().map_err(|e| {
        tracing::debug!("Failed to create temp dir: {}", e);
        ReportError::IOError
    })?;

    prepare_report_directory(working_dir.path()).await?;

    let stringified_items = playlist
        .items
        .iter()
        .map(|item| match item {
            PlaylistItem::Playback(AudioPlayback { audio_source, .. }) => {
                // TODO: use playback properties?
                match audio_source {
                    AudioSource::ResolvedTrack(entity) => (
                        entity
                            .body
                            .track
                            .track_title()
                            .map_or_else(|| "Unknown title".into(), ToString::to_string),
                        get_primary_dance(&entity.body.track)
                            .map(ToString::to_string)
                            .unwrap_or_default(),
                        get_mpm(&entity.body.track).map(|mpm| format!("{mpm}")),
                    ),
                    AudioSource::TextToSpeech(tts) => {
                        (format!("Announcement “{tts}”"), "".to_string(), None)
                    },
                    AudioSource::TrackQuery(query) => {
                        let dances = extract_dances(query);
                        let mpm = extract_mpm_range(query)
                            .map_err(|e| {
                                tracing::debug!(
                                    "Failed to parse query for MPM: query “{query}”, error {e}"
                                );
                                ReportError::ReportGenerationFailed
                            })
                            .ok()
                            .flatten()
                            .map(|mpm_range| {
                                if mpm_range.end - mpm_range.start == 1 {
                                    format!("{}", mpm_range.start)
                                } else {
                                    format!("{}–{}", mpm_range.start, mpm_range.end - 1)
                                }
                            });
                        (format!("Track by query “{query}”"), dances.join(", "), mpm)
                    },
                    AudioSource::TrackUid(uid) => {
                        // We do not handle track UIDs gracefully. Callers are expected
                        // to resolve tracks for more extensive details.
                        (format!("Track by UID “{uid}”"), "".to_string(), None)
                    },
                }
            },
            PlaylistItem::Pause(duration) => (
                format!("Pause ({} s)", duration.as_secs()),
                "".to_string(),
                None,
            ),
            PlaylistItem::Break => ("Planned Break".to_string(), "".to_string(), None),
            PlaylistItem::Stop => ("End of Playback".to_string(), "".to_string(), None),
            // TODO: allow actual separators
            PlaylistItem::Visual(Some(note)) => (format!("<em>{note}</em>"), "".to_string(), None),
            PlaylistItem::Visual(None) => ("".to_string(), "".to_string(), None),
        })
        .collect::<Vec<_>>();

    let playlist_length_estimate = playlist.estimate_length();
    call_weasyprint(
        &PlaylistTemplate {
            title: playlist.title.as_str(),
            formatted_length: format!(
                "{}{}",
                format_duration_millis(playlist_length_estimate.duration.as_millis()),
                if playlist_length_estimate.is_estimate {
                    " (estimated)"
                } else {
                    ""
                }
            )
            .as_str(),
            notes: "",
            tags: playlist.played_at.iter().map(|t| t as &str).collect(),
            playlist: stringified_items
                .iter()
                .map(|(t, m, p)| (t.as_str(), m.as_str(), p.as_deref()))
                .collect(),
        }
        .render()
        .map_err(|e| {
            tracing::debug!("Calling weasyprint failed: {}", e);
            ReportError::ReportGenerationFailed
        })?,
        working_dir.path(),
        target,
    )
    .await
}
