use std::path::Path;

use samba_backend_reports::render_plain_document;

#[tokio::main]
async fn main() {
    // HINT: do not use on windows ;)
    let event_sample = Path::new("/tmp/event-sample.pdf");
    let playlist_sample = Path::new("/tmp/playlist-sample.pdf");
    let playlist_ls_sample = Path::new("/tmp/playlist-landscape-sample.pdf");

    let failure = render_plain_document(
        include_str!("../examples-data/event-sample.html"),
        event_sample,
    )
    .await
    .is_err()
        || render_plain_document(
            include_str!("../examples-data/playlist-sample.html"),
            playlist_sample,
        )
        .await
        .is_err()
        || render_plain_document(
            include_str!("../examples-data/playlist-landscape-sample.html"),
            playlist_ls_sample,
        )
        .await
        .is_err();
    if failure {
        eprintln!("Report generation failed");
    } else {
        println!("Enjoy your reports in /tmp");
    };
}
