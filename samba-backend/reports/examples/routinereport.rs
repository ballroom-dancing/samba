use std::path::Path;

use samba_backend_reports::generate_routine_export_at;
use samba_shared::routines::{
    Figure, Foot, LevelRequirement, Routine, RoutineElement, Step, StepTiming, WeightTransfer,
};

#[tokio::main]
async fn main() {
    // HINT: do not use on windows ;)
    let path = Path::new("/tmp/out.pdf");

    if generate_routine_export_at(
        &Routine {
            id: 0,
            title: Some("Standard Example".into()),
            description: None,
            choreographer: None,
            performers: vec![],
            demonstration: vec![],
            steps: vec![
                // Wait and do one preparation step before starting the routine.
                // May also be used to describe starting on a 5 (against phrase)
                // and similar choreographic elements.
                RoutineElement::Pause(StepTiming::Slow),
                RoutineElement::Step(
                    Step::new(Foot::Left, StepTiming::Quick, WeightTransfer::Full)
                        .with_comment("start diagonally facing wall on long side".into()),
                ),
                // Start the routine with a fully described figure.
                RoutineElement::Figure(
                    Figure::new("Natural Turn Steps 1–3".into())
                        .with_steps(
                            vec![
                                Step::new(Foot::Right, StepTiming::Quick, WeightTransfer::Full),
                                Step::new(Foot::Left, StepTiming::Quick, WeightTransfer::Full),
                                Step::new(Foot::Right, StepTiming::Quick, WeightTransfer::Full),
                            ],
                            vec![
                                Step::new(Foot::Left, StepTiming::Quick, WeightTransfer::Full),
                                Step::new(Foot::Right, StepTiming::Quick, WeightTransfer::Full),
                                Step::new(Foot::Left, StepTiming::Quick, WeightTransfer::Full),
                            ],
                        )
                        .expect("The step counts are equal for leader and follower.")
                        .with_level_requirement(LevelRequirement::WdsfDCSyllabus),
                ),
                // Figures can also be added in a prototype fashion without steps
                // although this may make it impossible to determine the length of
                // the routine and similar calculations.
                RoutineElement::Figure(
                    Figure::new("Overturned Natural Spin Turn".into())
                        .with_level_requirement(LevelRequirement::WdsfDCSyllabus),
                ),
                RoutineElement::Figure(
                    Figure::new("Reverse Turn Steps 4–6".into())
                        .with_level_requirement(LevelRequirement::WdsfDCSyllabus),
                ),
                RoutineElement::Figure(
                    Figure::new("Whisk".into())
                        .with_level_requirement(LevelRequirement::WdsfDCSyllabus),
                ),
                RoutineElement::Figure(
                    Figure::new("Progressive Chasse to Left".into())
                        .with_level_requirement(LevelRequirement::WdsfDCSyllabus),
                ),
                RoutineElement::Figure(
                    Figure::new("Natural Turn Steps 1–3".into())
                        .with_level_requirement(LevelRequirement::WdsfDCSyllabus),
                ),
                RoutineElement::Separator(Some("start of the short side".into())),
                RoutineElement::Figure(
                    Figure::new("Impetus".into())
                        .with_level_requirement(LevelRequirement::WdsfDCSyllabus),
                ),
                RoutineElement::Figure(
                    Figure::new("Weave".into())
                        .with_level_requirement(LevelRequirement::WdsfDCSyllabus),
                ),
                RoutineElement::Figure(
                    Figure::new("Curved Feather".into())
                        .with_level_requirement(LevelRequirement::WdsfDCSyllabus),
                ),
                RoutineElement::Figure(
                    Figure::new("Outside Spin".into())
                        .with_level_requirement(LevelRequirement::WdsfDCSyllabus),
                ),
                RoutineElement::Figure(
                    Figure::new("Progressive Chasse to Left".into())
                        .with_level_requirement(LevelRequirement::WdsfDCSyllabus),
                ),
                // Skip 3 full quicks/beats (i.e. the preparation steps) when
                // starting this routine again on the long side.
                RoutineElement::Repeat(StepTiming::Custom(3 * 4)),
            ],
            notes: None,
        },
        path,
    )
    .await
    .is_err()
    {
        eprintln!("Report generation failed");
    } else {
        println!("Enjoy your report at {:?}", path);
    };
}
