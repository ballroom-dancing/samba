use leptos::*;

#[must_use]
#[component]
pub fn DropdownSelect(
    bind_signal: RwSignal<String>,
    options: Vec<String>,
    #[prop(default = false)] required: bool,
    #[prop(optional)] classes: Option<String>,
) -> impl IntoView {
    let items = options
        .clone()
        .into_iter()
        .map(|option| {
            let option_clone = option.clone();
            view! {
                <li>
                    <a on:click=move |_| {
                        bind_signal.update(|s| *s = option.clone());
                        // TODO: remove focus from dropdown list
                    }>{option_clone}</a>
                </li>
            }
        })
        .collect::<Vec<_>>();
    let valid_option = Memo::new(move |_| options.contains(&bind_signal()));

    view! {
        <div
            class=format!("dropdown {}", classes.unwrap_or_default())
            class:tooltip=required
            data-tip=required.then_some("required").unwrap_or_default()
        >
            <label
                class="btn m-1 w-full"
                class:tooltip=move || required
                class:btn-success=valid_option
                class:btn-error=move || !valid_option() && required
                class:btn-outline=move || !valid_option() && !required
                tabindex="0"
            >
                {move || {
                    if valid_option() { bind_signal().clone() } else { "Please choose one".into() }
                }}
                <span class="fa-solid fa-caret-down ml-1" aria-hidden="true"></span>
            </label>
            <ul
                tabindex="1"
                class="dropdown-content z-10 menu p-2 shadow bg-base-100 rounded-box w-full"
            >
                {items}
            </ul>
        </div>
    }
}
