use leptos::*;

#[must_use]
#[component]
pub fn Placeholder(
    #[prop(optional)] icon: Option<View>,
    #[prop(optional)] title: Option<View>,
    #[prop(optional)] subtitle: Option<View>,
    #[prop(optional)] commands: Option<Vec<View>>,
) -> impl IntoView {
    view! {
        <div role="alert" class="alert gap-8">
            {icon
                .map(|icon| {
                    view! { <div class="placeholder-icon">{icon}</div> }
                })}
            <div class="block">
                {title
                    .map(|title| {
                        view! { <h2 class="font-bold text-lg">{title}</h2> }
                    })}
                {subtitle
                    .map(|subtitle| {
                        view! { <h3>{subtitle}</h3> }
                    })}
            </div>
            {commands}
        </div>
    }
}
