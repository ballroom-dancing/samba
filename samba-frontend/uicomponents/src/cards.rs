use leptos::*;

#[must_use]
#[component]
pub fn Card(
    #[prop(optional)] title: Option<String>,
    #[prop(optional)] classes: Option<String>,
    #[prop(optional)] action_bar: Option<View>,
    children: Children,
) -> impl IntoView {
    view! {
        <div class=format!("card shadow-lg {}", classes.unwrap_or_default())>
            <div class="card-body">
                {title
                    .map(|title| {
                        view! { <h2 class="card-title">{title}</h2> }
                    })} {children()}
                {action_bar
                    .map(|action_bar| {
                        view! { <div class="card-actions grid grid-flow-col">{action_bar}</div> }
                    })}
            </div>
        </div>
    }
}

#[must_use]
#[component]
pub fn LRTileCard(
    #[prop(optional)] left: Option<View>,
    #[prop(optional)] right: Option<View>,
) -> impl IntoView {
    view! {
        <Card classes="card-compact shadow-md py-1 my-2".into()>
            <div class="flex items-center">
                <div class="grow">{left}</div>
                <div class="tile__buttons">{right}</div>
            </div>
        </Card>
    }
}
