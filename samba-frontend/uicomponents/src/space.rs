use leptos::*;

#[derive(Copy, Clone, Default, Debug)]
pub enum SpaceSize {
    #[default]
    Normal,
    Large,
    ExtraLarge,
}
impl SpaceSize {
    #[must_use]
    pub const fn to_tailwind_class(&self) -> &'static str {
        match self {
            SpaceSize::Normal => "h-10",
            SpaceSize::Large => "h-20",
            SpaceSize::ExtraLarge => "h-30",
        }
    }
}

#[must_use]
#[component]
pub fn Space(#[prop(optional)] size: SpaceSize) -> impl IntoView {
    view! { <div class=size.to_tailwind_class()></div> }
}
