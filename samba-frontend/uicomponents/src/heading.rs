use leptos::*;

#[derive(Copy, Clone, Debug, Default)]
pub enum HeadingLevel {
    #[default]
    Page,
    Section,
}

impl HeadingLevel {
    #[must_use]
    pub const fn to_tailwind_class(&self) -> &'static str {
        match self {
            HeadingLevel::Page => "text-2xl font-bold pb-4 pt-0",
            HeadingLevel::Section => "text-xl font-bold pt-4 pb-2",
        }
    }
}

#[must_use]
#[component]
pub fn Heading(
    #[prop(optional)] level: HeadingLevel,
    #[prop(optional)] classes: Option<String>,
    children: Children,
) -> impl IntoView {
    match level {
        HeadingLevel::Page => view! {
            <h1 class=format!(
                "{} {}",
                level.to_tailwind_class(),
                classes.unwrap_or_default(),
            )>{children()}</h1>
        }
        .into_view(),
        HeadingLevel::Section => view! {
            <h2 class=format!(
                "{} {}",
                level.to_tailwind_class(),
                classes.unwrap_or_default(),
            )>{children()}</h2>
        }
        .into_view(),
    }
}
