use leptos::*;

#[derive(Debug, Copy, Clone, Default)]
pub enum DialogButtons {
    Ok,
    #[default]
    OkCancel,
    YesNo,
    YesNoCancel,
}

#[must_use]
#[component]
pub fn ConfirmationModal(
    open: RwSignal<bool>,
    confirmation: WriteSignal<Option<bool>>,
    #[prop(optional)] title: Option<String>,
    #[prop(optional)] classes: Option<String>,
    #[prop(optional)] box_classes: Option<String>,
    #[prop(optional)] buttons: DialogButtons,
    children: Children,
) -> impl IntoView {
    view! {
        <dialog open=open class=format!("modal {}", classes.unwrap_or_default())>
            <div
                class=format!(
                    "modal-box md:flex md:flex-col md:justify-between {}",
                    box_classes.unwrap_or_default(),
                )
                role="document"
            >
                <div class="flex items-center">
                    <h2 class="grow font-bold text-xl">
                        {title.unwrap_or("Please confirm".into())}
                    </h2>
                    {matches!(buttons, DialogButtons::OkCancel | DialogButtons::YesNoCancel)
                        .then(|| {
                            view! {
                                <a
                                    class="btn btn-circle btn-ghost"
                                    aria-label="Close"
                                    on:click=move |_| {
                                        confirmation.set(None);
                                        open.set(false);
                                    }
                                >
                                    <span class="fa-wrapper fa fa-times"></span>
                                </a>
                            }
                        })}
                </div>
                {children()}
                <div class="mt-auto flex gap-2 items-center justify-end">
                    {matches!(buttons, DialogButtons::OkCancel | DialogButtons::YesNoCancel)
                        .then(|| {
                            view! {
                                <button
                                    class="btn"
                                    on:click=move |_| {
                                        confirmation.set(None);
                                        open.set(false);
                                    }
                                    type="button"
                                >
                                    "Cancel"
                                </button>
                            }
                        })}
                    {matches!(buttons, DialogButtons::YesNoCancel)
                        .then(|| {
                            view! {
                                <button
                                    class="btn"
                                    on:click=move |_| {
                                        confirmation.set(Some(false));
                                        open.set(false);
                                    }
                                    type="button"
                                >
                                    "No"
                                </button>
                            }
                        })}
                    <button
                        class="btn btn-primary"
                        on:click=move |_| {
                            confirmation.set(Some(true));
                            open.set(false);
                        }
                    >
                        {if matches!(buttons, DialogButtons::Ok | DialogButtons::OkCancel) {
                            "Ok"
                        } else {
                            "Yes"
                        }}
                    </button>
                </div>
            </div>
        </dialog>
    }
}
