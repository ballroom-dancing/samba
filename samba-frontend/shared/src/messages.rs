use uuid::Uuid;

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Message {
    pub id: Uuid,
    pub title: String,
    pub message: String,
    pub kind: MessageKind,
}

impl Message {
    #[must_use]
    pub fn new_error(title: String, message: String) -> Self {
        Message {
            id: Uuid::new_v4(),
            title,
            message,
            kind: MessageKind::Error,
        }
    }

    #[must_use]
    pub fn new_info(title: String, message: String) -> Self {
        Message {
            id: Uuid::new_v4(),
            title,
            message,
            kind: MessageKind::Information,
        }
    }
}

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum MessageKind {
    Error,
    Information,
}
