use codee::string::JsonSerdeWasmCodec;
use leptos::*;
use leptos_use::storage::use_local_storage;
use samba_shared::preferences::UserPreferences;
use uuid::Uuid;

mod messages;
pub use messages::*;

pub mod profile;
use profile::{LOCAL_STORAGE_PROFILE_KEY, UserProfile};

#[derive(Debug, Clone)]
pub struct AppState {
    pub backend_url: String,
    pub running_in_tauri: bool,
    pub messages: RwSignal<Vec<ReadSignal<Message>>>,
    pub user_preferences: RwSignal<UserPreferences>,
}

impl AppState {
    #[must_use]
    pub fn new(backend_url: String, running_in_tauri: bool) -> Self {
        Self {
            backend_url,
            running_in_tauri,
            messages: RwSignal::new(vec![]),
            user_preferences: RwSignal::new(UserPreferences::default()),
        }
    }

    pub fn add_message(&self, message: Message) {
        let (message, _) = create_signal(message);
        self.messages.update(|messages| messages.push(message));
    }

    pub fn remove_message(&self, id: Uuid) {
        self.messages
            .update(|messages| messages.retain(|msg| msg.get().id != id));
    }

    pub fn replace_user_preferences(&self, preferences: UserPreferences) {
        self.user_preferences.update(|p| *p = preferences);
    }

    /// Gets the current JWT for requests. An empty string as special kind of
    /// invalid token if no user profile exists.
    #[must_use]
    pub fn get_jwt(&self) -> String {
        let (profile, _, _) =
            use_local_storage::<UserProfile, JsonSerdeWasmCodec>(LOCAL_STORAGE_PROFILE_KEY);
        profile.get().jwt
    }

    /// Checks if an experimental feature is enabled.
    #[must_use]
    pub fn is_experimental_feature_enabled(&self, name: &str) -> bool {
        self.user_preferences
            .with(|pref| pref.experimental_feature_opt_in.contains(name))
    }
}
