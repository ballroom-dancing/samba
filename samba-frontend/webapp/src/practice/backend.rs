use anyhow::{anyhow, Context, Result};
use aoide::{
    json::{
        playlist::{
            Entity as SerialPlaylistEntity, EntityWithEntries as SerialPlaylistEntityWithEntries,
            PlaylistWithEntries as SerialPlaylistWithEntries,
        },
        track::Entity as SerialTrackEntity,
    },
    playlist::{EntityWithEntries as PlaylistEntityWithEntries, PlaylistWithEntries},
    PlaylistEntity, TrackEntity, TrackUid,
};
use samba_shared::tracks::WebsurferJsWaveformData;

use crate::util::{
    delete_from_remote, get_from_remote, get_from_remote_with_query, post_to_remote,
};

pub async fn get_session_by_id(id: &str) -> Result<PlaylistEntityWithEntries> {
    get_from_remote::<SerialPlaylistEntityWithEntries>(&format!("playlists/{id}"))
        .await
        .map(Into::into)
}

pub async fn delete_session(id: &str) -> Result<()> {
    delete_from_remote(&format!("playlists/{}", id)).await
}

pub async fn fetch_history_sessions() -> Result<Vec<PlaylistEntityWithEntries>> {
    Ok(
        get_from_remote::<Vec<SerialPlaylistEntityWithEntries>>("playlists")
            .await?
            .into_iter()
            .map(Into::into)
            .filter(|pwe: &PlaylistEntityWithEntries| {
                pwe.body.playlist.kind == Some("practice-history".into())
            })
            .collect(),
    )
}

pub async fn fetch_practice_sessions() -> Result<Vec<PlaylistEntityWithEntries>> {
    Ok(
        get_from_remote::<Vec<SerialPlaylistEntityWithEntries>>("playlists")
            .await?
            .into_iter()
            .map(Into::into)
            .filter(|pwe: &PlaylistEntityWithEntries| {
                pwe.body.playlist.kind == Some("practice-session".into())
            })
            .collect(),
    )
}

pub async fn create_practice_session(session: PlaylistWithEntries) -> Result<PlaylistEntity> {
    let serial_session: SerialPlaylistWithEntries = session.into();
    let serial_entity =
        post_to_remote::<SerialPlaylistEntity, _>("playlists/create", serial_session).await?;

    Ok(serial_entity.into())
}

pub async fn update_practice_session(
    id: &str,
    session: PlaylistEntityWithEntries,
) -> Result<PlaylistEntity> {
    let serial_session: SerialPlaylistEntityWithEntries = session.into();
    let serial_entity =
        post_to_remote::<SerialPlaylistEntity, _>(&format!("playlists/{id}"), serial_session)
            .await?;

    Ok(serial_entity.into())
}

pub async fn fetch_random_track_for_query(query: &str) -> Result<TrackEntity> {
    // TODO: respect user's exclude list from settings
    #[derive(serde::Serialize)]
    struct QueryParam {
        query: String,
        number_of_tracks: usize,
    }

    let query_param = QueryParam {
        query: query.into(),
        number_of_tracks: 1,
    };

    let serial_tracks: Vec<SerialTrackEntity> =
        get_from_remote_with_query("tracks/random", Some(query_param)).await?;
    if serial_tracks.is_empty() {
        return Err(anyhow!("no tracks found for random query"));
    }

    serial_tracks
        .into_iter()
        .next()
        .with_context(|| "track out of range (cannot happen)")?
        .try_into()
}

pub async fn fetch_waveform_for_track(track_uid: &TrackUid) -> Result<Vec<f64>> {
    let waveform_data: WebsurferJsWaveformData =
        get_from_remote(&format!("tracks/{track_uid}/waveform")).await?;
    Ok(waveform_data.data)
}
