use std::num::NonZeroUsize;

#[derive(Clone)]
pub enum PracticeAction {
    AdHoc,
    Edit(Option<String>),
    History,
    NewBasedOn(String),
    List,
    Saved(String, Option<NonZeroUsize>),
}

impl std::fmt::Display for PracticeAction {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(&match self {
            PracticeAction::AdHoc => "/adhoc".into(),
            PracticeAction::Edit(None) => "/create".into(),
            PracticeAction::Edit(Some(id)) => format!("/update/{}", id.as_str()),
            PracticeAction::History => "/history".into(),
            PracticeAction::List => "/list".into(),
            PracticeAction::Saved(id, heats) => format!(
                "/saved/{}{}",
                id.as_str(),
                if let Some(heats) = heats {
                    format!("?heats={}", heats.get())
                } else {
                    "".into()
                }
            ),
            PracticeAction::NewBasedOn(id) => format!("/create-off/{}", id.as_str()),
        })
    }
}
