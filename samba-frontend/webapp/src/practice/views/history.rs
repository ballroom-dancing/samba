use aoide::{
    playlist::EntityWithEntries as PlaylistEntityWithEntries,
    util::clock::{DateOrDateTime, YyyyMmDdDate},
};
use leptos::*;
use samba_frontend_shared::AppState;
use samba_frontend_uicomponents::{ConfirmationModal, DialogButtons, Heading, Placeholder};
use samba_shared::{playlists::Playlist, utils::format_duration_millis};

use super::super::{
    backend::{delete_session, fetch_history_sessions},
    navigation::PracticeAction,
};
use crate::{
    practice::views::get_last_modification_date, util::refresh_page, views::PleaseWaitView,
    AppRoutes,
};

#[component]
pub fn PracticeHistoryView() -> impl IntoView {
    // TODO: use Resource::new with serial types?
    let sessions = create_local_resource(
        || (),
        move |_| async move { fetch_history_sessions().await.unwrap_or_default() },
    );

    view! {
        <section>
            <Heading>"Practice session history"</Heading>
            <Suspense fallback=|| {
                view! { <PleaseWaitView /> }
            }>
                {move || {
                    sessions().map(|sessions| view! { <HistoryListHandler sessions=sessions /> })
                }}

            </Suspense>
        </section>
    }
}

#[component]
fn HistoryListHandler(sessions: Vec<PlaylistEntityWithEntries>) -> impl IntoView {
    let (sessions, _) = create_signal(sessions);

    view! {
        <section>
            <Show
                when=move || !sessions().is_empty()
                fallback=|| {
                    view! {
                        <Placeholder
                            icon=view! {
                                <span
                                    class="fa-solid fa-stop-circle"
                                    style="font-size: 500%;"
                                ></span>
                            }
                                .into_view()
                            title="There are no history sessions".into_view()
                            subtitle="Play back practice sessions to see their history here."
                                .into_view()
                            commands=vec![
                                view! {
                                    <div class="btn-container">
                                        <a
                                            class="btn btn-primary u-inline-block"
                                            href=AppRoutes::Practice(PracticeAction::List)
                                        >
                                            "Go to practice session overview"
                                        </a>
                                    </div>
                                }
                                    .into_view(),
                            ]
                        />
                    }
                }
            >
                <table class="table">
                    <thead>
                        <tr>
                            <th>"Date"</th>
                            <th>"Title"</th>
                            <th>"Tags"</th>
                            <th>"Length"</th>
                            <th>"Actions"</th>
                        </tr>
                    </thead>
                    <tbody>
                        <For each=sessions key=|x| x.hdr.uid.clone() let:pwe>
                            <SessionRow session=pwe />
                        </For>
                    </tbody>
                </table>
            // TODO: pagination
            </Show>
        </section>
    }
}

#[component]
fn SessionRow(session: PlaylistEntityWithEntries) -> impl IntoView {
    let app_state =
        use_context::<AppState>().expect("Sessions are only shown when logged in and initialized.");

    let (title, _) = create_signal(session.body.playlist.title.clone());
    let (session_id, _) = create_signal(session.hdr.uid.to_string());
    let playlist: Playlist = session.body.clone().into();

    let playlist_length_estimate = playlist.estimate_length();
    let length_estimate = format!(
        "{}{}",
        if playlist_length_estimate.is_estimate {
            "≥ "
        } else {
            ""
        },
        format_duration_millis(playlist_length_estimate.duration.as_millis())
    );

    let last_mod_date = get_last_modification_date(&session.body).map_or_else(
        || "Unknown".into(),
        |date| {
            let dt = DateOrDateTime::from(date);
            let d = YyyyMmDdDate::from(&dt);
            format!("{d}")
        },
    );

    let delete_dialog_open = RwSignal::new(false);
    let (deletion_confirmed, set_deletion_confirmed) = create_signal(None);
    let delete_action = move |_| {
        delete_dialog_open.set(true);
    };
    Effect::new(move |_| {
        if deletion_confirmed() == Some(true) {
            // TODO: error handling
            spawn_local(async move {
                delete_session(&session_id()).await.unwrap();
                refresh_page();
            });
        }
    });

    view! {
        <tr>
            <td>{last_mod_date.clone()}</td>
            <td>
                <a href=AppRoutes::Practice(
                    PracticeAction::Saved(session_id().clone(), None),
                )>{title}</a>
            </td>
            <td>"TODO: Tags"</td>
            <td>{length_estimate}</td>
            <td class="flex gap-2">
                // TODO: extract number of heats by looking at the first set of
                //       consecutive resolved tracks
                <span class="tooltip" data-tip="Play">
                    <a class="btn btn-primary"
                       href=AppRoutes::Practice(PracticeAction::Saved(session_id().clone(), None)).to_string()
                       role="button">
                        <span class="fa-regular fa-circle-play" />
                    </a>
                </span>
                <span class="tooltip" data-tip="New playlist based on this">
                    <a
                        class="btn"
                        href=AppRoutes::Practice(PracticeAction::NewBasedOn(session_id().clone())).to_string()
                        role="button"
                    >
                        <span class="fa-solid fa-up-right-from-square"></span>
                    </a>
                </span>
                <span class="tooltip" data-tip="Export PDF">
                    <a
                        class="btn"
                        download=format!("{}.pdf", title())
                        href=format!("{}/playlists/{}/export", app_state.backend_url, session_id())
                        rel="external"
                    >
                        <span class="fa-regular fa-file-pdf"></span>
                    </a>
                </span>
                <button
                    class="btn btn-error tooltip"
                    data-tip="Delete"
                    on:click=delete_action
                    type="button"
                >
                    <span class="fa-regular fa-trash-can"></span>
                </button>
            </td>
        </tr>
        <ConfirmationModal
            open=delete_dialog_open
            confirmation=set_deletion_confirmed
            buttons=DialogButtons::YesNo
        >
            "Do you really want to delete the playback of session “"
            {title}
            "” dated "
            {last_mod_date}
            "?"
        </ConfirmationModal>
    }
}
