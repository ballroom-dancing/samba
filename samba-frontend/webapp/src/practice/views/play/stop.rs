use leptos::*;
use samba_frontend_uicomponents::LRTileCard;

#[component]
pub fn StopView() -> impl IntoView {
    // We will ignore the active signal and never get out of it again.
    // Therefore, the playlist will never continue.
    view! { <LRTileCard left=view! { <span class="font-bold">"Stop"</span> }.into_view() /> }
}
