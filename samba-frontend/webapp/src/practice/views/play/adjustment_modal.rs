use std::{num::NonZeroUsize, time::Duration};

use leptos::*;
use leptos_router::Params;

#[derive(Clone, Debug)]
pub struct PlaybackAdjustments {
    pub heats: usize,
    pub skip_breaks: bool,
    pub pause_duration_override: Option<Duration>,
    pub track_duration_override: Option<Duration>,
    pub track_offset_override: Option<Duration>,
    pub fade_in_override: Option<Duration>,
    pub fade_out_override: Option<Duration>,
}

#[derive(Debug, Clone, Params, PartialEq)]
pub struct PlaybackAdjustmentQuery {
    heats: Option<NonZeroUsize>,
    skip_breaks: Option<bool>,
    pause_duration_override: Option<f64>,
    track_duration_override: Option<f64>,
    track_offset_override: Option<f64>,
    fade_in_override: Option<f64>,
    fade_out_override: Option<f64>,
}

impl From<PlaybackAdjustmentQuery> for PlaybackAdjustments {
    fn from(value: PlaybackAdjustmentQuery) -> Self {
        Self {
            heats: value.heats.map_or(1, |nzu| nzu.get()),
            skip_breaks: value.skip_breaks.unwrap_or(false),
            pause_duration_override: value.pause_duration_override.map(Duration::from_secs_f64),
            track_duration_override: value.track_duration_override.map(Duration::from_secs_f64),
            track_offset_override: value.track_offset_override.map(Duration::from_secs_f64),
            fade_in_override: value.fade_in_override.map(Duration::from_secs_f64),
            fade_out_override: value.fade_out_override.map(Duration::from_secs_f64),
        }
    }
}

#[component]
pub fn PlaybackAdjustmentModal(
    visibility: RwSignal<bool>,
    adjustments: RwSignal<PlaybackAdjustments>,
) -> impl IntoView {
    let (heats, set_heats) = create_signal(adjustments().heats);
    let (skip_breaks, set_skip_breaks) = create_signal(adjustments().skip_breaks);

    const DEFAULT_PAUSE_DURATION: Duration = Duration::from_secs(10);
    let (pause_duration, set_pause_duration) = create_signal(adjustments().pause_duration_override);
    let toggle_pause_adjustment = move |ev| {
        if event_target_checked(&ev) {
            set_pause_duration(Some(DEFAULT_PAUSE_DURATION));
        } else {
            set_pause_duration(None);
        }
    };

    const DEFAULT_TRACK_DURATION: Duration = Duration::from_secs(90);
    let (track_duration, set_track_duration) = create_signal(adjustments().track_duration_override);
    let toggle_duration_adjustment = move |ev| {
        if event_target_checked(&ev) {
            set_track_duration(Some(DEFAULT_TRACK_DURATION));
        } else {
            set_track_duration(None);
        }
    };

    const DEFAULT_TRACK_OFFSET: Duration = Duration::ZERO;
    let (track_offset, set_track_offset) = create_signal(adjustments().track_offset_override);
    let toggle_offset_adjustment = move |ev| {
        if event_target_checked(&ev) {
            set_track_offset(Some(DEFAULT_TRACK_OFFSET));
        } else {
            set_track_offset(None);
        }
    };

    const DEFAULT_FADE_DURATION: Duration = Duration::from_secs(5);
    let (fade_in, set_fade_in) = create_signal(adjustments().fade_in_override);
    let (fade_out, set_fade_out) = create_signal(adjustments().fade_out_override);
    let toggle_fade_in_adjustment = move |ev| {
        if event_target_checked(&ev) {
            set_fade_in(Some(DEFAULT_FADE_DURATION));
        } else {
            set_fade_in(None);
        }
    };
    let toggle_fade_out_adjustment = move |ev| {
        if event_target_checked(&ev) {
            set_fade_out(Some(DEFAULT_FADE_DURATION));
        } else {
            set_fade_out(None);
        }
    };

    let apply_adjustments = move |_| {
        visibility.set(false);
        adjustments.set(PlaybackAdjustments {
            heats: heats(),
            skip_breaks: skip_breaks(),
            pause_duration_override: pause_duration(),
            track_duration_override: track_duration(),
            track_offset_override: track_offset(),
            fade_in_override: fade_in(),
            fade_out_override: fade_out(),
        });
    };

    view! {
        <dialog open=visibility class="modal modal-bottom sm:modal-middle">
            <div
                class="modal-box md:flex md:flex-col md:justify-between md:h-[60%] md:min-h-[60%]"
                role="document"
            >
                <div class="flex items-center">
                    <h2 class="grow font-bold text-xl">"Adjust session playback"</h2>
                    <a
                        class="btn btn-circle btn-ghost"
                        aria-label="Close"
                        on:click=move |_| {
                            visibility.set(false);
                        }
                    >
                        <span class="fa-wrapper fa fa-times"></span>
                    </a>
                </div>
                <div class="flex gap-4 items-center mt-8 mb-2">
                    <input
                        class="toggle"
                        type="checkbox"
                        prop:checked=skip_breaks
                        on:input=move |ev| {
                            set_skip_breaks(event_target_checked(&ev));
                        }
                    />
                    <label>"Skip breaks"</label>
                </div>
                <div class="flex gap-4 items-center my-2">
                    <input
                        class="toggle"
                        type="checkbox"
                        prop:checked=move || heats() != 1
                        on:input=move |ev| {
                            if event_target_checked(&ev) {
                                set_heats(2);
                            } else {
                                set_heats(1);
                            }
                        }
                    />
                    <label class="grow">"Multiple heats"</label>
                    <input
                        class="input input-bordered w-32"
                        type="number"
                        min="0"
                        max="20"
                        disabled=move || heats() == 1
                        prop:value=heats
                        on:input=move |ev| {
                            if let Ok(val) = event_target_value(&ev).parse::<usize>() {
                                set_heats(val);
                            }
                        }
                    />
                    <span class="invisible">"s"</span>
                </div>
                <div class="flex gap-4 items-center my-2">
                    <input
                        class="toggle"
                        type="checkbox"
                        prop:checked=move || pause_duration().is_some()
                        on:input=toggle_pause_adjustment
                    />
                    <label class="grow">"Change all pauses"</label>
                    <input
                        class="input input-bordered w-32"
                        type="number"
                        min="0"
                        max="3600"
                        disabled=move || pause_duration().is_none()
                        prop:value=move || {
                            pause_duration().unwrap_or(DEFAULT_PAUSE_DURATION).as_secs()
                        }
                        on:input=move |ev| {
                            if let Ok(val) = event_target_value(&ev)
                                .parse::<u64>()
                                .map(Duration::from_secs)
                            {
                                set_pause_duration(Some(val));
                            }
                        }
                    />
                    <span>"s"</span>
                </div>
                <div class="flex gap-4 items-center my-2">
                    <input
                        class="toggle"
                        type="checkbox"
                        prop:checked=move || track_duration().is_some()
                        on:input=toggle_duration_adjustment
                    />
                    <label class="grow">"Limit all durations"</label>
                    <input
                        class="input input-bordered w-32"
                        type="number"
                        min="0"
                        disabled=move || track_duration().is_none()
                        prop:value=move || {
                            track_duration().unwrap_or(DEFAULT_TRACK_DURATION).as_secs()
                        }
                        on:input=move |ev| {
                            if let Ok(val) = event_target_value(&ev)
                                .parse::<u64>()
                                .map(Duration::from_secs)
                            {
                                set_track_duration(Some(val));
                            }
                        }
                    />
                    <span>"s"</span>
                </div>
                <div class="flex gap-4 items-center my-2">
                    <input
                        class="toggle"
                        type="checkbox"
                        prop:checked=move || track_offset().is_some()
                        on:input=toggle_offset_adjustment
                    />
                    <label class="grow">"Start all with offset"</label>
                    <input
                        class="input input-bordered w-32"
                        type="number"
                        min="0"
                        disabled=move || track_offset().is_none()
                        prop:value=move || track_offset().unwrap_or(DEFAULT_TRACK_OFFSET).as_secs()
                        on:input=move |ev| {
                            if let Ok(val) = event_target_value(&ev)
                                .parse::<u64>()
                                .map(Duration::from_secs)
                            {
                                set_track_offset(Some(val));
                            }
                        }
                    />
                    <span>"s"</span>
                </div>
                <div class="flex gap-4 items-center my-2">
                    <input
                        class="toggle"
                        type="checkbox"
                        prop:checked=move || fade_in().is_some()
                        on:input=toggle_fade_in_adjustment
                    />
                    <label class="grow">"Fade in (all) duration"</label>
                    <input
                        class="input input-bordered w-32"
                        type="number"
                        min="0"
                        disabled=move || fade_in().is_none()
                        prop:value=move || fade_in().unwrap_or(DEFAULT_FADE_DURATION).as_secs()
                        on:input=move |ev| {
                            if let Ok(val) = event_target_value(&ev)
                                .parse::<u64>()
                                .map(Duration::from_secs)
                            {
                                set_fade_in(Some(val));
                            }
                        }
                    />
                    <span>"s"</span>
                </div>
                <div class="flex gap-4 items-center my-2">
                    <input
                        class="toggle"
                        type="checkbox"
                        prop:checked=move || fade_out().is_some()
                        on:input=toggle_fade_out_adjustment
                    />
                    <label class="grow">"Fade out (all) duration"</label>
                    <input
                        class="input input-bordered w-32"
                        type="number"
                        min="0"
                        disabled=move || fade_out().is_none()
                        prop:value=move || fade_out().unwrap_or(DEFAULT_FADE_DURATION).as_secs()
                        on:input=move |ev| {
                            if let Ok(val) = event_target_value(&ev)
                                .parse::<u64>()
                                .map(Duration::from_secs)
                            {
                                set_fade_out(Some(val));
                            }
                        }
                    />
                    <span>"s"</span>
                </div>
                <div class="mt-auto flex gap-2 items-center justify-end">
                    <button
                        class="btn"
                        on:click=move |_| {
                            visibility.set(false);
                        }
                        type="button"
                    >
                        "Cancel"
                    </button>
                    <button class="btn btn-primary" on:click=apply_adjustments>
                        "Apply"
                    </button>
                </div>
            </div>
        </dialog>
    }
}
