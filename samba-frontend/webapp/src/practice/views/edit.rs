use aoide::{
    playlist::{
        EntityWithEntries as PlaylistEntityWithEntries, Entry as AoidePlaylistEntry,
        PlaylistWithEntries,
    },
    Playlist,
};
use leptos::*;
use leptos_router::{use_navigate, use_params_map};
use samba_frontend_uicomponents::{Heading, Placeholder, Space};
use samba_shared::playlists::PRACTICE_SESSION_KIND;

use super::super::{
    backend::{create_practice_session, get_session_by_id, update_practice_session},
    navigation::PracticeAction,
    SessionItem,
};
use crate::{
    views::{NotFoundView, PleaseWaitView},
    AppRoutes,
};

mod sessionitem;
use sessionitem::SessionItemView;
mod updatemodal;
use updatemodal::SessionItemUpdateModal;

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum EditPracticeMode {
    /// Create a new practice session (true for ad-hoc session, false for
    /// regular, listed session).
    Create(bool),
    /// Edit a practice session by id. The boolean indicates whether a new
    /// ID should be generated (i.e. we are not actually editing but rather
    /// creating a new playlist off an existing one).
    Edit(bool),
}

#[component]
pub fn EditPracticeView(mode: EditPracticeMode) -> impl IntoView {
    // TODO: non-local using aoide's serial types?
    let arguments = match mode {
        EditPracticeMode::Edit(generate_new_id) => {
            let id = Memo::new(move |_| use_params_map().with(|map| map.get("id").cloned()));
            create_local_resource(
                || (),
                move |_| async move {
                    let playlist = get_session_by_id(&id()?).await.ok()?;

                    let entries = RwSignal::new(
                        playlist
                            .body
                            .entries
                            .iter()
                            .map(|entry| SessionItem::new(entry.item.clone().into()))
                            .collect(),
                    );
                    let (entity, _) = create_signal(Some(playlist.clone()));

                    Some((
                        playlist.body.playlist.title.starts_with("Ad-hoc"),
                        entries,
                        playlist.body.playlist.clone(),
                        entity,
                        !generate_new_id,
                    ))
                },
            )
        },
        EditPracticeMode::Create(is_adhoc) => {
            let entries = RwSignal::new(vec![]);
            let (entity, _) = create_signal(None);

            create_local_resource(
                || (),
                move |_| async move {
                    Some((
                        is_adhoc,
                        entries,
                        Playlist {
                            title: is_adhoc
                                .then(|| "Ad-hoc practice session".to_string())
                                .unwrap_or_default(),
                            kind: Some(PRACTICE_SESSION_KIND.into()),
                            notes: None,
                            color: None,
                            flags: Default::default(),
                        },
                        entity,
                        false,
                    ))
                },
            )
        },
    };

    view! {
        <Suspense fallback=|| {
            view! { <PleaseWaitView /> }
        }>
            {move || {
                arguments()
                    .map(|args| {
                        if let Some((is_adhoc, session_items, playlist, entity, is_update)) = args {
                            view! {
                                <EditPracticeHandlerView
                                    is_adhoc=is_adhoc
                                    session_items=session_items
                                    playlist=playlist
                                    entity=entity
                                    is_update=is_update
                                />
                            }
                                .into_view()
                        } else {
                            view! {
                                <Heading>"Edit practice session"</Heading>
                                <NotFoundView />
                            }
                                .into_view()
                        }
                    })
            }}

        </Suspense>
    }
}

#[component]
fn EditPracticeHandlerView(
    is_adhoc: bool,
    session_items: RwSignal<Vec<SessionItem>>,
    playlist: Playlist,
    entity: ReadSignal<Option<PlaylistEntityWithEntries>>,
    is_update: bool,
) -> impl IntoView {
    let (session_title, set_session_title) = create_signal(playlist.title.clone());

    let show_add_modal = RwSignal::new(false);
    let add_item = move |_| {
        if show_add_modal() {
            show_add_modal.set(false);
        }
        show_add_modal.set(true);
    };

    let submit_action = move |_| {
        let entries: Vec<AoidePlaylistEntry> = session_items()
            .iter()
            .map(|item: &SessionItem| item.item.clone().into())
            .collect();

        let mut playlist = playlist.clone();
        playlist.title = session_title();

        let playlist = PlaylistWithEntries { playlist, entries };

        spawn_local(async move {
            // TODO: error handling
            let uid = if !is_update || entity().is_none() {
                create_practice_session(playlist)
                    .await
                    .unwrap()
                    .hdr
                    .uid
                    .to_string()
            } else {
                let mut entity = entity().clone().expect("We checked it is some above");
                entity.body = playlist;

                let uid = entity.hdr.uid.to_string();
                update_practice_session(&uid, entity).await.unwrap();
                uid
            };

            use_navigate()(
                &AppRoutes::Practice(if is_adhoc {
                    PracticeAction::Saved(uid, None)
                } else {
                    PracticeAction::List
                })
                .to_string(),
                Default::default(),
            );
        });
    };

    view! {
        <Heading>
            {if is_update { "Edit practice session" } else { "Create new practice session" }}
        </Heading>
        <form action="javascript:void(0);" method="post" on:submit=submit_action>
            <div class="flex gap-2 items-center">
                <Show when=move || !is_adhoc fallback=|| view! { <div class="grow"></div> }>
                    <label class="m-0" for="session-title">
                        "Title:"
                    </label>
                    <div class="form-control grow tooltip" data-tip="required">
                        <label class="relative block">
                            <span class="fa-solid fa-asterisk pointer-events-none py-2 w-8 h-8 absolute top-1/2 transform -translate-y-1/2 left-1"></span>
                            <input
                                class="input input-bordered py-3 px-4 w-full block pl-10"
                                id="session-title"
                                placeholder="My personal practice"
                                required=true
                                type="text"
                                on:input=move |ev| {
                                    set_session_title(event_target_value(&ev));
                                }
                                prop:value=session_title
                            />
                        </label>
                    </div>
                </Show>
                <div class="ml-2">
                    <button type="button" class="btn btn-primary" on:click=add_item>
                        "Add item"
                    </button>
                </div>
            </div>
            <Space />
            <Show
                when=move || !session_items().is_empty()
                fallback=|| {
                    view! {
                        <Placeholder
                            icon=view! {
                                <span
                                    class="fa-solid fa-stop-circle"
                                    style="font-size: 500%;"
                                ></span>
                            }
                                .into_view()
                            title="There are no items yet".into_view()
                            subtitle="Up to now you did not add any item.".into_view()
                            commands=vec![
                                view! {
                                    <div class="btn-container">
                                        // TODO: on_click add_item
                                        <button type="button" class="btn btn-secondary">
                                            "Add a first item to session"
                                        </button>
                                    </div>
                                }
                                    .into_view(),
                            ]
                        />
                    }
                }
            >
                <For
                    each=session_items
                    key=|x| x.uuid
                    children=move |item: SessionItem| {
                        view! { <SessionItemView item=item session_items=session_items /> }
                    }
                />
                <Space />
                <div class="flex justify-end">
                    <button type="submit" class="btn btn-secondary">
                        {if is_adhoc { "Play" } else { "Save" }}
                    </button>
                </div>
            </Show>
        </form>
        <SessionItemUpdateModal modal_visibility=show_add_modal session_items=session_items />
    }
}
