use leptos::*;
use samba_frontend_uicomponents::LRTileCard;
use samba_shared::playlists::{AudioPlayback, AudioSource, PlaylistItem};
use uuid::Uuid;

use super::{super::super::SessionItem, SessionItemUpdateModal};

#[component]
pub fn SessionItemView(
    item: SessionItem,
    session_items: RwSignal<Vec<SessionItem>>,
) -> impl IntoView {
    let show_update_modal = RwSignal::new(false);
    let item_display = match &item.item {
        PlaylistItem::Break => "Break (continue manually)".into(),
        PlaylistItem::Stop => "Stop".into(),
        PlaylistItem::Visual(label) => format!(
            "“{}”",
            label.as_ref().map_or_else(|| "–".into(), Clone::clone)
        ),
        PlaylistItem::Pause(duration) => {
            format!("Pause ({} seconds)", duration.as_secs())
        },
        PlaylistItem::Playback(AudioPlayback {
            audio_source: AudioSource::TrackUid(uid),
            ..
        }) => {
            // TODO: fetch track for ID and get title?
            format!("Track (id: {uid})")
        },
        PlaylistItem::Playback(AudioPlayback {
            audio_source: AudioSource::ResolvedTrack(entity),
            ..
        }) => {
            // TODO: fetch track for ID and get title?
            entity.body.track.track_title().map_or_else(
                || format!("Track (id: {})", entity.hdr.uid),
                ToString::to_string,
            )
        },
        PlaylistItem::Playback(AudioPlayback {
            audio_source: AudioSource::TrackQuery(query),
            ..
        }) => {
            format!("Track (query: {query})")
        },
        PlaylistItem::Playback(AudioPlayback {
            audio_source: AudioSource::TextToSpeech(text),
            ..
        }) => {
            format!("Spoken text: “{text}”")
        },
    };

    let remove_action = move |uuid: Uuid| {
        move |_| {
            let mut items = session_items().clone();
            items.retain(|element: &SessionItem| element.uuid != uuid);
            session_items.set(items);
        }
    };

    let first_item_uuid = move || {
        session_items().first().map_or_else(
            || {
                // if there is no first item, we can create a new UUID; because of
                // their randomness it will be not equal to any other UUID in the
                // vector (in this case none)
                Uuid::new_v4()
            },
            |item| item.uuid,
        )
    };
    let move_up_action = move |uuid: Uuid| {
        move |_| {
            if let Some(pos) = session_items
                .get()
                .iter()
                .position(|element: &SessionItem| element.uuid == uuid)
            {
                let mut items = session_items().clone();
                items.swap(pos, pos.saturating_sub(1));
                session_items.set(items);
            }
        }
    };

    let last_item_uuid = move || {
        session_items().last().map_or_else(
            || {
                // if there is no last item, we can create a new UUID; because of
                // their randomness it will be not equal to any other UUID in the
                // vector (in this case none)
                Uuid::new_v4()
            },
            |item| item.uuid,
        )
    };
    let move_down_action = move |uuid: Uuid| {
        move |_| {
            if let Some(pos) = session_items
                .get()
                .iter()
                .position(|element: &SessionItem| element.uuid == uuid)
            {
                let mut items = session_items().clone();
                items.swap(
                    pos,
                    (pos + 1).min(session_items.get().len().saturating_sub(1)),
                );
                session_items.set(items);
            }
        }
    };

    view! {
        <LRTileCard
            left=view! { <span class="font-bold pl-2">{item_display}</span> }.into_view()
            right=view! {
                <div class="join">
                    <button
                        class="btn join-item fa-solid fa-caret-up"
                        disabled=move || first_item_uuid() == item.uuid
                        on:click=move_up_action(item.uuid)
                        type="button"
                    ></button>
                    <button
                        class="btn join-item fa-solid fa-caret-down"
                        disabled=move || last_item_uuid() == item.uuid
                        on:click=move_down_action(item.uuid)
                        type="button"
                    ></button>
                    <button
                        class="btn join-item fa-solid fa-pen"
                        on:click=move |_| {
                            show_update_modal.set(true);
                        }
                        type="button"
                    ></button>
                    <button
                        class="btn join-item fa-solid fa-close"
                        disabled=move || session_items().len() == 1
                        on:click=remove_action(item.uuid)
                        type="button"
                    ></button>
                </div>
            }
                .into_view()
        />
        <SessionItemUpdateModal
            modal_visibility=show_update_modal
            session_items=session_items
            current_session_item=item
        />
    }
}
