use aoide::{
    playlist::{
        EntityWithEntries as PlaylistEntityWithEntries, Entry as AoidePlaylistEntry,
        PlaylistWithEntries as AoidePlaylistWithEntries,
    },
    Playlist as AoidePlaylist,
};
use leptos::*;
use leptos_router::{use_params_map, use_query};
use samba_frontend_uicomponents::{Heading, Placeholder};
use samba_shared::playlists::{AudioPlayback, AudioSource, PlaylistItem, PRACTICE_HISTORY_KIND};

use super::super::{
    backend::{create_practice_session, get_session_by_id},
    PracticeAction,
};
use crate::{util::refresh_page, views::PleaseWaitView, AppRoutes};

mod adjustment_modal;
use adjustment_modal::{PlaybackAdjustmentModal, PlaybackAdjustmentQuery, PlaybackAdjustments};
mod breakview;
use breakview::BreakView;
mod notes;
use notes::{NotesView, PRACTICE_SESSION_LINK_NOTE};
mod pause;
use pause::PauseView;
mod stop;
use stop::StopView;
mod track;
use track::{TrackContext, TrackView};
mod tts;
use self::tts::TtsView;

/// The state of the element.
#[derive(Copy, Clone, Debug, Default, PartialEq, Eq)]
pub enum PlayState {
    /// `Queued` is the initial state where the element is not supposed to do
    /// anything in the background.
    #[default]
    Queued,
    /// This is triggered when the element should perform its main action. It
    /// should be able to start without further delay.
    Playing,
    /// In this state, elements should be disabled and avoid doing any work.
    Finished,
}

#[component]
pub fn PracticePlayView() -> impl IntoView {
    let saved_id = Memo::new(move |_| use_params_map().with(|map| map.get("id").cloned()));
    // TODO: non-local with aoide's serial types?
    let session = create_local_resource(saved_id, move |_| async move {
        get_session_by_id(&saved_id()?).await.ok()
    });

    view! {
        <section>
            <Transition fallback=|| {
                view! {
                    <Heading>"Your practice session"</Heading>
                    <PleaseWaitView />
                }
            }>
                {move || {
                    session()
                        .map(|session| {
                            if let Some(playlist) = session {
                                view! { <PracticePlayHandler session=playlist /> }.into_view()
                            } else {
                                view! {
                                    <Heading>"Your practice session"</Heading>
                                    <Placeholder
                                        icon=view! {
                                            <span
                                                class="fa-solid fa-stop-circle"
                                                style="font-size: 500%;"
                                            ></span>
                                        }
                                            .into_view()
                                        title="Could not find practice session".into_view()
                                        subtitle="Maybe it has been deleted or you don't have access."
                                            .into_view()
                                        commands=vec![
                                            view! {
                                                <div class="btn-container">
                                                    <a
                                                        class="btn btn-primary u-inline-block"
                                                        href=AppRoutes::Practice(PracticeAction::List)
                                                    >
                                                        "Go to practice session overview"
                                                    </a>
                                                </div>
                                            }
                                                .into_view(),
                                        ]
                                    />
                                }
                                    .into_view()
                            }
                        })
                }}
            </Transition>
        </section>
    }
}

#[component]
fn PracticePlayHandler(session: PlaylistEntityWithEntries) -> impl IntoView {
    let playback_adjustment_query = use_query::<PlaybackAdjustmentQuery>();
    let playback_adjustments = RwSignal::<PlaybackAdjustments>::new(
        playback_adjustment_query()
            .expect(
                "Always extractible because of optional components (only fails on passed params \
                 of wrong type).",
            )
            .into(),
    );

    let session = Memo::new(move |_| {
        let mut session = session.clone();
        let mut entries = vec![];
        for entry in &session.body.entries {
            let session_item: PlaylistItem = entry.clone().into();
            if let PlaylistItem::Playback(mut props) = session_item {
                props.playback_properties.start_at = playback_adjustments()
                    .track_offset_override
                    .unwrap_or(props.playback_properties.start_at);
                props.playback_properties.duration = playback_adjustments()
                    .track_duration_override
                    .unwrap_or(props.playback_properties.duration);
                props.playback_properties.fade_in = playback_adjustments()
                    .fade_in_override
                    .unwrap_or(props.playback_properties.fade_in);
                props.playback_properties.fade_out = playback_adjustments()
                    .fade_out_override
                    .unwrap_or(props.playback_properties.fade_out);

                for _ in 0..playback_adjustments().heats {
                    entries.push(PlaylistItem::Playback(props.clone()).into());
                }
            } else if let PlaylistItem::Pause(duration) = session_item {
                entries.push(
                    PlaylistItem::Pause(
                        playback_adjustments()
                            .pause_duration_override
                            .unwrap_or(duration),
                    )
                    .into(),
                );
            } else if !(matches!(session_item, PlaylistItem::Break)
                && playback_adjustments().skip_breaks)
            {
                entries.push(entry.clone());
            }
        }
        session.body.entries = entries;
        session
    });
    let session_id = move || session().hdr.uid.to_string();
    let playlist_title = move || session().body.playlist.title.clone();
    let playlist_notes_view = move || {
        session()
            .body
            .playlist
            .notes
            .clone()
            .map(|notes| view! { <NotesView notes=notes /> })
    };

    let resolved_entries = Memo::new(move |_| {
        session()
            .body
            .entries
            .iter()
            .map(|entry| RwSignal::new(entry.clone()))
            .collect::<Vec<_>>()
    });
    let active_signals = Memo::new(move |_| {
        (0..=session().body.entries.len())
            .map(|_| RwSignal::new(PlayState::Queued))
            .collect::<Vec<_>>()
    });
    let playlist_not_started = move || active_signals()[0]() == PlayState::Queued;
    let playlist_finished =
        move || active_signals()[active_signals().len() - 1]() == PlayState::Finished;

    let start_stop_toggle = move |_| {
        if playlist_not_started() {
            // Starting means that the sentinel element finished. Through
            // the effects system it will trigger the next state.
            active_signals()[0].set(PlayState::Finished);

            // We assume that the playlist has finished loading when the
            // user starts it, so this is the point where we save it into
            // the history of played sessions.
            if !resolved_entries().is_empty() {
                let playlist = AoidePlaylistWithEntries {
                    playlist: AoidePlaylist {
                        title: format!(
                            "Playback of “{}”{}",
                            playlist_title(),
                            if playback_adjustments().heats > 1 {
                                format!(" ({} heats)", playback_adjustments().heats)
                            } else {
                                "".into()
                            }
                        ),
                        kind: Some(PRACTICE_HISTORY_KIND.into()),
                        notes: Some(format!("{PRACTICE_SESSION_LINK_NOTE}:{}", session_id())),
                        color: None,
                        flags: Default::default(),
                    },
                    entries: resolved_entries()
                        .iter()
                        .map(|signal| signal().clone())
                        .collect(),
                };

                spawn_local(async move {
                    // TODO: error handling
                    create_practice_session(playlist).await.unwrap();
                });
            }
        } else if playlist_finished() {
            // Reset the playlist to a fresh state by reloading the page.
            refresh_page();
        } else {
            // Stop the playback.
            for signal in &active_signals() {
                signal.set(PlayState::Finished);
            }
        }
    };

    let adjustment_modal_visibility = RwSignal::new(false);
    let show_adjustment_modal = move |_| {
        if adjustment_modal_visibility() {
            adjustment_modal_visibility.set(false);
        }
        adjustment_modal_visibility.set(true);
    };

    view! {
        <PlaybackAdjustmentModal
            visibility=adjustment_modal_visibility
            adjustments=playback_adjustments
        />
        <div class="flex gap-2 items-center">
            <Heading classes="pt-0 pb-0 grow".into()>{playlist_title}</Heading>
            <div class="flex gap-4">
                <button
                    class="btn"
                    disabled=move || !playlist_not_started()
                    on:click=show_adjustment_modal
                    type="button"
                >
                    "Adjust session"
                </button>
                <button
                    class="btn"
                    class:btn-primary=move || playlist_not_started() || playlist_finished()
                    class:btn-secondary=move || !playlist_not_started() && !playlist_finished()
                    on:click=start_stop_toggle
                    type="button"
                >
                    {move || {
                        if playlist_not_started() {
                            "Start"
                        } else if playlist_finished() {
                            "Reset"
                        } else {
                            "Stop"
                        }
                    }}
                </button>
            </div>
        </div>
        <div class="mx-16">{playlist_notes_view}</div>
        <EntriesView
            session=session.into()
            active_signals=active_signals.into()
            resolved_entries=resolved_entries.into()
        />
    }
}

#[component]
fn EntriesView(
    session: Signal<PlaylistEntityWithEntries>,
    active_signals: Signal<Vec<RwSignal<PlayState>>>,
    resolved_entries: Signal<Vec<RwSignal<AoidePlaylistEntry>>>,
) -> impl IntoView {
    view! {
        {move || {
            session()
                .body
                .entries
                .iter()
                .enumerate()
                .map(|(i, entry)| {
                    let previous_signal = active_signals()[i];
                    let active_signal = active_signals()[i + 1];
                    let session_item: PlaylistItem = entry.clone().into();
                    Effect::new(move |_| {
                        if previous_signal() == PlayState::Finished {
                            active_signal.set(PlayState::Playing);
                        }
                    });
                    match session_item {
                        PlaylistItem::Break => {
                            view! { <BreakView active=active_signal /> }.into_view()
                        }
                        PlaylistItem::Stop => {
                            Effect::new(move |_| {
                                if active_signal() == PlayState::Playing {
                                    for signal in &active_signals() {
                                        signal.set(PlayState::Finished);
                                    }
                                }
                            });
                            view! { <StopView/> }.into_view()
                        },
                        PlaylistItem::Visual(text) => view! {
                            <div class="divider my-2">{text}</div>
                        }
                        .into_view(),
                        PlaylistItem::Pause(duration) => {
                            let duration_signal = RwSignal::new(duration);
                            view! { <PauseView active=active_signal duration=duration_signal/> }
                                .into_view()
                        },
                        PlaylistItem::Playback(AudioPlayback {
                            audio_source: AudioSource::TextToSpeech(text),
                            ..
                        }) => view! { <TtsView active=active_signal text=text/> }.into_view(),
                        PlaylistItem::Playback(playback) => {
                            let query = match playback.audio_source {
                                // TODO: replace by fetching by id and dance card once factored out
                                AudioSource::TrackUid(uid) => format!("uid:{uid}"),
                                AudioSource::ResolvedTrack(entity) => {
                                    format!("uid:{}", entity.hdr.uid)
                                },
                                AudioSource::TrackQuery(query) => query,
                                AudioSource::TextToSpeech(_) => {
                                    unreachable!("pattern handled by outer match")
                                },
                            };

                            view! {
                                <Provider value=TrackContext {
                                    query,
                                    properties: playback.playback_properties.clone(),
                                }>
                                    <TrackView
                                        active=active_signal
                                        history_entry=resolved_entries()[i]
                                    />
                                </Provider>
                            }
                            .into_view()
                        },
                    }
                })
                .collect::<Vec<_>>()
            }
        }
    }
}
