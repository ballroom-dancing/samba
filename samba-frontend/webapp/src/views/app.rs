use codee::string::JsonSerdeWasmCodec as JsonCodec;
use leptos::*;
use leptos_meta::*;
use leptos_router::*;
use leptos_use::storage::use_local_storage;
use samba_frontend_shared::{
    profile::{UserProfile, LOCAL_STORAGE_PROFILE_KEY},
    AppState,
};
use samba_frontend_uicomponents::{Space, Toast, ToastType};

use crate::{
    events::{EventDetailsView, EventListView, EventUpdateView},
    library::{LibraryByDanceView, TrackListView},
    practice::{
        EditPracticeMode, EditPracticeView, PracticeHistoryView, PracticeListView, PracticePlayView,
    },
    routines::{RoutineDetailsView, RoutineListView, RoutineUpdateView},
    settings::{ManageLibrariesView, ProfileUpdateView, SettingsView},
    views::{
        AboutView, DashboardView, FooterView, HeaderView, LoginView, MessageToasts, NotFoundView,
    },
};

#[component]
pub fn App() -> impl IntoView {
    provide_meta_context();

    let (profile, _, _) = use_local_storage::<UserProfile, JsonCodec>(LOCAL_STORAGE_PROFILE_KEY);
    let logged_in = move || !profile().is_guest();
    let running_standalone =
        move || use_context::<AppState>().map_or(true, |state| !state.running_in_tauri);

    view! {
        <Stylesheet href="/css/fonts.css" />
        <Stylesheet href="/css/samba.css" />
        <Stylesheet href="/css/tailwind.css" />
        <Stylesheet href="/font-awesome/css/all.min.css" />
        <Link rel="shortcut icon" type_="image/ico" href="/favicon.ico" />
        <Router trailing_slash=TrailingSlash::Redirect>
            <Show when=move || running_standalone() && logged_in()>
                <HeaderView />
            </Show>
            <Routes>
                <Route
                    path="/"
                    view=move || {
                        view! {
                            // only show the outlet if data have loaded
                            <Show
                                when=logged_in
                                fallback=|| {
                                    view! {
                                        <MessageToasts />
                                        <LoginView />
                                    }
                                }
                            >
                                <div class="px-10 md:px-0 md:w-[75%] lg:w-[50%] mt-8 mb-10 mx-auto">
                                    <MessageToasts />

                                    // TODO: show on experimental routes (events/routines)
                                    {false
                                        .then(|| {
                                            view! {
                                                <Space />
                                                <Toast
                                                    kind=ToastType::Warning
                                                    message="You are using an experimental feature.".into()
                                                />
                                            }
                                        })}
                                    <Outlet />
                                </div>
                            </Show>
                        }
                    }
                >
                    // frontpage has multiple routes
                    <Route path="" view=DashboardView />
                    <Route path="/index.html" view=DashboardView />
                    <Route path="/dashboard" view=DashboardView />
                    // potentially static routes (currently not because parent
                    // not static)
                    <Route path="/about" view=AboutView />
                    // library
                    <Route path="/library" view=TrackListView />
                    <Route path="/library/list" view=TrackListView />
                    <Route path="/library/bydance" view=LibraryByDanceView />
                    // practice
                    <Route path="/practice" view=PracticeListView />
                    <Route path="/practice/list" view=PracticeListView />
                    <Route path="/practice/history" view=PracticeHistoryView />
                    <Route
                        path="/practice/adhoc"
                        view=|| view! { <EditPracticeView mode=EditPracticeMode::Create(true) /> }
                    />
                    <Route
                        path="/practice/create"
                        view=|| view! { <EditPracticeView mode=EditPracticeMode::Create(false) /> }
                    />
                    <Route
                        path="/practice/update/:id"
                        view=|| view! { <EditPracticeView mode=EditPracticeMode::Edit(false) /> }
                    />
                    <Route
                        path="/practice/create-off/:id"
                        view=|| view! { <EditPracticeView mode=EditPracticeMode::Edit(true) /> }
                    />
                    <Route path="/practice/saved/:id" view=PracticePlayView />
                    // settings
                    <Route path="/settings" view=SettingsView />
                    <Route path="/settings/dashboard" view=SettingsView />
                    <Route path="/settings/library" view=ManageLibrariesView />
                    <Route path="/settings/profile" view=ProfileUpdateView />
                    <Route path="/settings/users" view=NotFoundView />
                    // experimental: events
                    <Route path="/events" view=EventListView />
                    <Route path="/events/list" view=EventListView />
                    <Route path="/events/create" view=EventUpdateView />
                    <Route path="/events/edit/:id" view=EventUpdateView />
                    <Route path="/events/view/:id" view=EventDetailsView />
                    // experimental: routines
                    <Route path="/routines" view=RoutineListView />
                    <Route path="/routines/list" view=RoutineListView />
                    <Route path="/routines/create" view=RoutineUpdateView />
                    <Route path="/routines/edit/:id" view=RoutineUpdateView />
                    <Route path="/routines/view/:id" view=RoutineDetailsView />
                    // fallback
                    <Route path="/*any" view=NotFoundView />
                </Route>
            </Routes>
            <Show when=running_standalone>
                <FooterView />
            </Show>
        </Router>
    }
}
