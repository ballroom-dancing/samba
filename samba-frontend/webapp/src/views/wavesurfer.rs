use std::time::Duration;

use aoide::{media::content::ContentMetadata, TrackEntity};
use leptos::*;
use samba_shared::playlists::PlaybackProperties;
use serde::Serialize;
use wasm_bindgen::prelude::*;

#[component]
pub fn WaveSurferContainer(
    wavesurfer: RwSignal<Option<WaveSurfer>>,
    entity: TrackEntity,
    properties: PlaybackProperties,
) -> impl IntoView {
    let track_uid = entity.hdr.uid.to_string();

    let ContentMetadata::Audio(ref audio_content) = entity.body.track.media_source.content.metadata;
    // TODO: sane fallback value for duration
    let track_duration = audio_content
        .duration
        .map_or(120.0, |duration| duration.value() * 1000.0);
    let track_end_time = if properties.duration == Duration::ZERO {
        track_duration
    } else {
        (properties.start_at + properties.duration)
            .as_secs_f64()
            .min(track_duration)
    };
    let mut regions = vec![];
    if properties.start_at != Duration::ZERO || properties.duration != Duration::ZERO {
        regions.push(WaveSurferRegionOptions::new_with_id(
            format!("wavesurfer-{track_uid}"),
            properties.start_at.as_secs_f64(),
            track_end_time,
        ));
    }
    if properties.fade_in != Duration::ZERO {
        regions.push(WaveSurferRegionOptions::new_with_id_and_color(
            format!("wavesurfer-fadein-{track_uid}"),
            properties.start_at.as_secs_f64(),
            (properties.start_at + properties.fade_in).as_secs_f64(),
            "rgba(0, 0, 0, 0.2)".into(),
        ));
    }
    if properties.fade_out != Duration::ZERO {
        regions.push(WaveSurferRegionOptions::new_with_id_and_color(
            format!("wavesurfer-fadeout-{track_uid}"),
            track_end_time - properties.fade_out.as_secs_f64(),
            track_end_time,
            "rgba(0, 0, 0, 0.1)".into(),
        ));
    }

    view! { <div id=format!("wavesurfer-{track_uid}")></div> }.on_mount(move |_| {
        wavesurfer.set(Some(WaveSurfer::create(
            WaveSurferOptions::for_container(format!("#wavesurfer-{track_uid}"))
                .to_js_value_with_regions(&regions),
        )));
    })
}

#[derive(Serialize)]
#[rustfmt::skip]
#[serde(rename_all = "camelCase")]
pub struct WaveSurferOptions {
    /// Speed at which to play audio. Lower number is slower.
    /// Default: 1.0
    audio_rate: f64,
    // TODO: audioContext    object    none    Use your own previously initialized AudioContext or leave blank.
    // TODO: audioScriptProcessor    object    none    Use your own previously initialized ScriptProcessorNode or leave blank.
    /// If a scrollbar is present, center the waveform around the progress.
    /// Default: true
    auto_center: bool,
    /// WebAudio, MediaElement or MediaElementWebAudio. MediaElement is a
    /// fallback for unsupported browsers.
    /// Default: WebAudio
    backend: String,
    /// Change background color of the waveform container.
    /// Default: None
    background_color: Option<String>,
    /// The optional spacing between bars of the wave, if not provided will be
    /// calculated in legacy format.
    /// Default: None
    bar_gap: Option<f64>,
    /// Height of the waveform bars. Higher number than 1 will increase the
    /// waveform bar heights.
    /// Default: 1.0
    bar_height: f64,
    /// Minimum height to draw a waveform bar.
    /// Default: None
    bar_min_height: Option<f64>,
    /// Default behavior is to not draw a bar during silence.
    /// The radius that makes bars rounded.
    /// Default: 0.0
    bar_radius: f64,
    /// If specified, the waveform will be drawn like this: ▁ ▂ ▇ ▃ ▅ ▂
    /// Default: None
    bar_width: Option<f64>,
    /// Close and nullify all audio contexts when the destroy method is called.
    /// Default: false
    close_audio_context: bool,
    /// CSS-selector or HTML-element where the waveform should be drawn. This
    /// is the only required parameter. (originally mixed type, default none)
    container: String,
    /// The fill color of the cursor indicating the playhead position.
    /// Default: #333
    cursor_color: String,
    /// Measured in pixels.
    /// Default: 1
    cursor_width: i32,
    // drawingContextAttributes    object    {desynchronized: true}    Specify canvas 2d drawing context attributes.
    /// Whether to fill the entire container or draw only according to
    /// minPxPerSec.
    /// Default: true
    fill_parent: bool,
    /// Force decoding of audio using web audio when zooming to get a more
    /// detailed waveform.
    /// Default: false
    force_decode: bool,
    /// The height of the waveform. Measured in pixels.
    /// Default: 128
    height: i32,
    /// Whether to hide the horizontal scrollbar when one would normally be
    /// shown.
    /// Default: false
    hide_scrollbar: bool,
    /// Hide the mouse cursor when hovering over the waveform. By default it
    /// will be shown.
    /// Default: false
    hide_cursor: bool,
    /// Whether the mouse interaction will be enabled at initialization. You can
    /// switch this parameter at any time later on.
    /// Default: true
    interact: bool,
    /// (Use with regions plugin) Enable looping of selected regions.
    /// Default: true
    loop_selection: bool,
    /// Maximum width of a single canvas in pixels, excluding a small overlap
    /// (2 * pixelRatio, rounded up to the next even integer). If the waveform
    /// is longer than this value, additional canvases will be used to render
    /// the waveform, which is useful for very large waveforms that may be too
    /// wide for browsers to draw on a single canvas. This parameter is only
    /// applicable to the MultiCanvas renderer.
    /// Default: 4000
    max_canvas_width: i32,
    /// (Use with backend MediaElement) this enables the native controls for the
    /// media element.
    /// Default: false
    media_controls: bool,
    /// 'audio' or 'video'. Only used with backend MediaElement.
    /// Default: audio
    media_type: String,
    /// Minimum number of pixels per second of audio.
    /// Default: 50
    min_px_per_sec: i32,
    /// If true, normalize by the maximum peak instead of 1.0.
    /// Default: false
    normalize: bool,
    /// Use the PeakCache to improve rendering speed of large waveforms.
    /// Default: false
    partial_render: bool,
    // TODO: pixelRatio    integer    window.devicePixelRatio    Can be set to 1 for faster rendering.
    /// An array of plugin definitions to register during instantiation. They
    /// will be directly initialised unless they are added with the deferInit
    /// property set to true.
    /// Default: []
    // TODO: make it vector of objects
    plugins: Vec<String>,
    /// The fill color of the part of the waveform behind the cursor. When
    /// progressColor and waveColor are the same the progress wave is not
    /// rendered at all.
    /// Default: #555
    progress_color: String,
    /// Default minLength for regions, in seconds. When creating a region,
    /// specifying the minLength parameter for that region will override this.
    /// Default: None
    regions_min_length: Option<f64>,
    /// Set to false to keep the media element in the DOM when the player is
    /// destroyed. This is useful when reusing an existing media element via the
    /// loadMediaElement method.
    /// Default: true
    remove_media_element_on_destroy: bool,
    // TODO: renderer    Object    MultiCanvas    Can be used to inject a custom renderer.
    /// If set to true resize the waveform, when the window is resized. This is
    /// debounced with a 100ms timeout by default. If this parameter is a
    /// number it represents that timeout.
    /// Originally boolean or float. Default: false
    responsive: bool,
    /// Whether to scroll the container with a lengthy waveform. Otherwise the
    /// waveform is shrunk to the container width (see fillParent).
    /// Default: false
    scroll_parent: bool,
    /// Number of seconds to skip with the skipForward() and skipBackward()
    /// methods.
    /// Default: 2
    skip_length: f64,
    /// Render with seperate waveforms for the channels of the audio.
    /// Default: false
    split_channels: bool,
    // TODO: splitChannelsOptions    object    {}    Split channel options. Only applied when splitChannels=true. See below for options
    // TODO: splitChannelsOptions.overlay    boolean    false    Overlay waveforms for channels instead of rendering them on separate rows
    // TODO: splitChannelsOptions.relativeNormalization    boolean    false    Normalization maintains proportionality between channels. Only applies when normalize=true
    // TODO: splitChannelsOptions.filterChannels    array    []    List of channel numbers to be excluded from rendered waveforms. Channels are 0-indexed
    // TODO: splitChannelsOptions.channelColors    object    {}    Overrides color per channel. Each key indicates a channel number and the corresponding value is an object describing it's color porperties. For example: channelColors={ 0: { progressColor: 'green', waveColor: 'pink' }, 1: { progressColor: 'orange', waveColor: 'purple' } }
    /// The fill color of the waveform after the cursor.
    /// Default: #999
    wave_color: String,
    // TODO: xhr    Object    {}    XHR options. For example: let xhr = { cache: 'default', mode: 'cors', method: 'GET', credentials: 'same-origin', redirect: 'follow', referrer: 'client', headers: [ { key: 'Authorization', value: 'my-token' } ]};
}

impl WaveSurferOptions {
    /// Create a default options object with the selector as required
    /// parameter for the container. The defaults are optimized for a
    /// uniform samba styling.
    pub fn for_container(selector: String) -> Self {
        Self {
            audio_rate: 1.0,
            auto_center: true,
            backend: "WebAudio".to_string(),
            background_color: None,
            bar_gap: Some(3.0),
            bar_height: 1.0,
            bar_min_height: None,
            bar_radius: 3.0,
            bar_width: Some(3.0),
            close_audio_context: false,
            container: selector,
            cursor_color: "#333".to_string(),
            cursor_width: 3,
            fill_parent: true,
            force_decode: false,
            height: 96,
            hide_scrollbar: false,
            hide_cursor: false,
            interact: true,
            loop_selection: true,
            max_canvas_width: 4000,
            media_controls: false,
            media_type: "audio".to_string(),
            min_px_per_sec: 50,
            normalize: false,
            partial_render: true,
            plugins: vec![],
            progress_color: "#555".to_string(),
            regions_min_length: None,
            remove_media_element_on_destroy: true,
            responsive: true,
            scroll_parent: false,
            skip_length: 2.0,
            split_channels: false,
            wave_color: "#999".to_string(),
        }
    }

    /// Create a [`JsValue`] from this data object and instantiate the regions
    /// plugin in the course of this action with pre-initialized regions.
    pub fn to_js_value_with_regions(&self, regions: &[WaveSurferRegionOptions]) -> JsValue {
        #[rustfmt::skip]
        let regions_plugin = js_sys::eval(&format!(
            "[WaveSurfer.regions.create({{regions:{}}}),\
              WaveSurfer.cursor.create({{showTime: true, opacity: 1, customShowTimeStyle: {{\
                'background-color': '#000',\
                color: '#fff',\
                padding: '5px',\
                'font-size': '10pt',\
              }}}})]",
            serde_json::to_string(regions).expect("Region options can always be serialized.")
        ))
        .expect("Cannot fail to instantiate regions plugin if it is loaded.");
        let object = serde_wasm_bindgen::to_value(self)
            .expect("Cannot fail to transform this object to JSON");

        js_sys::Reflect::set(&object, &JsValue::from_str("plugins"), &regions_plugin)
            .expect("Always setting a valid value in a valid object.");

        object
    }
}

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct WaveSurferRegionOptions {
    /// The id of the region.
    /// Default: random
    id: String,
    /// The start position of the region (in seconds).
    /// Default: 0
    start: f64,
    /// The end position of the region (in seconds).
    /// Default: 0
    end: f64,
    /// Whether to loop the region when played back.
    /// Default: false
    #[serde(rename = "loop")]
    r#loop: bool,
    /// Allow/dissallow dragging the region.
    /// Default: true
    drag: bool,
    /// Allow/dissallow resizing the region.
    /// Default: true
    resize: bool,
    /// HTML color code.
    /// Default: rgba(0, 0, 0, 0.1)
    color: String,
    /// Optional minimum length for the region (in seconds).
    /// Default: None
    min_length: Option<f64>,
    /// Optional maximum length for the region (in seconds).
    /// Default: None
    max_length: Option<f64>,
}

impl WaveSurferRegionOptions {
    /// Create new region options with samba's defaults for a given region ID.
    pub fn new_with_id(id: String, start: f64, end: f64) -> Self {
        Self {
            id,
            start,
            end,
            r#loop: false,
            drag: false,
            resize: false,
            color: "rgba(0, 0, 0, 0.1)".into(),
            min_length: None,
            max_length: None,
        }
    }

    /// Create new region options with samba's defaults for a given region ID.
    pub const fn new_with_id_and_color(id: String, start: f64, end: f64, color: String) -> Self {
        Self {
            id,
            start,
            end,
            r#loop: false,
            drag: false,
            resize: false,
            color,
            min_length: None,
            max_length: None,
        }
    }
}

#[wasm_bindgen]
unsafe extern "C" {
    pub fn name() -> String;

    pub type WaveSurfer;

    #[wasm_bindgen(static_method_of = WaveSurfer)]
    pub fn create(options: JsValue) -> WaveSurfer;

    /// Cancel the audio file loading process.
    #[wasm_bindgen(method, js_name = "cancelAjax")]
    pub fn cancel_ajax(this: &WaveSurfer);

    /// Removes events, elements and disconnects Web Audio nodes.
    #[wasm_bindgen(method)]
    pub fn destroy(this: &WaveSurfer);

    /// Clears the waveform as if a zero-length audio is loaded.
    #[wasm_bindgen(method)]
    pub fn empty(this: &WaveSurfer);

    /// Returns a map of plugin names that are currently initialised.
    #[wasm_bindgen(method, js_name = "getActivePlugins")]
    pub fn get_active_plugins(this: &WaveSurfer) -> JsValue;

    /// Returns the background color of the waveform container.
    #[wasm_bindgen(method, js_name = "getBackgroundColor")]
    pub fn get_background_color(this: &WaveSurfer) -> String;

    /// Returns current progress in seconds.
    #[wasm_bindgen(method, js_name = "getCurrentTime")]
    pub fn get_current_time(this: &WaveSurfer) -> f64;

    /// Returns the fill color of the cursor indicating the playhead position.
    #[wasm_bindgen(method, js_name = "getCursorColor")]
    pub fn get_cursor_color(this: &WaveSurfer) -> String;

    /// Returns the duration of an audio clip in seconds.
    #[wasm_bindgen(method, js_name = "getDuration")]
    pub fn get_duration(this: &WaveSurfer) -> f64;

    /// Returns the playback speed of an audio clip.
    #[wasm_bindgen(method, js_name = "getPlaybackRate")]
    pub fn get_playback_rate(this: &WaveSurfer) -> f64;

    // Returns the fill color of the waveform behind the cursor.
    #[wasm_bindgen(method, js_name = "getProgressColor")]
    pub fn get_progress_color(this: &WaveSurfer) -> String;

    /// Returns the volume of the current audio clip.
    #[wasm_bindgen(method, js_name = "getVolume")]
    pub fn get_volume(this: &WaveSurfer) -> f64;

    /// Returns the current mute status.
    #[wasm_bindgen(method, js_name = "getMute")]
    pub fn get_mute(this: &WaveSurfer) -> bool;

    /// Returns an array of the current set filters.
    #[wasm_bindgen(method, js_name = "getFilters")]
    pub fn get_filters(this: &WaveSurfer) -> JsValue;

    /// Returns the fill color of the waveform after the cursor.
    #[wasm_bindgen(method, js_name = "getWaveColor")]
    pub fn get_wave_color(this: &WaveSurfer) -> String;

    // exportPCM(length, accuracy, noWindow, start) – Exports PCM data into a JSON
    // array. Optional parameters length [number] - default: 1024, accuracy [number]
    // - default: 10000, noWindow [true|false] - default: false, start [number] -
    // default: 0 exportImage(format, quality, type) – Return waveform image as
    // data URI or Blob.

    /// Returns true if currently playing, false otherwise.
    #[wasm_bindgen(method, js_name = "isPlaying")]
    pub fn is_playing(this: &WaveSurfer) -> bool;

    /// Loads audio from URL via XHR. load(url, peaks, preload).
    #[wasm_bindgen(method)]
    pub fn load(this: &WaveSurfer, url: &str);

    /// Loads audio from URL via XHR. load(url, peaks, preload).
    /// Optional array of peaks.
    #[wasm_bindgen(method, js_name = "load")]
    pub fn load_with_peaks(this: &WaveSurfer, url: &str, peaks: &[f64]);

    /// Loads audio from URL via XHR. load(url, peaks, preload).
    /// Optional array of peaks. Optional preload parameter
    /// [none|metadata|auto], passed to the Audio element if using backend
    /// MediaElement.
    #[wasm_bindgen(method, js_name = "load")]
    pub fn load_with_preload(this: &WaveSurfer, url: &str, preload: &str);

    // loadBlob(url) – Loads audio from a Blob or File object.

    /// Subscribes to an event. See WaveSurfer Events for the list of all
    /// events.
    #[wasm_bindgen(method)]
    pub fn on(this: &WaveSurfer, event_name: &str, callback: &Closure<dyn FnMut()>);
    // un(eventName, callback) – Unsubscribes from an event.

    /// Unsubscribes from all events.
    #[wasm_bindgen(method, js_name = "unAll")]
    pub fn un_all(this: &WaveSurfer);

    /// Stops playback.
    #[wasm_bindgen(method)]
    pub fn pause(this: &WaveSurfer);

    /// Starts playback from the current position.
    #[wasm_bindgen(method)]
    pub fn play(this: &WaveSurfer);

    /// Starts playback from the current position. Optional start measured in
    /// seconds can be used to set the range of audio to play.
    #[wasm_bindgen(method, js_name = "play")]
    pub fn play_from(this: &WaveSurfer, from: f64);

    /// Starts playback from the current position. Optional start and end
    /// measured in seconds can be used to set the range of audio to play.
    #[wasm_bindgen(method, js_name = "play")]
    pub fn play_range(this: &WaveSurfer, from: f64, to: f64);

    /// Plays if paused, pauses if playing.
    #[wasm_bindgen(method, js_name = "playPause")]
    pub fn play_pause(this: &WaveSurfer);

    /// Seeks to a progress and centers view [0..1] (0 = beginning, 1 = end).
    #[wasm_bindgen(method, js_name = "seekAndCenter")]
    pub fn seek_and_center(this: &WaveSurfer, progress: f64);

    /// Seeks to a progress [0..1] (0 = beginning, 1 = end).
    #[wasm_bindgen(method, js_name = "seekTo")]
    pub fn seek_to(this: &WaveSurfer, progress: f64);

    /// Sets the background color of the waveform container.
    #[wasm_bindgen(method, js_name = "setBackgroundColor")]
    pub fn set_background_color(this: &WaveSurfer, color: String);

    /// Sets the fill color of the cursor indicating the playhead position.
    #[wasm_bindgen(method, js_name = "setCursorColor")]
    pub fn set_cursor_color(this: &WaveSurfer, color: String);

    /// Sets the height of the waveform.
    #[wasm_bindgen(method, js_name = "setHeight")]
    pub fn set_height(this: &WaveSurfer, height: f64);

    // setFilter(filters) - For inserting your own WebAudio nodes into the graph.
    // See Connecting Filters below.

    /// Sets the speed of playback (0.5 is half speed, 1 is normal speed, 2 is
    /// double speed and so on).
    #[wasm_bindgen(method, js_name = "setPlaybackRate")]
    pub fn set_playback_rate(this: &WaveSurfer, rate: f64);

    /// Sets set a point in seconds for playback to stop at.
    #[wasm_bindgen(method, js_name = "setPlayEnd")]
    pub fn set_play_end(this: &WaveSurfer, position: f64);

    // Sets the playback volume to a new value [0..1] (0 = silent, 1 = maximum).
    #[wasm_bindgen(method, js_name = "setVolume")]
    pub fn set_volume(this: &WaveSurfer, new_volume: f64);

    /// Mute the current sound. Can be a boolean value of true to mute sound or
    /// false to unmute.
    #[wasm_bindgen(method, js_name = "setMute")]
    pub fn set_mute(this: &WaveSurfer, mute: bool);

    /// Sets the fill color of the waveform behind the cursor.
    #[wasm_bindgen(method, js_name = "setProgressColor")]
    pub fn set_progress_color(this: &WaveSurfer, color: String);

    /// Sets the fill color of the waveform after the cursor.
    #[wasm_bindgen(method, js_name = "setWaveColor")]
    pub fn set_wave_color(this: &WaveSurfer, color: String);

    /// Skip a number of seconds from the current position (use a negative value
    /// to go backwards).
    #[wasm_bindgen(method)]
    pub fn skip(this: &WaveSurfer, offset: f64);

    /// Rewind skipLength seconds.
    #[wasm_bindgen(method, js_name = "skipBackward")]
    pub fn skip_backward(this: &WaveSurfer);

    /// Skip ahead skipLength seconds.
    #[wasm_bindgen(method, js_name = "skipForward")]
    pub fn skip_forward(this: &WaveSurfer);

    /// Set the sink id to change audio output device.
    #[wasm_bindgen(method, js_name = "setSinkId")]
    pub fn set_sink_id(this: &WaveSurfer, device_id: String);

    /// Stops and goes to the beginning.
    #[wasm_bindgen(method)]
    pub fn stop(this: &WaveSurfer);

    /// Toggles the volume on and off.
    #[wasm_bindgen(method, js_name = "toggleMute")]
    pub fn toggle_mute(this: &WaveSurfer);

    /// Toggle mouse interaction.
    #[wasm_bindgen(method, js_name = "toggleInteraction")]
    pub fn toggle_interaction(this: &WaveSurfer);

    /// Toggles scrollParent.
    #[wasm_bindgen(method, js_name = "toggleScroll")]
    pub fn toggle_scroll(this: &WaveSurfer);

    /// Horizontally zooms the waveform in and out. The parameter is a number of
    /// horizontal pixels per second of audio. It also changes the parameter
    /// minPxPerSec and enables the scrollParent option.
    #[wasm_bindgen(method)]
    pub fn zoom(this: &WaveSurfer, px_per_sec: f64);

    ///////////////////////////////////////////////////////////////////////////
    // Methods from the regions plugin
    ///////////////////////////////////////////////////////////////////////////

    pub type Region;

    /// Creates a region on the waveform. Returns a Region object. See Region
    /// Options, Region Methods and Region Events below. Note: You cannot add
    /// regions until the audio has finished loading, otherwise the start: and
    /// end: properties of the new region will be set to 0, or an unexpected
    /// value.
    #[wasm_bindgen(method, js_name = "addRegion")]
    pub fn add_region(this: &WaveSurfer, options: &JsValue) -> Region;

    /// Removes all regions.
    #[wasm_bindgen(method, js_name = "clearRegions")]
    pub fn clear_regions(this: &WaveSurfer);

    /// Lets you create regions by selecting areas of the waveform with mouse.
    /// options are Region objects' params (see below).
    #[wasm_bindgen(method, js_name = "enableDragSelection")]
    pub fn enable_drag_selection(this: &WaveSurfer, options: &JsValue);

    /// Get the list of existing regions.
    #[wasm_bindgen(method, getter, js_name = "regions.list")]
    pub fn list_regions(this: &WaveSurfer) -> Vec<Region>;

    /// Plays the region once from start to end.
    #[wasm_bindgen(method)]
    pub fn play(this: &Region);

    /// Plays the region on a loop.
    #[wasm_bindgen(method, js_name = "playLoop")]
    pub fn play_loop(this: &Region);

    /// Removes the region.
    #[wasm_bindgen(method)]
    pub fn remove(this: &Region);

    /// Adds timeInSeconds to the start and end params.
    #[wasm_bindgen(method, js_name = "onDrag")]
    pub fn on_drag(this: &Region, time_in_secs: f64);

    /// Adds timeInSeconds to end by default.
    #[wasm_bindgen(method, js_name = "onResize")]
    pub fn on_resize(this: &Region, time_in_secs: f64);

    /// Adds timeInSeconds to end by default. The optional parameter 'start'
    /// will add timeInSeconds to start.
    #[wasm_bindgen(method, js_name = "onResize")]
    pub fn on_resize_start(this: &Region, time_in_secs: f64, start: &str);
}
