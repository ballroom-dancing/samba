use leptos::*;
use samba_frontend_shared::{AppState, Message, MessageKind};
use samba_frontend_uicomponents::Space;

#[component]
pub fn MessageToasts() -> impl IntoView {
    let app_state = use_context::<AppState>()
        .expect("Message toasts require the state to extract messages from.");
    let has_messages = move || !app_state.messages.get().is_empty();

    view! {
        <Show when=has_messages>
            <Space />
            <For each=app_state.messages key=|x| x.get().id let:message>
                <MessageItem item=message />
            </For>
        </Show>
    }
}

#[component]
fn MessageItem(item: ReadSignal<Message>) -> impl IntoView {
    let app_state = use_context::<AppState>()
        .expect("Message toasts require the state to extract messages from.");
    let id = move || item.get().id;

    let handle_destroy = move |_| {
        app_state.remove_message(id());
    };
    let is_error = move || matches!(item.get().kind, MessageKind::Error);
    let is_information = move || matches!(item.get().kind, MessageKind::Information);

    view! {
        <div class="toast" class=("toast--error", is_error) class=("toast--info", is_information)>
            <button class="btn-close" on:click=handle_destroy type="button"></button>
            <h4 class="toast__title">{item.get().title}</h4>
            <Show when=is_error fallback=move || view! { <p>{item.get().message}</p> }>
                <p>
                    "Please (re)start the backend server and reload the page. \
                     In the current state you will not be able to actually use samba."
                </p>
                <p>
                    <small>"Error message as provided: " {item.get().message}</small>
                </p>
            </Show>
        </div>
    }
}
