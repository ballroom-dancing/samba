use codee::string::JsonSerdeWasmCodec as JsonCodec;
use leptos::*;
use leptos_use::storage::use_local_storage;
use samba_frontend_shared::profile::{UserProfile, LOCAL_STORAGE_PROFILE_KEY};
use samba_frontend_uicomponents::{Card, Heading};

use super::navigation::SettingsComponent;
use crate::AppRoutes;

mod library;
mod profile;

pub use library::*;
pub use profile::*;

#[component]
pub fn SettingsView() -> impl IntoView {
    let (profile, _, _) = use_local_storage::<UserProfile, JsonCodec>(LOCAL_STORAGE_PROFILE_KEY);
    let is_admin = move || profile().is_admin;

    view! {
        <section>
            <Heading>"Setup samba"</Heading>
            <div class="grid grid-flow-row grid-cols-2 gap-4">
                <Card
                    title="Libraries".into()
                    action_bar=view! {
                        <a class="btn" href=AppRoutes::Settings(SettingsComponent::Library)>
                            "Manage libraries"
                        </a>
                    }
                        .into_view()
                >
                    <p>"Manage which folders will be available to aoide and samba."</p>
                </Card>
                <Card
                    title="Profile".into()
                    action_bar=view! {
                        <a class="btn" href=AppRoutes::Settings(SettingsComponent::Profile)>
                            "Update profile"
                        </a>
                    }
                        .into_view()
                >
                    <p>"Change personal settings and preferences."</p>
                </Card>
                <Show when=is_admin>
                    <Card
                        title="Users".into()
                        action_bar=view! {
                            <a
                                class="btn w-100p u-inline-block"
                                href=AppRoutes::Settings(SettingsComponent::Users)
                            >
                                "Coming soon"
                            </a>
                        }
                            .into_view()
                    >
                        <p>"Manage who has access to the samba frontend."</p>
                    </Card>
                </Show>
            </div>
        </section>
    }
}
