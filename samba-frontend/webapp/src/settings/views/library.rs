use leptos::*;
use samba_frontend_uicomponents::{Card, Heading, Placeholder, Space};
use samba_shared::libraries::Library;

use super::super::{
    backend::{delete_library, fetch_libraries, scan_all_libraries, scan_library},
    SettingsComponent,
};
use crate::{
    library::{LibraryAction, LibraryViewParameters},
    settings::backend::{create_library, update_library},
    util::refresh_page,
    views::PleaseWaitView,
    AppRoutes,
};

#[component]
pub fn ManageLibrariesView() -> impl IntoView {
    let (rescanning_all_libraries, set_rescanning_all_libraries) = create_signal(None);
    let request_library_rescan = create_action(move |_: &()| async move {
        // TODO: error handling
        set_rescanning_all_libraries(scan_all_libraries().await.ok());
    });

    let show_create_modal = RwSignal::new(false);
    // TODO: error handling
    let libraries = Resource::new(
        || (),
        move |_| async move { fetch_libraries().await.unwrap_or_default() },
    );

    view! {
        <section>
            <Heading>"Manage libraries"</Heading>
            <div class="flex items-center gap-4">
                <div class="grow">
                    <a
                        class="btn btn-outline w-full"
                        href=AppRoutes::Settings(SettingsComponent::Dashboard)
                    >
                        <span class="fa-solid fa-circle-caret-left"></span>
                        <span class="icon-pad"></span>
                        "Back to settings"
                    </a>
                </div>
                <div class="grow">
                    <button
                        class="btn w-full"
                        disabled=move || libraries().map_or(true, |libs| libs.is_empty())
                        on:click=move |_| request_library_rescan.dispatch(())
                        type="button"
                    >
                        "Reindex all"
                    </button>
                </div>
                <div class="grow">
                    <button
                        class="btn btn-primary w-full"
                        on:click=move |_| {
                            if show_create_modal() {
                                show_create_modal.set(false);
                            }
                            show_create_modal.set(true);
                        }
                    >
                        "New"
                    </button>
                </div>
            </div>
            <LibraryUpdateModal modal_visibility=show_create_modal />
            <Suspense fallback=PleaseWaitView>
                {move || {
                    libraries()
                        .map(|libs| {
                            view! {
                                <LibraryListHandler
                                    libraries=libs
                                    rescanning_all_libraries=rescanning_all_libraries
                                />
                            }
                        })
                }}

            </Suspense>
        </section>
    }
}

#[component]
fn LibraryListHandler(
    libraries: Vec<Library>,
    rescanning_all_libraries: ReadSignal<Option<String>>,
) -> impl IntoView {
    let (libraries, _) = create_signal(libraries);
    // TODO: update this regularly from backend
    let disable_all_actions = Memo::new(move |_| rescanning_all_libraries().is_some());

    view! {
        <Show
            when=move || !libraries().is_empty()
            fallback=|| {
                view! {
                    <Space />
                    <Placeholder
                        icon=view! {
                            <span class="fa-solid fa-stop-circle" style="font-size: 500%;"></span>
                        }
                            .into_view()
                        title=view! { "There are no libraries" }.into_view()
                        subtitle=view! {
                            "Please go ahead and create a library to get you started."
                        }
                            .into_view()
                    />
                }
            }
        >
            <Show when=move || rescanning_all_libraries().is_some()>
                <progress class="progress progress-info" max="100">
                    "Indexing…"
                </progress>
            </Show>
            <Space />
            <For
                each=libraries
                key=|lib| lib.id.clone()
                children=move |lib| {
                    view! {
                        <LibraryRow library=lib disable_all_actions=disable_all_actions.into() />
                    }
                }
            />
        </Show>
    }
}

#[component]
fn LibraryRow(library: Library, disable_all_actions: Signal<bool>) -> impl IntoView {
    let (library_is_reindexing, set_library_is_reindexing) = create_signal(None);
    let show_update_modal = RwSignal::new(false);

    let actions_disabled = move || disable_all_actions() || library_is_reindexing().is_some();

    let display_path = library
        .path
        .strip_prefix("file://")
        .map_or_else(|| library.path.clone(), |path| path.to_owned());
    let display_title = format!("{} ({})", &library.name, &library.id);

    let library_id = RwSignal::new(library.id.clone());
    // TODO: for deletion as well as reindexing show message that it is a
    //       background task if successful
    //       (document.getElementById(SambaIds.settingsToast) as?
    // HTMLDivElement?)?.let {           it.clear()
    //           it.ensureClass("p-4")
    //           it.append {
    //               toast {
    //                   ex?.let { exception ->
    //                       content {
    //                           +"The server has failed to process your request. "
    //                           +"Please try again. The server responded:
    // ${exception.message}"                   }
    //                   } ?: content {
    //                       +"The server has received your request and will process
    // it. "                       +"Go on with what you were doing before and
    // let the server "                       +"handle everything for you."
    //                   }
    //               }
    //           }
    //       }
    let request_library_deletion = create_action(move |_: &()| async move {
        // TODO: deletion approval
        delete_library(&library_id.get()).await.unwrap();
        refresh_page();
    });
    let request_library_reindexing = create_action(move |_: &()| async move {
        // TODO: error handling
        set_library_is_reindexing(scan_library(&library_id.get()).await.ok());
    });

    view! {
        <Card>
            <div class="flex gap-2">
                <div class="grow">
                    <p class="font-bold max-w-[80%] justify-left">
                        <a href=AppRoutes::Library(
                            LibraryAction::View(LibraryViewParameters {
                                filter: Some(format!("lib:{}", library_id.get())),
                                ..Default::default()
                            }),
                        )>{display_title}</a>
                    </p>
                    <p class="text-gray-600 text-xs mt-2 max-w-[80%] justify-left">
                        {display_path}
                    </p>
                </div>
                <div class="join">
                    <button
                        class="btn join-item tooltip"
                        data-tip="Reindex"
                        disabled=actions_disabled
                        on:click=move |_| request_library_reindexing.dispatch(())
                    >
                        <span class="fa-solid fa-arrows-rotate"></span>
                    </button>
                    <button
                        class="btn join-item tooltip"
                        data-tip="Edit"
                        disabled=actions_disabled
                        on:click=move |_| {
                            if show_update_modal() {
                                show_update_modal.set(false);
                            }
                            show_update_modal.set(true);
                        }
                    >
                        <span class="fa-solid fa-pen"></span>
                    </button>
                    <button
                        class="btn btn-error join-item tooltip"
                        data-tip="Delete"
                        disabled=actions_disabled
                        on:click=move |_| request_library_deletion.dispatch(())
                    >
                        <span class="fa-regular fa-trash-can"></span>
                    </button>
                </div>
            </div>
            <Show when=move || library_is_reindexing().is_some()>
                <progress class="progress progress-info" max="100">
                    "Indexing…"
                </progress>
            </Show>
        </Card>
        <LibraryUpdateModal modal_visibility=show_update_modal library=library />
    }
}

#[component]
pub fn LibraryUpdateModal(
    modal_visibility: RwSignal<bool>,
    #[prop(optional)] library: Option<Library>,
) -> impl IntoView {
    let (library_id, set_library_id) = create_signal(
        library
            .as_ref()
            .map(|lib| lib.id.clone())
            .unwrap_or_default(),
    );
    let (library_name, set_library_name) = create_signal(
        library
            .as_ref()
            .map(|lib| lib.name.clone())
            .unwrap_or_default(),
    );
    let (library_path, set_library_path) = create_signal(
        library
            .as_ref()
            .map(|lib| lib.path.clone())
            .unwrap_or_default(),
    );
    let (scan_subdirs, set_scan_subdirs) =
        create_signal(library.as_ref().map_or(true, |lib| lib.scan_subdirectories));
    let (share_with_others, set_share_with_others) =
        create_signal(library.as_ref().map_or(false, |lib| lib.shared));
    let (aoide_collection, _) =
        create_signal(library.as_ref().map(|lib| lib.aoide_collection.clone()));

    // TODO: input verification
    let is_update = library.is_some();
    let request_library_update = create_action(move |_: &()| async move {
        let library = Library {
            id: library_id.get().clone(),
            name: library_name.get().clone(),
            path: format!(
                "file://{}{}",
                library_path.get(),
                (!library_path.get().ends_with('/'))
                    .then_some("/")
                    .unwrap_or_default()
            ),
            aoide_collection: aoide_collection
                .get()
                .clone()
                // Only None if creation instead of update.
                .unwrap_or_default(),
            scan_subdirectories: scan_subdirs.get(),
            shared: share_with_others.get(),
            excluded_directories: vec![],
        };
        if !is_update {
            create_library(library).await.unwrap();
        } else {
            update_library(&library_id(), library).await.unwrap();
        }
        refresh_page();
    });

    view! {
        <dialog open=modal_visibility class="modal modal-bottom sm:modal-middle">
            <div
                class="modal-box md:flex md:flex-col md:justify-between md:h-[50%] md:min-h-[50%]"
                role="document"
            >
                <div class="flex items-center">
                    <h2 class="grow font-bold text-xl">
                        {if is_update { "Edit library" } else { "Create new library" }}
                    </h2>
                    <a
                        class="btn btn-circle btn-ghost"
                        aria-label="Close"
                        on:click=move |_| {
                            modal_visibility.set(false);
                        }
                    >
                        <span class="fa-wrapper fa fa-times"></span>
                    </a>
                </div>
                <div class="grid grid-cols-5 items-center gap-y-6">
                    <label class="col-span-2" for="library-id">
                        "Identifier:"
                    </label>
                    <input
                        class="input input-bordered col-span-3"
                        id="library-id"
                        type="text"
                        placeholder="lib-id"
                        disabled=is_update
                        prop:value=library_id
                        on:input=move |ev| {
                            set_library_id(event_target_value(&ev));
                        }
                    />
                    <label class="col-span-2" for="library-name">
                        "Name:"
                    </label>
                    <input
                        class="input input-bordered col-span-3"
                        id="library-name"
                        type="text"
                        placeholder="Library name"
                        prop:value=library_name
                        on:input=move |ev| {
                            set_library_name(event_target_value(&ev));
                        }
                    />
                    <label class="col-span-2" for="library-directory">
                        "Directory:"
                    </label>
                    <input
                        class="input input-bordered col-span-3"
                        id="library-directory"
                        type="text"
                        placeholder="/path/to/library"
                        prop:value=library_path
                        on:input=move |ev| {
                            set_library_path(event_target_value(&ev));
                        }
                    />
                    {is_update
                        .then(|| {
                            view! {
                                <p class="col-span-2 text-gray-600 text-xs">"aoide ID:"</p>
                                <input
                                    class="input input-bordered col-span-3 text-xs"
                                    disabled=true
                                    prop:value=aoide_collection
                                />
                            }
                        })}
                    <label class="col-span-2" for="library-scan-subdirs">
                        "Scan subdirectories"
                    </label>
                    <input
                        id="library-scan-subdirs"
                        class="col-span-3 toggle"
                        type="checkbox"
                        prop:checked=scan_subdirs
                        on:input=move |ev| {
                            set_scan_subdirs(event_target_checked(&ev));
                        }
                    />
                    <label class="col-span-2" for="library-share">
                        "Share with others"
                    </label>
                    <input
                        id="library-share"
                        class="col-span-3 toggle"
                        type="checkbox"
                        prop:checked=share_with_others
                        on:input=move |ev| {
                            set_share_with_others(event_target_checked(&ev));
                        }
                    />
                </div>
                <div class="mt-auto flex gap-2 items-center justify-end">
                    <button
                        class="btn"
                        on:click=move |_| {
                            modal_visibility.set(false);
                        }
                        type="button"
                    >
                        "Cancel"
                    </button>
                    <button
                        class="btn btn-primary"
                        on:click=move |_| request_library_update.dispatch(())
                    >
                        {if is_update { "Update" } else { "Create" }}
                    </button>
                </div>
            </div>
        </dialog>
    }
}
