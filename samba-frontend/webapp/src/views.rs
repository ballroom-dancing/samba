use leptos::*;
use samba_frontend_uicomponents::Placeholder;

use crate::AppRoutes;

mod about;
mod app;
mod dashboard;
mod footer;
mod header;
mod login;
mod messages;

pub mod wavesurfer;
pub use self::{about::*, app::*, dashboard::*, footer::*, header::*, login::*, messages::*};

#[component]
pub fn NotFoundView() -> impl IntoView {
    view! {
        <Placeholder
            icon=view! { <span style="font-size:500%;">"🚧"</span> }.into_view()
            title=view! { "We are sorry!" }.into_view()
            subtitle=view! { "The page you requested has not been found." }.into_view()
            commands=vec![
                view! {
                    <div class="btn-container">
                        <a class="btn btn-primary" href=AppRoutes::Home>
                            "Return to dashboard"
                        </a>
                    </div>
                }
                    .into_view(),
            ]
        />
    }
}

#[component]
pub fn PleaseWaitView() -> impl IntoView {
    view! {
        <Placeholder title=view! {
            <div class="animated loading loading-left">
                <p>"Loading resources, please wait…"</p>
            </div>
        }
            .into_view() />
    }
}
