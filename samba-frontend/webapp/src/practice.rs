use samba_shared::playlists::PlaylistItem;
use uuid::Uuid;

mod backend;
mod navigation;
mod views;

pub use self::{navigation::PracticeAction, views::*};

#[derive(Clone, Debug, PartialEq)]
struct SessionItem {
    uuid: Uuid,
    item: PlaylistItem,
}

impl SessionItem {
    fn new(item: PlaylistItem) -> Self {
        Self {
            uuid: Uuid::new_v4(),
            item,
        }
    }
}
