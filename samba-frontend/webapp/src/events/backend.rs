use anyhow::{anyhow, Result};
use leptos::*;
use samba_frontend_shared::AppState;
use samba_shared::events::{Event, StoredEvent};

use crate::util::{get_from_remote, post_to_remote, put_to_remote};

pub async fn fetch_events() -> Result<Vec<StoredEvent>> {
    get_from_remote("events").await
}

pub async fn fetch_event(id: i32) -> Result<StoredEvent> {
    get_from_remote(&format!("events/{id}")).await
}

pub async fn create_event(new_event: &Event) -> Result<i32> {
    post_to_remote("events/create", new_event).await
}

pub async fn update_event(event_id: i32, updated_event: &Event) -> Result<()> {
    put_to_remote(&format!("events/{}", event_id), updated_event).await
}

pub async fn fetch_event_report_url(event_id: i32) -> Result<String> {
    let app_state = use_context::<AppState>().ok_or(anyhow!(
        "Fetching reports requires an initialized app with state."
    ))?;
    Ok(format!(
        "{}/events/{}/report",
        &app_state.backend_url, event_id
    ))
}
