use leptos::*;
use samba_frontend_uicomponents::{Heading, Placeholder};
use samba_shared::events::StoredEvent;

use super::super::{backend::fetch_events, navigation::EventAction};
use crate::{views::PleaseWaitView, AppRoutes};

#[component]
pub fn EventListView() -> impl IntoView {
    // TODO: proper error handling
    let events = Resource::new(
        || (),
        move |_| async move { fetch_events().await.unwrap_or_default() },
    );

    view! {
        <section>
            <Heading>"Your events"</Heading>
            <Suspense fallback=|| {
                view! { <PleaseWaitView /> }
            }>
                {move || { events().map(|evts| view! { <EventListHandler events=evts /> }) }}
            </Suspense>
        </section>
    }
}

#[component]
fn EventListHandler(events: Vec<StoredEvent>) -> impl IntoView {
    let (events, _) = create_signal(events);

    view! {
        <section>
            <Show
                when=move || events().is_empty()
                fallback=|| {
                    view! {
                        <Placeholder
                            icon=view! {
                                <span
                                    class="fa-solid fa-stop-circle"
                                    style="font-size: 500%;"
                                ></span>
                            }
                                .into_view()
                            title=view! { "There are no events" }.into_view()
                            subtitle=view! { "Up to now you did not register any events." }
                                .into_view()
                            commands=vec![
                                view! {
                                    <div class="btn-container">
                                        <a
                                            class="btn btn-primary u-inline-block"
                                            href=AppRoutes::Events(EventAction::Create)
                                        >
                                            "Create a new event"
                                        </a>
                                    </div>
                                }
                                    .into_view(),
                            ]
                        />
                    }
                }
            >
                <div class="btn-container">
                    <a
                        class="btn btn-primary u-pull-right"
                        href=AppRoutes::Events(EventAction::Create)
                    >
                        "New"
                    </a>
                </div>
                <table class="table borderless striped u-text-left">
                    <thead>
                        <tr>
                            <th>"Date"</th>
                            <th>"Title"</th>
                            <th>"Location"</th>
                            <th>"Tags"</th>
                        </tr>
                    </thead>
                    <tbody>
                        <For each=events key=|x| x.identifier let:evt>
                            <tr>
                                <td>{evt.date.to_string()}</td>
                                <td>
                                    <a href=AppRoutes::Events(
                                        EventAction::View(evt.identifier),
                                    )>{evt.title.clone()}</a>
                                </td>
                                <td>{evt.location.clone()}</td>
                                <td class="tag-container">

                                    {evt
                                        .tags
                                        .iter()
                                        .map(|tag| {
                                            view! { <div class="tag">{tag}</div> }
                                        })
                                        .collect::<Vec<_>>()}

                                </td>
                            </tr>
                        </For>
                    </tbody>
                </table>
            // TODO: pagination
            </Show>
        </section>
    }
}
