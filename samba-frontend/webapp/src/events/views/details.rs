use leptos::*;
use leptos_router::use_params;
use samba_frontend_uicomponents::Heading;
use samba_shared::events::StoredEvent;

use super::super::{
    backend::{fetch_event, fetch_event_report_url},
    navigation::{EventAction, EventId},
};
use crate::{
    views::{NotFoundView, PleaseWaitView},
    AppRoutes,
};

#[component]
pub fn EventDetailsView() -> impl IntoView {
    let event_id = use_params::<EventId>();
    let event = Resource::new(event_id, move |_| async move {
        fetch_event(event_id.get().ok()?.id).await.ok()
    });

    view! {
        <section>
            <Suspense fallback=|| {
                view! { <PleaseWaitView /> }
            }>
                {move || { event().map(|evt| view! { <EventDetailsHandler event=evt /> }) }}
            </Suspense>
        </section>
    }
}

#[component]
fn EventDetailsHandler(event: Option<StoredEvent>) -> impl IntoView {
    if let Some(event) = event {
        // TODO: proper error handling
        let (event, _) = create_signal(event);
        let event_title = move || event().title.clone();

        let report_url = Resource::new(
            || (),
            move |_| async move { fetch_event_report_url(event().identifier).await.ok() },
        );
        let report_button = move || {
            view! {
                <Suspense>
                    {move || {
                        report_url()
                            .map(|report_url| {
                                report_url
                                    .map(|url| {
                                        view! {
                                            <a
                                                class="btn btn-primary u-pull-right u-inline-block ml-1"
                                                download=format!("{}.pdf", event_title())
                                                href=url.clone()
                                                rel="external"
                                            >
                                                <span class="fa-regular fa-file-pdf"></span>
                                            </a>
                                        }
                                    })
                            })
                    }}

                </Suspense>
            }
        };

        let has_tags = move || !event().tags.is_empty();
        let tag_list = move || {
            view! {
                <Show when=has_tags>
                    <tr>
                        <th class="u-text-left">"Tags"</th>
                        <td class="u-text-left tag-container">
                            {move || {
                                event()
                                    .tags
                                    .iter()
                                    .map(|tag| {
                                        view! { <div class="tag">{tag}</div> }
                                    })
                                    .collect::<Vec<_>>()
                            }}

                        </td>
                    </tr>
                </Show>
            }
        };

        view! {
            <section>
                <Heading>"Viewing event “" {event_title} "”"</Heading>
                <div class="btn-container">
                    {report_button}
                    <a
                        class="btn btn-primary u-pull-right u-inline-block"
                        href=AppRoutes::Events(EventAction::Edit(event().identifier))
                    >
                        "Edit"
                    </a>
                </div>
                <table class="table borderless small">
                    <tbody>
                        <tr>
                            <th class="u-text-left">"Date"</th>
                            <td class="u-text-left">{event().date.to_string()}</td>
                        </tr>
                        <tr>
                            <th class="u-text-left">"Location"</th>
                            <td class="u-text-left">{event().location.clone()}</td>
                        </tr>
                        <tr>
                            <th class="u-text-left">"Description"</th>
                            <td class="u-text-left">{event().description.clone()}</td>
                        </tr>
                        <tr>
                            <th class="u-text-left">"Organizers"</th>
                            <td class="u-text-left">{event().organizers.join(", ")}</td>
                        </tr>
                        {tag_list}
                    </tbody>
                </table>
            </section>
        }
        .into_view()
    } else {
        view! {
            <section>
                // TODO: adjust not found view
                <NotFoundView />
            </section>
        }
        .into_view()
    }
}
