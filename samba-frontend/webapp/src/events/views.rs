mod details;
mod list;
mod update;

pub use self::{details::EventDetailsView, list::EventListView, update::EventUpdateView};
