use leptos::*;
use samba_frontend_webapp::App;

mod tauri;
use tauri::{get_backend_port, running_in_tauri};

#[cfg(feature = "ssr")]
#[tokio::main]
async fn main() {
    use std::sync::Arc;

    use axum::{routing::post, Router};
    use leptos::*;
    use leptos_axum::{generate_route_list, LeptosRoutes};
    use samba_frontend_webapp::fallback::file_and_error_handler;

    let conf = get_configuration(None).await.unwrap();
    let addr = conf.leptos_options.site_addr;
    let leptos_options = Arc::new(conf.leptos_options);
    // Generate the list of routes in your Leptos App
    let routes = generate_route_list(|| view! { <App /> }).await;

    let app = Router::new()
        .route("/api/*fn_name", post(leptos_axum::handle_server_fns))
        .leptos_routes(leptos_options.clone(), routes, || view! { <App /> })
        .fallback(file_and_error_handler)
        .with_state(leptos_options);

    log!("listening on http://{}", &addr);
    axum::Server::bind(&addr)
        .serve(app.into_make_service())
        .await
        .unwrap();
}

#[cfg(not(feature = "ssr"))]
pub fn main() {
    use leptos::logging::log;
    use samba_frontend_shared::AppState;

    _ = console_log::init_with_level(log::Level::Debug);
    console_error_panic_hook::set_once();

    log!(
        "csr mode - mounting to body / detected tauri: {}",
        running_in_tauri()
    );

    mount_to_body(|| {
        provide_context(AppState::new(
            format!("http://localhost:{}", get_backend_port()),
            running_in_tauri(),
        ));

        view! { <App /> }
    });
}
