use leptos_router::*;

pub enum LibraryAction {
    View(LibraryViewParameters),
    ViewByDance,
}

#[derive(Clone, Debug, Default, PartialEq)]
pub struct LibraryViewParameters {
    pub page: Option<usize>,
    pub page_size: Option<usize>,
    pub filter: Option<String>,
    pub sort: Option<String>,
}

impl Params for LibraryViewParameters {
    fn from_map(map: &ParamsMap) -> Result<Self, ParamsError> {
        Ok(Self {
            page: map.get("page").and_then(|page| page.parse().ok()),
            page_size: map.get("paginate").and_then(|page| page.parse().ok()),
            filter: map.get("filter").cloned(),
            sort: map.get("sort").cloned(),
        })
    }
}

impl std::fmt::Display for LibraryAction {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(&match self {
            LibraryAction::View(parameters) => {
                let mut query_params = vec![];
                if let Some(page) = parameters.page {
                    query_params.push(format!("page={page}"));
                }
                if let Some(page_size) = parameters.page_size {
                    query_params.push(format!("paginate={page_size}"));
                }
                if let Some(ref filter) = parameters.filter {
                    query_params.push(format!("filter={filter}"));
                }
                format!(
                    "{}{}",
                    if query_params.is_empty() { "" } else { "?" },
                    query_params.join("&")
                )
            },
            LibraryAction::ViewByDance => "/bydance".into(),
        })
    }
}
