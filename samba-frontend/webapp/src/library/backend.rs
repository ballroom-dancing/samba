use anyhow::{anyhow, Context, Result};
use aoide::{
    json::{playlist::Entity as SerialPlaylistEntity, track::Entity as SerialTrackEntity},
    playlist::Entity as PlaylistEntity,
    TrackEntity, TrackUid,
};
use leptos::*;
use samba_frontend_shared::AppState;
use samba_shared::{dancing::Dance, playlists::CreationFromTrackQueryParameters, Paginated};
use serde::Serialize;

use crate::util::{
    get_from_remote, get_from_remote_with_query, get_from_remote_without_error_handling,
};

#[derive(Default, Serialize)]
pub struct TrackQueryParams {
    #[serde(rename = "query", skip_serializing_if = "Option::is_none")]
    pub filter: Option<String>,
    pub page: usize,
    pub page_size: usize,
}

pub async fn fetch_tracks(parameters: TrackQueryParams) -> Result<Paginated<Vec<TrackEntity>>> {
    // TODO: respect user's exclude list from settings
    let paginated_serial_tracks: Paginated<Vec<SerialTrackEntity>> =
        get_from_remote_with_query("tracks", Some(parameters)).await?;
    let mut tracks: Vec<TrackEntity> = Vec::with_capacity(paginated_serial_tracks.result.len());
    for t in paginated_serial_tracks.result {
        tracks.push(t.try_into().with_context(|| "invalid track entity")?);
    }

    Ok(Paginated {
        page: paginated_serial_tracks.page,
        page_size: paginated_serial_tracks.page_size,
        total_count: paginated_serial_tracks.total_count,
        result: tracks,
    })
}

pub async fn fetch_dances() -> Result<Vec<Dance>> {
    get_from_remote("dances").await
}

#[allow(unsafe_code)]
pub async fn fetch_track_artwork_url(track_uid: &TrackUid) -> Result<String> {
    let artwork_url = format!("tracks/{}/artwork", track_uid);

    if get_from_remote_without_error_handling(&artwork_url)
        .await?
        .status()
        == 200
    {
        Ok(artwork_url)
    } else {
        Err(anyhow!("No artwork on this URL"))
    }
}

pub async fn fetch_track_file_url(track_uid: &TrackUid) -> Result<String> {
    let app_state = use_context::<AppState>().ok_or(anyhow!(
        "Fetching music files requires an initialized app with state."
    ))?;
    Ok(format!(
        "{}/tracks/{}/file",
        &app_state.backend_url, track_uid
    ))
}

pub async fn create_filtered_library_playback(search_filter: &str) -> Result<PlaylistEntity> {
    let serial_entity: SerialPlaylistEntity = get_from_remote_with_query(
        "playlists/create",
        Some(CreationFromTrackQueryParameters {
            query: search_filter.into(),
            offset: 0,
            limit: None,
            randomize_order: false,
        }),
    )
    .await?;

    Ok(serial_entity.into())
}

pub async fn create_filtered_and_paged_library_playback(
    search_filter: &str,
    page: usize,
    page_size: usize,
) -> Result<PlaylistEntity> {
    let serial_entity: SerialPlaylistEntity = get_from_remote_with_query(
        "playlists/create",
        Some(CreationFromTrackQueryParameters {
            query: search_filter.into(),
            offset: page * page_size,
            limit: Some(page_size),
            randomize_order: false,
        }),
    )
    .await?;

    Ok(serial_entity.into())
}
