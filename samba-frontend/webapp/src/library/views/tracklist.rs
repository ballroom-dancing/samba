use aoide::TrackEntity;
use leptos::*;
use leptos_router::*;
use samba_frontend_shared::AppState;
use samba_frontend_uicomponents::{Heading, HeadingLevel, Pagination, Placeholder, Space};
use samba_shared::{dancing::Dance, preferences::TrackRowIndicator, Paginated};

use super::super::{
    backend::{
        create_filtered_and_paged_library_playback, create_filtered_library_playback, fetch_dances,
        fetch_tracks, TrackQueryParams,
    },
    navigation::{LibraryAction, LibraryViewParameters},
    uicomponents::TrackRow,
};
use crate::{
    practice::PracticeAction, settings::SettingsComponent, util::scroll_top, views::PleaseWaitView,
    AppRoutes,
};

/// Determine whether a column is the sort column by looking at the `sort`
/// query parameter which must identify the column and optionally the sort
/// direction, e.g. `sort=title` (title ascending), `sort=title,asc`,
/// `sort=title,desc`.
fn column_is_sorted_by(sort_string: &Option<String>, name: &str, descending: bool) -> bool {
    sort_string.as_ref().map_or(false, |sort| {
        let (key, direction) = sort.split_once(',').unwrap_or((sort, ""));
        key == name && ((direction == "desc" && descending) || (direction != "desc" && !descending))
    })
}

#[component]
pub fn TrackListView() -> impl IntoView {
    let parameters = use_query::<LibraryViewParameters>();
    let parameters =
        Memo::new(move |_| parameters().expect("LibraryViewParameters can always be extracted."));

    let app_state =
        use_context::<AppState>().expect("Tracks are only shown when logged in and initialized");

    let current_page = RwSignal::new(parameters().page.unwrap_or(1));
    let page_size = RwSignal::new(
        parameters().page_size.unwrap_or(
            app_state
                .user_preferences
                .get()
                .pagination_default_page_size,
        ),
    );
    Effect::new(move |_| {
        if let Some(page) = parameters().page {
            current_page.set(page);
        }
        if let Some(size) = parameters().page_size {
            page_size.set(size);
        }
    });

    // TODO: respect sorting
    // TODO: non-local with serial types?
    let dances_and_tracks = create_local_resource(parameters, move |_| async move {
        // Whenever we fetch, we scroll to top because the navigate call
        // does not respect the scroll option in NavigateOptions.
        // TODO: remove when NavigateOptions work as intended
        scroll_top(true);

        let dances = fetch_dances().await.unwrap_or_default();
        fetch_tracks(TrackQueryParams {
            filter: parameters().filter.clone(),
            page: current_page(),
            page_size: page_size(),
        })
        .await
        .map(move |res| (dances, res))
        .ok()
    });

    let (drawer_open, set_drawer_open) = create_signal(false);
    let current_playback_list = RwSignal::new(vec![]);
    let current_playback_item = RwSignal::new(0);
    Effect::new(move |_| {
        if current_playback_item() == current_playback_list().len() {
            set_drawer_open(false);
        }
    });

    view! {
        <section>
            <Heading>"Your library"</Heading>
            <div class="drawer drawer-end">
                <input
                    class="drawer-toggle"
                    id="playback-drawer"
                    type="checkbox"
                    prop:checked=drawer_open
                    on:input=move |ev| set_drawer_open(event_target_checked(&ev))
                />
                <div class="drawer-content">
                    <Transition fallback=|| {
                        view! { <PleaseWaitView /> }
                    }>
                        {move || {
                            dances_and_tracks()
                                .map(|dances_and_tracks| {
                                    if let Some(
                                        (dances, Paginated { total_count, result: tracks, .. }),
                                    ) = dances_and_tracks {
                                        let (dances, _) = create_signal(dances);
                                        let tracks = RwSignal::new(tracks);
                                        let total_count = RwSignal::new(total_count);
                                        view! {
                                            <TrackPageHandler
                                                parameters=parameters
                                                tracks=tracks
                                                current_playback_list=current_playback_list
                                                playback_drawer_open=drawer_open
                                                dances=dances.into()
                                                total_count=total_count
                                                current_page=current_page
                                                page_size=page_size
                                            />
                                        }
                                            .into_view()
                                    } else {
                                        view! { <TrackListFetchError parameters=parameters/> }.into_view()
                                    }
                                })
                        }}
                    </Transition>
                </div>
                <div class="drawer-side"
                     class=("lg:drawer-open", move || !current_playback_list().is_empty())>
                    <label for="playback-drawer" aria-label="close sidebar" class="drawer-overlay !bg-transparent"></label>
                    <div class="menu p-4 w-80 min-h-full bg-base-200 text-base-content">
                        <Heading level=HeadingLevel::Section>
                            "Playback Queue"
                        </Heading>
                        // TODO: table of current playback list
                    </div>
                </div>
            </div>
        </section>
    }
}

#[component]
fn TrackPageHandler(
    parameters: Memo<LibraryViewParameters>,
    dances: Signal<Vec<Dance>>,
    tracks: RwSignal<Vec<TrackEntity>>,
    current_playback_list: RwSignal<Vec<TrackEntity>>,
    playback_drawer_open: ReadSignal<bool>,
    total_count: RwSignal<usize>,
    current_page: RwSignal<usize>,
    page_size: RwSignal<usize>,
) -> impl IntoView {
    let app_state =
        use_context::<AppState>().expect("Tracks are only shown when logged in and initialized");

    let (help_modal_visibility, set_help_modal_visibility) = create_signal(false);
    let has_track_row_indicator = move || {
        !matches!(
            (app_state.user_preferences)().track_row_indicator,
            TrackRowIndicator::None
        )
    };
    let default_page_size = app_state
        .user_preferences
        .get()
        .pagination_default_page_size;

    let (current_filter, set_current_filter) =
        create_signal(parameters().filter.unwrap_or_default());
    Effect::new(move |_| {
        if let Some(ref filter) = parameters().filter {
            set_current_filter(filter.to_string());
        }
    });
    let sort_string = Memo::new(move |_| parameters().sort);
    let (total_pages, set_total_pages) = create_signal(0);
    Effect::new(move |_| {
        set_total_pages((total_count() + page_size() - 1) / page_size());
    });

    // when current page changes, the view updates but users probably expect to
    // be sent to the top of the page, therefore we'll add a callback for that
    Effect::new(move |_| {
        current_page.track();
        spawn_local(async move {
            let mut parameters = parameters();
            parameters.filter = Some(current_filter()).filter(|f| !f.is_empty());
            parameters.page = Some(current_page());
            parameters.page_size = Some(page_size()).filter(|&s| s != default_page_size);
            use_navigate()(
                &AppRoutes::Library(LibraryAction::View(parameters)).to_string(),
                Default::default(),
            );
        });
    });

    let play_ad_hoc_playlist = move |play_all_pages: bool| {
        spawn_local(async move {
            // TODO: handle error
            let entity = if play_all_pages {
                create_filtered_library_playback(&current_filter())
                    .await
                    .unwrap()
            } else {
                create_filtered_and_paged_library_playback(
                    &current_filter(),
                    current_page(),
                    page_size(),
                )
                .await
                .unwrap()
            };
            // TODO: load playlist into current_playback_list
            current_playback_list.set(tracks());
            use_navigate()(
                &AppRoutes::Practice(PracticeAction::Saved(entity.hdr.uid.to_string(), None))
                    .to_string(),
                Default::default(),
            );
        });
    };

    view! {
        <dialog open=help_modal_visibility class="modal modal-bottom sm:modal-middle">
            <div class="modal-box">
                <h3 class="font-bold text-lg">"Filtering the library"</h3>
                <p class="pt-4">
                    "The basic query syntax is " <code>"field:value"</code>
                    " (put quotes around the value if it contains spaces). "
                    "By default, the fields track_title, album_title, track_artist, album_artist, and genre are searched. "
                    "There are a few more fields, among them the library columns dance and mpm."
                </p>
                <p>
                    "Some fields accept range queries, e.g. mpm or tempo_bpm. For example, "
                    <code>"mpm:[23 TO 25]"</code>
                    "finds all tracks with 23 to 25 mpm (inclusive). If you want to specify an exclusive boundary, use "
                    <code>"mpm:{23 TO 25}"</code> "(curly braces)."
                </p>
                <p class="pb-4">
                    "This documentation should be extended with more available fields. Contributions are welcome. "
                    "The query syntax itself is documented in "
                    <a
                        class="link"
                        href="https://docs.rs/tantivy/latest/tantivy/query/struct.QueryParser.html"
                    >
                        "the query parser module"
                    </a> " of the underlying tantivy library."
                </p>
                <div class="modal-action">
                    <form method="dialog">
                        <button class="btn">Close</button>
                    </form>
                </div>
            </div>
        </dialog>
        <form
            action=AppRoutes::Library(LibraryAction::View(Default::default()))
            method="get"
            class="flex gap-2"
        >
            <div class="grow flex join">
                <input
                    class="join-item grow form-input appearance-none bg-base-100"
                    name="filter"
                    placeholder="Query expression"
                    type="search"
                    prop:value=current_filter
                    on:input=move |ev| {
                        set_current_filter(event_target_value(&ev));
                    }
                />
                <button
                    type="button"
                    class="join-item btn bg-base-100"
                    on:click=move |_| {
                        if help_modal_visibility() {
                            set_help_modal_visibility(false);
                        }
                        set_help_modal_visibility(true);
                    }
                >
                    <span class="fa-solid fa-circle-question"></span>
                </button>
                <button type="submit" class="join-item btn">
                    <span class="fa-solid fa-filter"></span>
                </button>
            </div>
            <div class="join flex">
                <Show when=move || !current_playback_list().is_empty()>
                    <label for="playback-drawer" class="btn btn-neutral join-item drawer-button">
                        <span
                            class="fa-solid"
                            class:fa-angles-right=playback_drawer_open
                            class:fa-angles-left=move || !playback_drawer_open()
                        />
                        <span class="fa-solid fa-circle-play" />
                    </label>
                </Show>
                <Show when=move || current_playback_list().is_empty()>
                    <button
                        type="button"
                        class="btn join-item"
                        on:click=move |_| play_ad_hoc_playlist(false)
                    >
                        <span class="fa-solid fa-circle-play" />
                    </button>
                </Show>
                <div class="dropdown dropdown-end join-item">
                    <div tabindex="0" role="button" class="btn join-item px-2">
                        <span class="fa-solid fa-caret-down" />
                    </div>
                    <ul tabindex="0" class="dropdown-content z-10 menu p-2 shadow bg-base-100 rounded-box w-64">
                        <li><a on:click=move |_| play_ad_hoc_playlist(false)>"Play current page only"</a></li>
                        <li><a on:click=move |_| play_ad_hoc_playlist(true)>"Play all pages for this query"</a></li>
                    </ul>
                </div>
            </div>
        </form>
        {move || {
            parameters()
                .filter
                .map(|filter| {
                    view! {
                        <p class="text-sm pt-3 text-gray-600">
                            "Showing " {move || page_size().min(total_count())} " of " {total_count}
                            " tracks "
                            {(total_pages() > 1)
                                .then(|| {
                                    view! {
                                        "(page "
                                        {current_page}
                                        ") "
                                    }
                                })} "that matched your query “" {filter} "”"
                        </p>
                    }
                })
        }}
        <div class="pt-6">
            <table class="table table-pin-rows">
                <thead>
                    <tr>
                        {move || {
                            has_track_row_indicator()
                                .then(|| {
                                    view! {
                                        <ColumnHeader
                                            id="track-color"
                                            sort=sort_string
                                            classes="track-artwork-preview".into() />
                                    }
                                })
                        }}
                        <ColumnHeader sort=sort_string id="title">
                            "Title"
                        </ColumnHeader>
                        <ColumnHeader sort=sort_string id="artist">
                            "Artist"
                        </ColumnHeader>
                        <ColumnHeader sort=sort_string id="album">
                            "Album"
                        </ColumnHeader>
                        <ColumnHeader sort=sort_string id="dance">
                            "Dance"
                        </ColumnHeader>
                        <ColumnHeader id="mpm" sort=sort_string classes="u-text-center".into()>
                            <abbr title="Measures Per Minute (default of \
                            the primary dance shown if not \
                            extracted from track)">"MPM"</abbr>
                        </ColumnHeader>
                        <ColumnHeader id="length" sort=sort_string classes="u-text-right".into()>
                            "Length"
                        </ColumnHeader>
                    </tr>
                </thead>
                <tbody>
                    {move || {
                        if tracks().is_empty() {
                            view! {
                                <td colSpan=format!(
                                    "{}",
                                    if has_track_row_indicator() { 7 } else { 6 },
                                )>
                                    <EmptyTrackListFallback
                                        current_page=current_page.into()
                                        page_size=page_size.into()
                                    />
                                </td>
                            }
                                .into_view()
                        } else {
                            view! {
                                <For each=tracks key=|x| x.hdr.uid.clone() let:track>
                                    <TrackRow track=track dances=dances />
                                </For>
                            }
                                .into_view()
                        }
                    }}

                </tbody>
            </table>
        </div>
        <Show when=move || { total_pages() > 1 }>
            <Space />
            <div class="flex justify-center">
                <Pagination
                    current_page=current_page
                    total_pages=total_pages
                    ellipsis_distance=app_state.user_preferences.get().pagination_ellipsis_distance
                />
            </div>
        </Show>
    }
}

#[component]
fn EmptyTrackListFallback(current_page: Signal<usize>, page_size: Signal<usize>) -> impl IntoView {
    view! {
        <Placeholder
            icon=view! { <span class="fa-solid fa-stop-circle" style="font-size: 500%;"></span> }
                .into_view()
            title="No track found".into_view()
            subtitle="Your query did not yield any results. Please check that your \
                 library is indexed and your filters are not too restricted."
                .into_view()
            commands=vec![
                view! {
                    <div class="btn-container">
                        <a
                            class="btn btn-primary u-block m-1"
                            href=AppRoutes::Settings(SettingsComponent::Library)
                        >
                            "Go to library settings"
                        </a>
                    </div>
                }
                    .into_view(),
                view! {
                    <div class="btn-container">
                        <a
                            class="btn btn-primary u-block m-1"
                            href=AppRoutes::Library(
                                LibraryAction::View(LibraryViewParameters {
                                    filter: None,
                                    page: Some(current_page()),
                                    page_size: Some(page_size()),
                                    sort: None,
                                }),
                            )
                        >

                            "Reset filters"
                        </a>
                    </div>
                }
                    .into_view(),
            ]
        />
    }
}

#[component]
fn TrackListFetchError(parameters: Memo<LibraryViewParameters>) -> impl IntoView {
    view! {
        <Placeholder
            icon=view! { <span class="fa-solid fa-stop-circle" style="font-size: 500%;"></span> }
                .into_view()
            title="Error while fetching library".into_view()
            subtitle="If you have not yet done so, please select a library folder and index the library. \
                 If your library is all valid, please make sure you did not apply too strict filtering."
                .into_view()
            commands=vec![
                view! {
                    <div class="btn-container">
                        <a
                            class="btn btn-primary u-block m-1"
                            href=AppRoutes::Settings(SettingsComponent::Library)
                        >
                            "Go to library settings"
                        </a>
                    </div>
                }
                    .into_view(),
                view! {
                    <div class="btn-container">
                        <a
                            class="btn btn-primary u-block m-1"
                            href=AppRoutes::Library(
                                LibraryAction::View(LibraryViewParameters {
                                    filter: None,
                                    ..parameters().clone()
                                }),
                            )
                        >

                            "Reset filters"
                        </a>
                    </div>
                }
                    .into_view(),
            ]
        />
    }
}

/// Print a column header which indicates if this column participates in the
/// current sorting directions.
#[component]
pub fn ColumnHeader(
    id: &'static str,
    sort: Memo<Option<String>>,
    #[prop(optional)] classes: Option<String>,
    #[prop(optional)] children: Option<Children>,
) -> impl IntoView {
    // TODO: flush right instead of margin
    let sorted_view = move || {
        if column_is_sorted_by(&sort(), id, false) {
            view! { <span class="ml-2 fa-solid fa-sort-up tooltip" data-tip="sorted ascending"></span> }
        .into_view()
        } else if column_is_sorted_by(&sort(), id, true) {
            view! { <span class="ml-2 fa-solid fa-sort-down" data-tip="sorted descending"></span> }
                .into_view()
        } else {
            View::default().into_view()
        }
    };

    let toggle_sorting = move |_| {
        // This establishes a three-click cycle: none → ascending sorting →
        // descending sorting → none.
        let new_sorting_query_param = if column_is_sorted_by(&sort(), id, false) {
            // column is current sort column (ascending) → descending
            Some(format!("{id},desc"))
        } else if column_is_sorted_by(&sort(), id, true) {
            // column is descending sort column → remove
            None
        } else {
            // column is not current sort column (neither ascending, see
            // previous branch, nor descending, see this branch) → ascending
            Some(format!("{id},asc"))
        };

        // TODO: do not unwrap and check why removal does not work
        let url = web_sys::Url::new(&window().location().href().unwrap()).unwrap();
        if let Some(sort) = new_sorting_query_param {
            url.search_params().set("sort", &sort);
        } else {
            url.search_params().delete("sort");
        }
        window().location().assign(&url.search()).unwrap();
    };

    view! {
        <th class=move || classes.clone().unwrap_or_default() on:click=toggle_sorting>
            {children.map(|children| children())}
            {sorted_view}
        </th>
    }
}
