use itertools::Itertools;
use leptos::*;
use samba_frontend_uicomponents::{Heading, HeadingLevel};
use samba_shared::dancing::{Dance, DanceCategory};

use super::super::{backend::fetch_dances, uicomponents::DanceCardLink};
use crate::views::PleaseWaitView;

#[component]
pub fn LibraryByDanceView() -> impl IntoView {
    let sorted_dances = Resource::new(
        || (),
        move |_| async move {
            let mut dances = fetch_dances().await.unwrap_or_default();
            dances.sort_by(|a, b| a.category.cmp(&b.category));
            dances
        },
    );

    view! {
        <Suspense fallback=|| {
            view! { <PleaseWaitView /> }
        }>
            {move || { sorted_dances().map(|dances| view! { <DanceListHandler dances=dances /> }) }}
        </Suspense>
    }
}

#[component]
fn DanceListHandler(dances: Vec<Dance>) -> impl IntoView {
    let mut dances_by_cat: Vec<(String, ReadSignal<Vec<ReadSignal<Dance>>>)> =
        Vec::with_capacity(4);
    for (key, group) in &dances.into_iter().chunk_by(|d| d.category) {
        let (group_signal, _) = create_signal(
            group
                .map(|dance| {
                    let (dance_signal, _) = create_signal(dance);
                    dance_signal
                })
                .collect(),
        );
        dances_by_cat.push((
            match key {
                DanceCategory::Ballroom => "Ballroom".into(),
                DanceCategory::Latin => "Latin".into(),
                DanceCategory::Social => "Social".into(),
                DanceCategory::Unknown => "Others".into(),
            },
            group_signal,
        ));
    }
    let (dances_by_cat, _) = create_signal(dances_by_cat);

    view! {
        <Heading>"Select the dance to browse for"</Heading>
        <For
            each=dances_by_cat
            key=|(name, _)| name.clone()
            children=|(name, dances)| {
                view! {
                    <section>
                        <Heading level=HeadingLevel::Section>{name}</Heading>
                        <For each=dances key=|dance| dance().name.clone() let:x>
                            <DanceCardLink dance=x />
                        </For>
                    </section>
                }
            }
        />
    }
}
