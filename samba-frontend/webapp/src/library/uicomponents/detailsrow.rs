use aoide::{track::tag::FACET_ID_GENRE, Track, TrackEntity, TrackUid};
use leptos::*;
use samba_frontend_shared::AppState;
use samba_frontend_uicomponents::Badge;
use samba_shared::{
    dancing::FACET_ID_DANCE,
    preferences::TrackRowIndicator,
    tracks::metadata::{FACET_ID_MOVIE, FACET_ID_MPM},
};

use crate::aoide::{get_tags_for_track_excluding_facets, join_tags_for_facet};

mod action_buttons;
use action_buttons::{DownloadButton, LintButton, PreviewButton};
mod artwork;
use artwork::TrackArtwork;
mod lintrow;
use lintrow::LintRow;
mod playerrow;
use playerrow::PlayerRow;

fn extract_file_name(uri: &str) -> String {
    if let Some(name) = uri.split('/').last() {
        match urlencoding::decode(name) {
            Ok(file) => file.into_owned(),
            Err(_) => "Invalid filename".into(),
        }
    } else {
        uri.to_owned()
    }
}

/// Print details for `track` as row to be included in a table cell or a card
/// body.
#[component]
pub fn TrackDetailsRow(
    track: ReadSignal<TrackEntity>,
    show_preview_row: RwSignal<Option<bool>>,
    show_lint_row: RwSignal<Option<bool>>,
) -> impl IntoView {
    let app_state = use_context::<AppState>()
        .expect("Tracks can only be shown when logged in, i.e. app initialized");
    let has_track_row_indicator = move || {
        !matches!(
            (app_state.user_preferences)().track_row_indicator,
            TrackRowIndicator::None
        )
    };

    let track_id = Signal::derive(move || track().hdr.uid.clone());
    let track_body = Signal::derive(move || track().body.track.clone());
    let source_uri = move || {
        track()
            .body
            .track
            .media_source
            .content
            .link
            .path
            .to_string()
    };
    let file_name = Signal::derive(move || extract_file_name(&source_uri()));

    let preview_row_visible = RwSignal::new(show_preview_row.get().unwrap_or(false));
    Effect::new(move |_| {
        show_preview_row.set(Some(preview_row_visible.get()));
    });
    let lint_row_visible = RwSignal::new(show_lint_row.get().unwrap_or(false));
    Effect::new(move |_| show_lint_row.set(Some(lint_row_visible.get())));

    view! {
        {move || {
            has_track_row_indicator().then_some(view! { <td class="track-artwork-preview"></td> })
        }}

        <td colSpan="6">
            <div class="flex gap-4">
                <div class="w-1/5 justify-center">
                    <TrackArtwork track_id=track_id />
                </div>
                <div class="grow">
                    <DetailsList track_uid=track_id track=track_body />
                </div>
                <div class="join join-vertical md:join-horizontal">
                    <Show when=move || show_lint_row().is_some()>
                        <LintButton show_lint_row=lint_row_visible />
                    </Show>
                    <DownloadButton track_uid=track_id suggested_file_name=file_name />
                    <Show when=move || show_preview_row().is_some()>
                        <PreviewButton show_preview_row=preview_row_visible />
                    </Show>
                </div>
            </div>
            <Show when=move || show_preview_row().unwrap_or(false)>
                <div class="flex justify-center">
                    <PlayerRow row_visibility=preview_row_visible track_uid=track_id />
                </div>
            </Show>
            <Show when=move || show_lint_row().unwrap_or(false)>
                <div class="flex">
                    <LintRow track=track_body />
                </div>
            </Show>
        </td>
    }
}

/// A list of details for the track to complement the row information.
#[cfg_attr(not(debug_assertions), allow(unused_variables))]
#[component]
fn DetailsList(track_uid: Signal<TrackUid>, track: Signal<Track>) -> impl IntoView {
    let source_uri = move || track.with(|track| track.media_source.content.link.path.to_string());
    let file_name = move || extract_file_name(&source_uri());

    let primary_artist = move || {
        track.with(|track| {
            track
                .track_artist()
                .map_or_else(|| "Unknown".into(), ToOwned::to_owned)
        })
    };
    let other_artists = move || {
        track.with(|track| {
            track
                .actors
                .iter()
                .filter(|actor| actor.name != primary_artist())
                .map(|actor| actor.name.as_str())
                .intersperse(", ")
                .collect::<String>()
        })
    };

    let primary_title = move || {
        track.with(|track| {
            track
                .track_title()
                .map_or_else(|| "Unknown".into(), ToOwned::to_owned)
        })
    };
    let other_titles = move || {
        track.with(|track| {
            track
                .titles
                .iter()
                .filter(|title| title.name != primary_title())
                .map(|title| title.name.as_str())
                .intersperse(", ")
                .collect::<String>()
        })
    };

    let album_title = move || {
        track.with(|track| {
            track
                .album_title()
                .map(ToOwned::to_owned)
                .unwrap_or_default()
        })
    };
    let album_artist = move || {
        track.with(|track| {
            track
                .album_artist()
                .map(ToOwned::to_owned)
                .unwrap_or_default()
        })
    };

    let release_date = move || {
        track.with(|track| {
            track
                .released_at
                .as_ref()
                .map(|release_date| release_date.to_string())
                .unwrap_or_default()
        })
    };

    let movie =
        move || track.with(|track| join_tags_for_facet(track, FACET_ID_MOVIE).unwrap_or_default());
    let genre =
        move || track.with(|track| join_tags_for_facet(track, FACET_ID_GENRE).unwrap_or_default());
    let dance =
        move || track.with(|track| join_tags_for_facet(track, FACET_ID_DANCE).unwrap_or_default());

    let remaining_tags = move || {
        track.with(|track| {
            get_tags_for_track_excluding_facets(track, &[
                FACET_ID_MOVIE,
                FACET_ID_GENRE,
                FACET_ID_DANCE,
            ])
        })
    };
    let remaining_tag_view = move || {
        remaining_tags()
            .iter()
            .filter(|(facet, _)| facet != FACET_ID_MPM.as_str())
            .map(|(facet, values)| {
                view! {
                    <Badge
                        // If the title is empty, simply show a pill. This should
                        // be swapped for values as soon as scores are incorporated.
                        title=facet.into()
                        description=values.to_string()
                        classes="ml-1".to_string()
                    />
                }
            })
            .collect::<Vec<_>>()
    };

    view! {
        <ul class="no-bullets" style="border-left: none;">
            <li class="pb-4">
                <code>{file_name}</code>
            </li>
            <Show when=move || !other_artists().is_empty()>
                <li>"Other artists: " {other_artists}</li>
            </Show>
            <Show when=move || !other_titles().is_empty()>
                <li>"Other titles: " {other_titles}</li>
            </Show>
            <Show when=move || !album_title().is_empty()>
                <li>
                    "Album: " {album_title}
                    <span class:hidden=move || {
                        album_artist().is_empty()
                    }>" (" {album_artist} ")"</span>
                </li>
            </Show>
            <Show when=move || !movie().is_empty()>
                <li>"Known from: " {movie}</li>
            </Show>
            <Show when=move || !genre().is_empty()>
                <li>"Genre: " {genre}</li>
            </Show>
            <Show when=move || !dance().is_empty()>
                <li>"Dances: " {dance}</li>
            </Show>
            <Show when=move || !release_date().is_empty()>
                <li>"Released: " {release_date}</li>
            </Show>
            <Show when=move || !remaining_tags().is_empty()>
                <li class="py-4">{remaining_tag_view}</li>
            </Show>
            <Show when=|| { cfg!(debug_assertions) }>
                <li class="text-gray-600">"Track ID: " {track_uid().to_string()}</li>
            </Show>
        </ul>
    }
}
