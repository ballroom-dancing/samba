use leptos::*;
use samba_frontend_uicomponents::{Badge, BadgeLevel, LRTileCard};
use samba_shared::dancing::Dance;

use crate::{
    library::navigation::{LibraryAction, LibraryViewParameters},
    AppRoutes,
};

#[component]
pub fn DanceCardLink(dance: ReadSignal<Dance>) -> impl IntoView {
    view! {
        <a
            class="hide-visual p-0 m-0"
            href=AppRoutes::Library(
                LibraryAction::View(LibraryViewParameters {
                    filter: Some(format!("dance:{}", dance.get().abbreviation)),
                    ..Default::default()
                }),
            )
        >
            <DanceCard dance=dance />
        </a>
    }
}

#[component]
fn DanceCard(dance: ReadSignal<Dance>) -> impl IntoView {
    view! {
        <LRTileCard
            left=view! { <span>{move || dance().name}</span> }.into_view()
            right=view! {
                <Badge
                    level=BadgeLevel::Info
                    title="MPM".into()
                    // TODO: check reactivity (does this react to changes in dance?)
                    description=format!("{}–{}", dance().mpm_range.start, dance().mpm_range.end)
                />
            }
                .into_view()
        />
    }
}
