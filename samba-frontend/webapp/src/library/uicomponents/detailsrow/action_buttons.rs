use aoide::TrackUid;
use leptos::*;

use super::super::super::backend::fetch_track_file_url;

#[component]
pub fn DownloadButton(
    track_uid: Signal<TrackUid>,
    suggested_file_name: Signal<String>,
) -> impl IntoView {
    let file_url = Resource::new(
        || (),
        move |_| async move { fetch_track_file_url(&track_uid.get()).await.ok() },
    );

    view! {
        <Suspense>
            {move || {
                file_url()
                    .map(|url| {
                        url.map(|url| {
                            view! {
                                <a
                                    class="btn join-item"
                                    download=suggested_file_name
                                    href=url.clone()
                                    rel="external"
                                >
                                    <span class="fa-solid fa-download"></span>
                                </a>
                            }
                        })
                    })
            }}

        </Suspense>
    }
}

#[component]
pub fn PreviewButton(show_preview_row: RwSignal<bool>) -> impl IntoView {
    view! {
        <span
            class="btn join-item"
            on:click=move |_| {
                show_preview_row.set(!show_preview_row());
            }
        >
            <span
                class="fa-regular"
                class:fa-circle-stop=show_preview_row
                class:fa-circle-play=move || !show_preview_row()
            ></span>
        </span>
    }
}

#[component]
pub fn LintButton(show_lint_row: RwSignal<bool>) -> impl IntoView {
    view! {
        <span
            class="btn join-item"
            on:click=move |_| {
                show_lint_row.set(!show_lint_row());
            }
        >
            <span
                class="fa-lightbulb"
                class:fa-solid=show_lint_row
                class:fa-regular=move || !show_lint_row()
            ></span>
        </span>
    }
}
