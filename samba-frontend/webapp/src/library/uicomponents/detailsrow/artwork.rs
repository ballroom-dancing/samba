use aoide::TrackUid;
use leptos::*;
use wasm_bindgen::JsCast;
use web_sys::HtmlElement;

use super::super::super::backend::fetch_track_artwork_url;

#[component]
pub fn TrackArtwork(track_id: Signal<TrackUid>) -> impl IntoView {
    let image_src = Resource::new(
        || (),
        move |_| async move { fetch_track_artwork_url(&track_id.get()).await.ok() },
    );

    view! {
        <Suspense fallback=|| {
            view! { <FallbackAlbumImage /> }
        }>
            {move || {
                image_src()
                    .map(|src| {
                        if let Some(src) = src.clone() {
                            view! { <ImageHandler image_src=src /> }.into_view()
                        } else {
                            view! { <FallbackAlbumImage /> }.into_view()
                        }
                    })
            }}

        </Suspense>
    }
}

#[component]
fn ImageHandler(image_src: String) -> impl IntoView {
    let image_node = create_node_ref::<leptos::html::Img>();
    let error_handler = move |_| {
        let Some(image) = image_node.get() else {
            // This can basically never happen because the browser will have to
            // have the element in DOM to fire the error callback.
            return;
        };
        if !image.complete() || image.natural_height() == 0 {
            let parent = image
                .parent_node()
                .expect("The img element is part of a picture.")
                .unchecked_into::<HtmlElement>();
            mount_to(parent, || {
                view! { <FallbackAlbumImage /> }
            });
            image.remove();
        }
    };

    view! {
        <picture>
            <img
                alt="Media artwork for this track"
                on:error=error_handler
                ref=image_node
                src=image_src
                class="w-full"
            />
        </picture>
    }
}

#[component]
fn FallbackAlbumImage() -> impl IntoView {
    view! {
        <div class="w-100p u-relative u-center">
            <p class="u-absolute text-white font-bold u-center-alt lead">
                "Cover art not available."
            </p>
            <p class="w-100p pt-12 pb-12 pl-4 pr-4 bg-dark"></p>
        </div>
    }
}
