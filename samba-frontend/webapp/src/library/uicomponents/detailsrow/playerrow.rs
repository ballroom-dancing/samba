use aoide::TrackUid;
use leptos::*;

use super::super::super::backend::fetch_track_file_url;

#[component]
pub fn PlayerRow(row_visibility: RwSignal<bool>, track_uid: Signal<TrackUid>) -> impl IntoView {
    let file_url = Resource::new(
        || (),
        move |_| async move { fetch_track_file_url(&track_uid.get()).await.ok() },
    );

    view! {
        <Suspense>
            {move || {
                file_url()
                    .map(|url| {
                        url.map_or_else(
                            || {
                                view! {
                                    <div class="bg-error w-full my-8 py-2 flex justify-center">
                                        <p>"This track does not have a valid media source."</p>
                                    </div>
                                }
                                    .into_view()
                            },
                            |url| {
                                view! {
                                    <audio
                                        autoplay=true
                                        class="u-block w-100p"
                                        controls=true
                                        on:ended=move |_| {
                                            row_visibility.set(false);
                                        }

                                        src=url.clone()
                                        style="min-height: 50px;"
                                    >
                                        <div class="toast toast--danger">
                                            "Your browser does not support HTML5 audio, please update \
                                                and try again."
                                        </div>
                                    </audio>
                                }
                                    .into_view()
                            },
                        )
                    })
            }}

        </Suspense>
    }
}
