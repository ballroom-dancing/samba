use leptos::*;
use leptos_router::*;

pub enum RoutineAction {
    Create,
    Edit(i32),
    View(i32),
    List,
}

#[derive(Params, Clone, Copy, PartialEq)]
pub struct RoutineId {
    pub id: i32,
}

impl std::fmt::Display for RoutineAction {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(&match self {
            RoutineAction::Create => "/create".into(),
            RoutineAction::Edit(id) => format!("/edit/{id}"),
            RoutineAction::View(id) => format!("/view/{id}"),
            RoutineAction::List => "".into(),
        })
    }
}
