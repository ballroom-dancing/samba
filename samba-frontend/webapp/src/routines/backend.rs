use anyhow::{anyhow, Result};
use leptos::use_context;
use samba_frontend_shared::AppState;
use samba_shared::routines::{Routine, StoredRoutine};

use crate::util::{get_from_remote, post_to_remote, put_to_remote};

pub async fn fetch_routines() -> Result<Vec<StoredRoutine>> {
    get_from_remote("routines").await
}

pub async fn fetch_routine(id: i32) -> Result<StoredRoutine> {
    get_from_remote(&format!("routines/{id}")).await
}

pub async fn fetch_routine_report_url(routine_id: i32) -> Result<String> {
    let app_state = use_context::<AppState>().ok_or(anyhow!(
        "Fetching reports requires an initialized app with state."
    ))?;
    Ok(format!(
        "{}/routines/{}/report",
        &app_state.backend_url, routine_id
    ))
}

pub async fn create_routine(routine: Routine) -> Result<i32> {
    post_to_remote("routines/create", routine).await
}

pub async fn update_routine(id: i32, routine: Routine) -> Result<()> {
    put_to_remote(&format!("routines/{id}"), routine).await
}
