use leptos::*;
use leptos_router::*;
use samba_frontend_uicomponents::{Card, Heading, HeadingLevel};
use samba_shared::routines::{RoutineElement, StoredRoutine};

use super::super::{
    backend::{fetch_routine, fetch_routine_report_url},
    navigation::{RoutineAction, RoutineId},
};
use crate::{
    views::{NotFoundView, PleaseWaitView},
    AppRoutes,
};

mod figure_cards;
use figure_cards::FigureCardContent;
mod step_cards;
use step_cards::StepCardContent;

#[component]
pub fn RoutineDetailsView() -> impl IntoView {
    let routine_id = use_params::<RoutineId>();
    let routine = Resource::new(routine_id, move |_| async move {
        fetch_routine(routine_id.get().ok()?.id).await.ok()
    });

    view! {
        <section>
            <Suspense fallback=|| {
                view! { <PleaseWaitView /> }
            }>
                {move || { routine().map(|rtn| view! { <RoutineDetailsHandler routine=rtn /> }) }}
            </Suspense>
        </section>
    }
}

#[component]
fn RoutineDetailsHandler(routine: Option<StoredRoutine>) -> impl IntoView {
    if let Some(routine) = routine {
        let routine_id = routine.identifier;
        let (routine_title, _) = create_signal(
            routine
                .title
                .as_ref()
                .cloned()
                .unwrap_or_else(|| "Untitled routine".into()),
        );

        let last_step = routine.steps.len();
        let steps = routine
            .steps
            .iter()
            .enumerate()
            .map(|(i, elem)| {
                let is_separator = matches!(elem, RoutineElement::Separator(_));
                let elem = view! { <RoutineElement element=elem.clone() /> }.into_view();

                view! {
                    <li>
                        <Show when=move || { i > 0 }>
                            <hr />
                        </Show>
                        {if is_separator {
                            view! {
                                <div class="timeline-middle min-h-0 h-0">
                                    // The invisible circle is needed for width preservation.
                                    <span class="fa-solid fa-circle invisible" />
                                </div>
                                <div class="timeline-end w-full -ml-2 -mb-1">{elem}</div>
                            }
                                .into_view()
                        } else {
                            view! {
                                // The invisible circle is needed for width preservation.
                                // The invisible circle is needed for width preservation.
                                // The invisible circle is needed for width preservation.
                                <div class="timeline-middle">
                                    <span class="fa-solid fa-circle" />
                                </div>
                                <div class="timeline-end ml-4 mt-2 mb-2 w-full">
                                    <Card classes="card-compact shadow-sm -translate-y-3"
                                        .into()>{elem}</Card>
                                </div>
                            }
                                .into_view()
                        }}
                        <Show when=move || { i < last_step - 1 }>
                            <hr />
                        </Show>
                    </li>
                }
                .into_view()
            })
            .collect::<Vec<_>>();

        let report_url = Resource::new(
            || (),
            move |_| async move { fetch_routine_report_url(routine_id).await.ok() },
        );
        let report_button = move || {
            view! {
                <Suspense>
                    {move || {
                        report_url()
                            .map(|report_url| {
                                report_url
                                    .map(|url| {
                                        view! {
                                            <a
                                                class="btn"
                                                download=format!("{}.pdf", routine_title())
                                                href=url.clone()
                                                rel="external"
                                            >
                                                <span class="fa-regular fa-file-pdf"></span>
                                            </a>
                                        }
                                    })
                            })
                    }}

                </Suspense>
            }
        };

        view! {
            <section>
                // TODO: adjust title for dance vs. track and add choreographer
                <div class="flex gap-2 items-center mb-4">
                    <Heading classes="grow".into()>
                        "Viewing routine “"
                        {routine_title()}
                        "”"
                    </Heading>
                    {report_button}
                    <a
                        class="btn btn-primary"
                        href=AppRoutes::Routines(RoutineAction::Edit(routine_id))
                    >
                        "Edit"
                    </a>
                </div>
                <div>
                    <div class="flex gap-4">
                        {routine
                            .value
                            .choreographer
                            .map(|c| {
                                view! {
                                    <div class="grow">
                                        <span class="fa-solid fa-user mr-2" />
                                        <span>{c}</span>
                                    </div>
                                }
                            })}
                        {(!routine.value.performers.is_empty())
                            .then(|| {
                                view! {
                                    <div class="grow">
                                        <span class="fa-solid fa-star mr-2" />
                                        <span>{routine.value.performers.join(", ")}</span>
                                    </div>
                                }
                            })}
                    </div>
                    <div class="flex gap-4 my-2">
                        {routine.value.description.map(|d| view! { <p class="grow">{d}</p> })}
                        {(!routine.value.demonstration.is_empty())
                            .then(|| {
                                view! {
                                    <div class="grow">
                                        <span class="text-bold">"Demo performance(s):"</span>
                                        <ul class="list-disc">
                                            {routine
                                                .value
                                                .demonstration
                                                .iter()
                                                .enumerate()
                                                .map(|(i, d)| {
                                                    view! {
                                                        <li>
                                                            <a href=d.to_string()>"Example " {i + 1}</a>
                                                        </li>
                                                    }
                                                })
                                                .collect::<Vec<_>>()}
                                        </ul>
                                    </div>
                                }
                            })}
                    </div>
                </div>
                <Heading level=HeadingLevel::Section classes="mt-4 mb-4".into()>
                    "Steps"
                </Heading>
                <ul class="timeline timeline-snap-icon timeline-compact timeline-vertical flex-wrap">
                    {steps}
                </ul>
                {routine
                    .value
                    .notes
                    .map(|n| {
                        view! {
                            <Heading level=HeadingLevel::Section classes="mt-4 mb-2".into()>
                                "Notes"
                            </Heading>
                            <p>{n}</p>
                        }
                    })}
            </section>
        }
        .into_view()
    } else {
        view! {
            <section>
                // TODO: adjust title for dance vs. track and add choreographer
                <NotFoundView />
            </section>
        }
        .into_view()
    }
}

#[component]
fn RoutineElement(element: RoutineElement) -> impl IntoView {
    match element {
        RoutineElement::Figure(figure) => view! { <FigureCardContent figure=figure /> }.into_view(),
        RoutineElement::Step(step) => {
            view! { <StepCardContent step=step bold_face=true /> }.into_view()
        },
        RoutineElement::Pause(timing) => view! {
            <span>
                <span class="font-bold">"Pause"</span>
                {if timing.to_semiquavers() > 0 {
                    format!(" (for {} beats)", timing.to_semiquavers().div_ceil(4))
                } else {
                    "".into()
                }}
            </span>
        }
        .into_view(),
        RoutineElement::Stop => view! { <span class="font-bold">"Stop here"</span> }.into_view(),
        RoutineElement::Repeat(timing) => view! {
            <span>
                <span class="font-bold">"Repeat"</span>
                {if timing.to_semiquavers() > 0 {
                    format!(" (skipping first {} beats)", timing.to_semiquavers().div_ceil(4))
                } else {
                    " from beginning".into()
                }}
            </span>
        }
        .into_view(),
        RoutineElement::Separator(comment) => view! {
            <div class="flex flex-col w-full">
                <div class="divider divider-end text-gray-600 text-sm">{comment}</div>
            </div>
        }
        .into_view(),
    }
}
