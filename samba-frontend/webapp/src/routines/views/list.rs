use itertools::Itertools;
use leptos::*;
use leptos_router::use_navigate;
use samba_frontend_uicomponents::{Card, Heading, Placeholder};
use samba_shared::routines::StoredRoutine;

use super::super::{backend::fetch_routines, navigation::RoutineAction};
use crate::{views::PleaseWaitView, AppRoutes};

#[component]
pub fn RoutineListView() -> impl IntoView {
    let routines = Resource::new(
        || (),
        move |_| async move { fetch_routines().await.unwrap_or_default() },
    );

    view! {
        <section>
            <Suspense fallback=|| {
                view! {
                    <Heading>"Your routines"</Heading>
                    <PleaseWaitView />
                }
            }>
                {move || { routines().map(|rtns| view! { <RoutineListHandler routines=rtns /> }) }}
            </Suspense>
        </section>
    }
}

#[component]
fn RoutineListHandler(routines: Vec<StoredRoutine>) -> impl IntoView {
    let nonempty_routines = !routines.is_empty();
    let (routines, _) = create_signal(routines);

    view! {
        <section>
            <Show
                when=move || nonempty_routines
                fallback=|| {
                    view! {
                        <Heading>"Your routines"</Heading>
                        <Placeholder
                            icon=view! {
                                <span
                                    class="fa-solid fa-stop-circle"
                                    style="font-size: 500%;"
                                ></span>
                            }
                                .into_view()
                            title=view! { "There are no routines" }.into_view()
                            subtitle=view! { "Up to now you did not register any routines." }
                                .into_view()
                            commands=vec![
                                view! {
                                    <div class="btn-container">
                                        <a
                                            class="btn btn-primary u-inline-block"
                                            href=AppRoutes::Routines(RoutineAction::Create)
                                        >
                                            "Create a new routine"
                                        </a>
                                    </div>
                                }
                                    .into_view(),
                            ]
                        />
                    }
                }
            >
                <div class="flex gap-2 items-center mb-8">
                    <Heading classes="grow".into()>"Your routines"</Heading>
                    <a
                        class="btn btn-primary u-pull-right"
                        href=AppRoutes::Routines(RoutineAction::Create)
                    >
                        "New"
                    </a>
                </div>
                <For each=routines key=|x| x.identifier let:rtn>
                    <RoutineCard routine=rtn />
                </For>
            // TODO: pagination
            </Show>
        </section>
    }
}

#[component]
fn RoutineCard(routine: StoredRoutine) -> impl IntoView {
    let choreographer = routine.value.choreographer.map(|c| {
        view! {
            <span>
                <span class="fa-solid fa-user mr-2" />
                <span>{c}</span>
            </span>
        }
    });

    let performers = (!routine.value.performers.is_empty()).then(|| {
        let capped_string = format!(
            "{}{}",
            routine.value.performers.iter().take(3).join(", "),
            if routine.value.performers.len() > 3 {
                ", …"
            } else {
                ""
            }
        );
        view! {
            <span>
                <span class="fa-solid fa-star mr-2" />
                <span>{capped_string}</span>
            </span>
        }
    });

    view! {
        <Card
            title=routine.value.title.clone().unwrap_or_else(|| "Untitled routine".into())
            classes="cursor-pointer".into()
            on:click=move |_| {
                use_navigate()(
                    &AppRoutes::Routines(RoutineAction::View(routine.identifier)).to_string(),
                    Default::default(),
                );
            }
        >
            <div class="flex gap-4">{choreographer} {performers}</div>
            {routine.value.description}
        </Card>
    }
}
