use leptos::*;
use samba_frontend_uicomponents::LRTileCard;
use samba_shared::routines::{Figure, RoutineElement, Step};
use uuid::Uuid;

use super::{super::super::RoutineItem, RoutineItemUpdateModal};

#[component]
pub fn RoutineItemView(
    item: RoutineItem,
    routine_items: RwSignal<Vec<RoutineItem>>,
) -> impl IntoView {
    let show_update_modal = RwSignal::new(false);
    let item_display = match &item.item {
        RoutineElement::Figure(Figure { title, .. }) => format!("Figure “{title}”"),
        RoutineElement::Step(Step { comment, .. }) => format!(
            "Step{}",
            comment
                .as_ref()
                .map(|c| format!(" (with comment: “{c}”)"))
                .unwrap_or_default()
        ),
        RoutineElement::Pause(timing) => {
            if timing.to_semiquavers() > 0 {
                format!("Pause (for {} beats)", timing.to_semiquavers().div_ceil(4))
            } else {
                "Pause".into()
            }
        },
        RoutineElement::Stop => "Stop".into(),
        RoutineElement::Repeat(timing) => {
            if timing.to_semiquavers() > 0 {
                format!(
                    "Repeat (skipping first {} beats)",
                    timing.to_semiquavers().div_ceil(4)
                )
            } else {
                "Repeat from beginning".into()
            }
        },
        RoutineElement::Separator(comment) => format!(
            "Separator{}",
            comment
                .as_ref()
                .map(|c| format!(" (with comment: “{c}”)"))
                .unwrap_or_default()
        ),
    };

    let remove_action = move |uuid: Uuid| {
        move |_| {
            let mut items = routine_items().clone();
            items.retain(|element: &RoutineItem| element.uuid != uuid);
            routine_items.set(items);
        }
    };

    let first_item_uuid = move || {
        routine_items().first().map_or_else(
            || {
                // if there is no first item, we can create a new UUID; because of
                // their randomness it will be not equal to any other UUID in the
                // vector (in this case none)
                Uuid::new_v4()
            },
            |item| item.uuid,
        )
    };
    let move_up_action = move |uuid: Uuid| {
        move |_| {
            if let Some(pos) = routine_items
                .get()
                .iter()
                .position(|element: &RoutineItem| element.uuid == uuid)
            {
                let mut items = routine_items().clone();
                items.swap(pos, pos.saturating_sub(1));
                routine_items.set(items);
            }
        }
    };

    let last_item_uuid = move || {
        routine_items().last().map_or_else(
            || {
                // if there is no last item, we can create a new UUID; because of
                // their randomness it will be not equal to any other UUID in the
                // vector (in this case none)
                Uuid::new_v4()
            },
            |item| item.uuid,
        )
    };
    let move_down_action = move |uuid: Uuid| {
        move |_| {
            if let Some(pos) = routine_items
                .get()
                .iter()
                .position(|element: &RoutineItem| element.uuid == uuid)
            {
                let mut items = routine_items().clone();
                items.swap(
                    pos,
                    (pos + 1).min(routine_items.get().len().saturating_sub(1)),
                );
                routine_items.set(items);
            }
        }
    };

    view! {
        <LRTileCard
            left=view! { <span class="font-bold pl-2">{item_display}</span> }.into_view()
            right=view! {
                <div class="join">
                    <button
                        class="btn join-item fa-solid fa-caret-up"
                        disabled=move || first_item_uuid() == item.uuid
                        on:click=move_up_action(item.uuid)
                        type="button"
                    ></button>
                    <button
                        class="btn join-item fa-solid fa-caret-down"
                        disabled=move || last_item_uuid() == item.uuid
                        on:click=move_down_action(item.uuid)
                        type="button"
                    ></button>
                    <button
                        class="btn join-item fa-solid fa-pen"
                        on:click=move |_| {
                            show_update_modal.set(true);
                        }
                        type="button"
                    ></button>
                    <button
                        class="btn join-item fa-solid fa-close"
                        disabled=move || routine_items().len() == 1
                        on:click=remove_action(item.uuid)
                        type="button"
                    ></button>
                </div>
            }
                .into_view()
        />
        <RoutineItemUpdateModal
            modal_visibility=show_update_modal
            routine_items=routine_items
            current_routine_item=item
        />
    }
}
