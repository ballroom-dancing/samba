use anyhow::{anyhow, Context};
use codee::string::JsonSerdeWasmCodec as JsonCodec;
use gloo_net::http::{Request, Response};
use leptos::*;
use leptos_use::storage::use_local_storage;
use samba_frontend_shared::{
    profile::{UserProfile, LOCAL_STORAGE_PROFILE_KEY},
    AppState, Message,
};

use crate::management::logout;

/// Handle a remote reponse and especially the specific error kind of being
/// unauthorized. In this error case, log out the user to force a new session
/// instead of constantly getting invalid responses.
async fn handle_remote_response(response: Response) -> anyhow::Result<Response> {
    if response.status() == 401 {
        // TODO: use refresh tokens and try to prolong the session
        let (profile, _, _) =
            use_local_storage::<UserProfile, JsonCodec>(LOCAL_STORAGE_PROFILE_KEY);
        if !profile().is_guest() {
            // If a user was logged in, log him out to force refreshing the
            // session token.
            logout().await;
        }
        return Err(anyhow!("Unauthorized access, new authorization required."));
    }

    if response.status() == 502 || response.status() == 503 {
        let app_state = use_context::<AppState>()
            .ok_or(anyhow!("App state must be initialized for requests."))?;
        app_state.add_message(Message::new_error(
            "Error while contacting backend.".into(),
            format!(
                "Backend seems to be down. Please check that it is running at {}",
                app_state.backend_url
            ),
        ));
    }

    Ok(response)
}

/// Fetch a value of type `T` from the backend (using JSON deserialization).
pub async fn get_from_remote_with_query<T, Q>(
    remote_part: &str,
    query_params: Option<Q>,
) -> anyhow::Result<T>
where
    T: serde::de::DeserializeOwned,
    Q: serde::ser::Serialize,
{
    let app_state =
        use_context::<AppState>().ok_or(anyhow!("App state must be initialized for requests."))?;
    let mut request_builder = Request::get(&format!("{}/{}", &app_state.backend_url, remote_part));
    if !app_state.get_jwt().is_empty() {
        request_builder =
            request_builder.header("Authorization", &format!("Bearer {}", app_state.get_jwt()));
    }
    if let Some(q) = query_params {
        let query_value = serde_json::to_value(q)?;
        let map = query_value
            .as_object()
            .ok_or(anyhow!("query params must be map"))?;
        request_builder = request_builder.query(map.iter().map(|(k, v)| {
            (
                k.as_str(),
                serde_json::to_string(&v).expect("value to be serializable"),
            )
        }));
    }

    handle_remote_response(
        request_builder
            .send()
            .await
            .with_context(|| "did not get response")?,
    )
    .await?
    .json()
    .await
    .with_context(|| "failed to deserialize from JSON")
}

/// Fetch a value of type `T` from the backend (using JSON deserialization).
pub async fn get_from_remote<T>(remote_part: &str) -> anyhow::Result<T>
where
    T: serde::de::DeserializeOwned,
{
    get_from_remote_with_query(remote_part, Option::<()>::None).await
}

/// Fetch a plain response from the backend (using JSON deserialization).
pub async fn get_from_remote_without_error_handling(remote_part: &str) -> anyhow::Result<Response> {
    let app_state =
        use_context::<AppState>().ok_or(anyhow!("App state must be initialized for requests."))?;
    let mut request_builder = Request::get(&format!("{}/{}", &app_state.backend_url, remote_part));
    if !app_state.get_jwt().is_empty() {
        request_builder =
            request_builder.header("Authorization", &format!("Bearer {}", app_state.get_jwt()));
    }

    request_builder
        .send()
        .await
        .with_context(|| "did not get response")
}

/// Get a response of type `R` from the backend (using JSON deserialization)
/// for a post request with body of type `B`.
pub async fn post_to_remote<R, B>(remote_part: &str, body: B) -> anyhow::Result<R>
where
    B: serde::ser::Serialize,
    R: serde::de::DeserializeOwned,
{
    let app_state =
        use_context::<AppState>().ok_or(anyhow!("App state must be initialized for requests."))?;
    let mut request_builder = Request::post(&format!("{}/{}", &app_state.backend_url, remote_part));
    if !app_state.get_jwt().is_empty() {
        request_builder =
            request_builder.header("Authorization", &format!("Bearer {}", app_state.get_jwt()));
    }

    handle_remote_response(
        request_builder
            .json(&body)
            .with_context(|| "failed to serialize")?
            .send()
            .await
            .with_context(|| "did not get response")?,
    )
    .await?
    .json()
    .await
    .with_context(|| "failed to deserialize from JSON")
}

/// Get a Response from the backend  for a post request with body of type `B`.
pub async fn post_to_remote_without_error_handling<B>(
    remote_part: &str,
    body: B,
) -> anyhow::Result<Response>
where
    B: serde::ser::Serialize,
{
    let app_state =
        use_context::<AppState>().ok_or(anyhow!("App state must be initialized for requests."))?;
    let mut request_builder = Request::post(&format!("{}/{}", &app_state.backend_url, remote_part));
    if !app_state.get_jwt().is_empty() {
        request_builder =
            request_builder.header("Authorization", &format!("Bearer {}", app_state.get_jwt()));
    }

    request_builder
        .json(&body)
        .with_context(|| "failed to serialize")?
        .send()
        .await
        .with_context(|| "did not get response")
}

/// Get a response of type `R` from the backend (using JSON deserialization)
/// for a post request with body of type `B`. Sometimes, an update will only
/// result in an empty HTTP response body which will be deserialized as `()`.
pub async fn put_to_remote<R, B>(remote_part: &str, body: B) -> anyhow::Result<R>
where
    B: serde::ser::Serialize,
    R: serde::de::DeserializeOwned,
{
    let app_state =
        use_context::<AppState>().ok_or(anyhow!("App state must be initialized for requests."))?;
    let mut request_builder = Request::put(&format!("{}/{}", &app_state.backend_url, remote_part));
    if !app_state.get_jwt().is_empty() {
        request_builder =
            request_builder.header("Authorization", &format!("Bearer {}", app_state.get_jwt()));
    }

    // TODO: check if we can get by without string allocation
    let text = handle_remote_response(
        request_builder
            .json(&body)
            .with_context(|| "failed to serialize")?
            .send()
            .await
            .with_context(|| "did not get response")?,
    )
    .await?
    .text()
    .await?;

    serde_json::from_str(if !text.is_empty() {
        &text
    } else {
        // An empty string representation is invalid JSON. Therefore, replace
        // it with `null` (which is serde_json's representation of Rust's unit
        // type).
        "null"
    })
    .with_context(|| "failed to deserialize from JSON")
}

/// Delete the resource at the given remote location.
pub async fn delete_from_remote<R>(remote_part: &str) -> anyhow::Result<R>
where
    R: serde::de::DeserializeOwned,
{
    let app_state =
        use_context::<AppState>().ok_or(anyhow!("App state must be initialized for requests."))?;
    let mut request_builder =
        Request::delete(&format!("{}/{}", &app_state.backend_url, remote_part));
    if !app_state.get_jwt().is_empty() {
        request_builder =
            request_builder.header("Authorization", &format!("Bearer {}", app_state.get_jwt()));
    }

    // TODO: check if we can get by without string allocation
    let text = handle_remote_response(
        request_builder
            .send()
            .await
            .with_context(|| "did not get response")?,
    )
    .await?
    .text()
    .await?;

    serde_json::from_str(if !text.is_empty() {
        &text
    } else {
        // An empty string representation is invalid JSON. Therefore, replace
        // it with `null` (which is serde_json's representation of Rust's unit
        // type).
        "null"
    })
    .with_context(|| "failed to deserialize from JSON")
}

/// Refresh the page at the same location.
pub fn refresh_page() {
    window().location().reload().unwrap();
}

/// Scroll to the top of the view.
pub fn scroll_top(smooth: bool) {
    let options = web_sys::ScrollToOptions::new();
    options.set_top(0.0);
    options.set_behavior(if smooth {
        web_sys::ScrollBehavior::Smooth
    } else {
        web_sys::ScrollBehavior::Instant
    });
    window().scroll_to_with_scroll_to_options(&options);
}
