const invoke = window.__TAURI__.invoke

export async function getBackendPort() {
    return await invoke("get_backend_port");
}
