# samba-frontend-webapp

This is the core user interface of samba. It provides all Rust sources for the
WebAssembly application.

## Troubleshooting

## Build errors with `cargo-leptos`

While this frontend is built with leptos, it is a direct port of a sycamore
frontend. As such (and because of the intention of use in a Tauri webapp), it
has been ported with CSR in mind. Use `trunk` for now. Make sure to use the
`csr` feature (`trunk serve --features csr`).

As it uses daisyui, `npm i daisyui` is a necessary setup step before running
`trunk` (`npm` and `tailwindcss` are installed as part of the Nix development
shell).

If you are interested in bringing SSR and hydration to the application, feel
free to file a merge request fixing build issues with `cargo-leptos`.

### Speech synthesis

All major browsers advertise support for the Web SpeechSynthesis API. However,
on Linux I have not been able to get it working in Chromium-based browsers. So
for speech synthesis, best use an operating system and browser that does
support the API.

My Arch Linux development setup for speech synthesis testing includes:

- `firefox` (actually `firefox-developer-edition`) from the official
  repositories (the one one can install from nix does not work)
- `speech-dispatcher` as backend daemon which connects to an actual
  speech engine
- `espeak-ng` for speech synthesis (this can be tested on the command-line
  using `espeak-ng "Test"`, if that works and `spd-say "Test"` as well, the
  remaining troubles will be caused by the browser)
